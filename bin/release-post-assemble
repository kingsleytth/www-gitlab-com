#!/usr/bin/env ruby

require 'yaml'
require 'fileutils'

module ReleasePostHelpers
  Abort = Class.new(StandardError)
  Done = Class.new(StandardError)

  def capture_stdout(cmd)
    IO.popen(cmd, &:read)
  end

  def fail_with(message)
    raise Abort, "\e[31merror\e[0m #{message}"
  end

  def git_add(src)
    capture_stdout(['git', 'add', src])
  end

  def git_commit(message)
    capture_stdout(['git', 'commit', '--message', message])
  end

  def git_mv(src, dst)
    capture_stdout(['git', 'mv', src, dst])
    $stdout.puts "\e[34mmoved\e[0m #{src} to #{dst}"
  end

  def replace(old, new, file)
    expr = "%s##{old}##{new}#|x"
    $stdout.puts "\e[34mchanged\e[0m #{old} path to #{new} in #{file}"
    capture_stdout(['ex', '-sc', expr, file])
  end

end

class ReleasePost
  include ReleasePostHelpers

  def initialize
    assert_release_branch!

    @release = branch_name.match('^release-(\d+-\d+)$').captures[0].tr('-','_')
    @release_data_dir = File.join(File.expand_path('../data', __dir__), 'release_posts', @release)
    @release_img_dir = File.join(File.expand_path('../source', __dir__), 'images', @release)
  end

  def assemble
    # Move unreleased items to release directory
    FileUtils.mkdir_p @release_data_dir
    Dir.glob("data/release_posts/unreleased/*.{yaml,yml}") do |filepath|
      git_mv(filepath, @release_data_dir)
    end

    # Move unrelease images to the release directory
    FileUtils.mkdir_p @release_img_dir
    Dir.glob("source/images/unreleased/*.{png,jpg,jpeg,gif}") do |filepath|
      git_mv(filepath, @release_img_dir)
    end

    Dir.glob(File.join(@release_data_dir, "*.{yaml,yml}")) do |filepath|
      file = YAML.load_file(filepath)

      # Update image_urls
      content_blocks(file).each { |block| update_image_url(filepath, block) }
    end

    git_commit("Added new release post content")
  end

  private

  def assert_release_branch!
    return unless !branch_name.match('^release-\d+-\d+$')

    fail_with "Create or checkout a release branch first!"
  end

  def branch_name
    @branch_name ||= capture_stdout(%w[git symbolic-ref --short HEAD]).strip
  end

  def content_blocks(content)
    return [
      content.dig('features', 'top'),
      content.dig('features', 'primary'),
      content.dig('features', 'secondary'),
      content.dig('deprecation')
    ].compact.reduce([], :|)
  end

  def update_image_url(filepath, block)
    return if block['image_url'].nil? || File.exists?(File.join('source', block['image_url']))

    guessed_image_url = block['image_url'].gsub('/unreleased/',"/#{@release}/")

    if (File.exists?(File.join('source', guessed_image_url)))
      replace(block['image_url'], guessed_image_url, filepath)
      git_add(filepath)
      return
    end

    puts "#{block['image_url']} is broken"
  end
end

if $0 == __FILE__
  begin
    ReleasePost.new.assemble
  rescue ReleasePostHelpers::Abort => ex
    $stderr.puts ex.message
    exit 1
  rescue ReleasePostHelpers::Done
    exit
  end
end
