Please review and update issues/epics/MRs from the last release retro in the document.  

Please have a retrospective with your team following the guidelines outlined in the handbook here: https://about.gitlab.com/handbook/engineering/management/#team-retrospectives and https://about.gitlab.com/handbook/engineering/management/team-retrospectives/.  

After the retrospective is complete, please select some of your learnings to share company-wide in the retro doc: https://docs.google.com/document/d/1nEkM_7Dj4bT21GJy0Ut3By76FZqCfLBmFQNVThmW2TY/edit

For items which have didn't go well, create an issue to address.  In the case where a manager feels an issue can/should not be created, please include that in the what went wrong section.

Please try to group by *topic* rather than by *team*, as suggested in #3416. Let's try to make sure our entries are in as soon as possible so we can start prepping for a smoother retrospective.

| Team                | Eng Manager         | Product Manager |  Retro done?        | Doc updated?       |
| ------------------- | --------------- | :---------: | :----------------: | :----------------: |
| Acquisition | @jeromezng  | @jstava |  |  | 
| Configure | @nicholasklick | @nagyv-gitlab  |  |  |
| Conversion | @jeromezng  | @s_awezec |  |  | 
| Create:Static Site Editor  | @jeanduplessis | @ericschurter  |  |  |
| Create:Editor       | @andr3 + @dsatcher + @rkuba  | @phikai  |    |   | 
| Create:Knowledge    | @andr3 + @dsatcher + @rkuba  | @cdybenko  |     |    | 
| Create:Source Code  | @m_gill + @andr3  | @danielgruesso  |  |  | 
| Database            | @craig-gomes | @joshlambert  |  |  | 
| Defend              | @lkerr + @thiagocsf  | @matt_wilson + @sam.white  |  |  | 
| Distribution        | @mendeni          | @ljlane  |  |  : | 
| Growth:Expansion    | @pcalder | @timhey  |  |  | 
| Growth:Retention    | @pcalder | @mkarampalas  |  |  | 
| Fulfillment         | @jameslopez + @chris_baus  | @amandarueda  |  | | 
| Geo                 | @nhxnguyen  | @fzimmer  |   |  | 
| Gitaly              | @zj-gitlab  | @jramsay  |   |  | 
| Manage              | @lmcandrew + @Dennis                | @hdelalic + @jeremy  | | | 
| Manage:Analytics    | @dennis + @djensen + @wortschi      | @jeremy + @jshackelford  |   |   | 
| Memory              | @craig-gomes | @joshlambert  |  |  | 
| Monitor:APM         | @mnohr + @ClemMakesApps | @dhershkovitch |  |  |
| [Monitor:Health](https://gitlab.com/gl-retrospectives/monitor/health/-/issues/4)      | @crystalpoole  + @ClemMakesApps | @sarahwaldner  |  |  | 
| Package             | @jhampton | @trizzi  |  | | |
| Plan                | @donaldcook + @johnhope + @jlear | @justinfarris |  |  | 
| Release:Progressive Delivery | @csouthard + @nicolewilliams | @ogolowinski  |  |  | 
| Release:Release Management | @sean_carroll + @nicolewilliams | @jmeshell  |    |  | 
| [Quality](https://gitlab.com/gl-retrospectives/quality/-/issues/17)| @meks |  | | | 
| [Global Search](https://gitlab.com/gl-retrospectives/search-team/-/issues/7)              | @changzhengliu  | @phikai + @JohnMcGuire  | :heavy_check_mark: |:heavy_check_mark: | 
| [Secure](https://gitlab.com/gl-retrospectives/secure/-/issues/15)| @twoodham  | @stkerr + @tmccaslin + @NicoleSchwartz + @derekferguson  |  | | 
| Telemetry | @jeromezng  | @sid_reddy |  |  | 
| UX                  | @clenneville    |  |  |  | 
| Verify:CI           | @darbyfrey | @thaoyeager  |  |  | 
| Verify:Runner       | @erushton | @DarrenEastman  |  |  | 
| Verify:Testing      | @rickywiens  | @jheimbuck_gl  |  |  | 
| Ecosystem | @leipert | @deuley | | | 

cc: @fseifoddini

