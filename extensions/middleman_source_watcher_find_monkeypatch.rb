module Middleman
  class SourceWatcher
    # Find a specific file in this watcher.
    #
    # @param [String, Pathname] path The search path.
    # @param [Boolean] glob If the path contains wildcard characters.
    # @return [Middleman::SourceFile, nil]
    # Contract Or[String, Pathname], Maybe[Bool] => Maybe[IsA['Middleman::SourceFile']]
    def find(path, glob = false)
      path = path.to_s.encode!('UTF-8', 'UTF-8-MAC') if RUBY_PLATFORM =~ /darwin/

      ###
      # PATCH FOR MONOREPO
      ###

      # 'duplicate_relative_prefix' allows monorepo-root level partials like `/source/includes/layout/head`
      # to be found from site-level pages like `sites/handbook/source/handbook/some-page.html.md`,
      # but _*WITHOUT*_ requiring a SourceWatcher (`files.watch(:source, path: monorepo_root/...`)
      # to be created for the monorepo root `source` dir, which would cause ALL the files in there
      # to get loaded, not just the ones for the site.
      #
      # For example, if there were a file `/source/layouts/shared-layout.haml`, which contained a
      # reference to a partial `= partial "includes/layout/head"`, in order to use that layout
      # from `sites/handbook/source/my-page.md`, add the following entry to `sites/handbook/config.rb`:
      #
      # files.watch(
      #   :source,
      #   path: File.expand_path("#{monorepo_root}/source/includes", __dir__),
      #   duplicate_relative_prefix: 'includes',
      # )
      #
      # In other words, if there is a given relative path reference from file A, and file
      # A may be included by different middleman roots at different directories, 'duplicate_relative_prefix'
      # can compensate for that.

      duplicate_relative_prefix = @options[:duplicate_relative_prefix]
      duplicate_relative_prefix_regex = %r{^#{duplicate_relative_prefix.to_s}/}
      if duplicate_relative_prefix && path =~ duplicate_relative_prefix_regex
        path = path.gsub!(duplicate_relative_prefix_regex, '')
      end

      ###
      # END PATCH FOR MONOREPO
      ###

      p = Pathname(path)

      return nil if p.absolute? && !p.to_s.start_with?(@directory.to_s)

      p = @directory + p if p.relative?

      if glob
        @extensionless_files[p]
      else
        @files[p]
      end
    end
  end
end
