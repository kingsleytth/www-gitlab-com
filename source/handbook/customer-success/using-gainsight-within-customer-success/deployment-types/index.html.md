---
layout: markdown_page
title: "Customer Deployment Types"
---
## Customer Deployment Types

During customer onboarding, these fields should be filled out in Gainsight. Deployment types definitions for customer attributes on the Gainsight C360:

### Customer Conversion Source

The purpose is understanding where the customer came from — this is about sourcing (e.g., marketing/SDR'ing as an analogy). Each customer should fit into one of three buckets regardless of when they started using GitLab:

* Community conversion (first value waived) - A customer who was using the free community version and is converting (or converted) to a paid subscription tier.
* New Customer - This is a brand new customer who doesn’t (or didn't) have an existing GitLab deployment.
* Existing - New TAM (first value waived) - The customer was already a paying customer and is being (or was) assigned a TAM (e.g., upgraded from a small deployment to large).

### Hosting

What is the customer's main deployment type?

* Self-managed - on-premises
* Self-managed - cloud
* Self-managed - hybrid
* GitLab.com
* Self-managed and GitLab.com

### Deployer

Addresses the question of *who* did the work to deploy them?

* Customer-deployed
* GitLab services
* Partner services
