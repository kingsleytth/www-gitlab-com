---
layout: handbook-page-toc
title: "Field Communications"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Welcome to the Field Communications Handbook

This team is responsible for creating effective, timely, and easily consumable communications with Sales and Customer Success audiences. Our goal is to help the field sell better, faster, and smarter with communication programs that keep them better informed of organizational/business updates that impact their roles, as well as useful resources that will enable day-to-day work. 

For more information about the Field Enablement team, visit the [team handbook page.](/handbook/sales/field-operations/field-enablement/)

## Field Communications Playbook 
In order to streamline important announcements and reduce noise for the field team, we operate by a set Field Communications Playbook. This playbook defines tier 1, tier 2, and tier 3 organizational announcements/changes and applies a communication roll-out plan to each. The playbook works on a matrix of two main components: 
1. **Urgency:** How crucial is it that the field know now? 
1. **Impact:** How much does it impact the field's ability to sell and support effectively? 

### How to Use 
If you have an announcement or update that you'd like to communicate to the field, please review the playbook below. Run through the qualifying questions that will assist you in identifying the tier of your announcement. Once you have identified your announcement tier, review the applicable playbook for your tier and familiarize yourself with the necessary components needed to make an impactful and effective announcement to the field team. We have also included visualizations of this process below. 

Once you have identified the tier of your update/announcement and necessary communication components, please open up an issue using [the request process below](/handbook/sales/field-communications/#requesting-field-announcements). 

## Tier 1 Update
This update is both highly-urgent and selling-related. It likely includes a knowledge and/or skill gap that needs to be addressed in the field. 

Examples: Major process changes, CRO/CEO/e-group calls-to-action, product packaging changes, product marketing major updates 

### Qualifying Questions 
* Does this update directly impact the field’s ability to sell effectively? 
* Does this update directly impact all or the majority of the field? 
* Does this update require enablement of the field? 

If you answered no to any of these questions, consider the playbook for Tier 2 updates.

### Communications Playbook 
* Cadence for Tier 1 updates should be twofold: 
    * Real-time/as-it-happens 
    * Retroactively reiterated across channels 
* DRIs should engage these teams for input:
    * [Field Leadership](/handbook/sales/#team-structure--roles) - Executive sign-off 
    * [Field Enablement](/handbook/sales/field-operations/field-enablement/) - Creation of enablement materials 
    * [Field Operations](/handbook/sales/field-operations/) - Facilitation of relevant process changes
    * [Field Communications](/handbook/sales/field-communications/) - Consultation on communication plan
    * [People Operations](/handbook/people-group/) - Consultation on topics that impact team members 
* The update should leverage these communication channels: 
    * [#field-fyi Slack channel](/handbook/sales/sales-google-groups/field-fyi-channel/) - For real-time, asynchronous communication that gives context around the announcement, shares enablement resources, and clearly defines a CTA. 
    * #sales-managers Slack channel - To garner manager-level awareness and solict manager help cascading updates and time-sensitive CTAs to their teams.  
    * [WW Field Sales Call](/handbook/sales/#bi-weekly-monday-sales-call) - For synchronous communication that amplifies the announcement and allows the DRI and leadership to field any questions/concerns.
    * Segment Updates for Field Leaders - To arm managers with the necessary information to discuss the update with their teams and reiterate any CTAs. 
    * [Field Flash newsletter](/handbook/sales/field-communications/field-flash-newsletter/) - To reiterate/recap the news and link to enablement resources. 
    * [Enablement/CS Webinar](/handbook/sales/training/sales-enablement-sessions/) - To deep-dive into the update and any resulting behavioral/procedural changes. 
    * Handbook - To maintain a single-source of truth, evergreen reference guide. 
* Considerations: 
    * No blackout restrictions applied to this level of communications. Note that as a best practice, there should only be 1-2 tier 1 updates per month. The ultimate goal should be only 2-3 per quarter to avoid distracting the field. 
    * The primary spokespeople for this update should be [field leadership](/handbook/sales/#team-structure--roles). 

### Practical Application
There is an upcoming major pricing update that will impact all customers and prospects. The field must be enabled to field conversations with customers regarding the changes. The business DRI will engage Field Leadership, Ops, Enablement and Communications as well as People Ops to discuss overall impact, plan and create enablement materials, facilitate the appropriate changes to sales systems and collaborate on internal messaging to field members. 

When the announcement goes live, the CRO or other relevant e-group member will notify the field via field-fyi as well as the closest applicable WW Sales Call, sharing enablement resources, updated/new handbook documentation, and a clearly-defined CTA. Managers will be reminded to cascade information about the update to their teams via the sales-manager Slack channel. Field Comms will reiterate the update and recirculate enablement resources via segment updates to field leaders as well as in Field Flash newsletter. Enablement will coordinate a webinar to deep-dive into the update, and field comms will circulate the recording as another reiteration point. 

## Tier 2 Update
This update is moderately to minimally urgent and selling-related. It likely includes an important update or change that does not involve a major knowledge and/or skill gap, but still has a sizable impact on the field’s ability to sell. 

Examples: Process changes, CRO/CEO/e-group announcements, sales/company performance, enablement resources, competitive intelligence, customer stories, channel & alliance partner updates

### Qualifying Questions 
* Does this update directly impact at least one major segment (ENT, COMM, CS) of the field organization?
* Does this update help the field do their jobs more effectively?  
* Does this update need to be communicated in the next week? 

If you answered no to any of these questions, consider the playbook for Tier 3 updates.

### Communications Playbook 
* Cadence for Tier 2 updates should be twofold: 
    * Next available opportunity
    * Reiterated across channels 
* DRIs should engage these teams for input:
    * [Field Enablement](/handbook/sales/field-operations/field-enablement/) - Assess a need to update enablement materials 
    * [Field Operations](/handbook/sales/field-operations/) - Facilitation of relevant process changes
    * [Field Communications](/handbook/sales/field-communications/) - Consultation on communication plan
* The update should leverage these communication channels: 
    * [#field-fyi Slack channel](/handbook/sales/sales-google-groups/field-fyi-channel/) - For asynchronous communication that gives context around the update and shares any relevant resources. 
        * Note: Tier 2 announcements will be prioritized after Tier 1 announcements – i.e. if a Tier 1 announcement needs to go out, the Tier 2 announcement will be held until the next business day. (For more information, see [best practices.](/handbook/sales/sales-google-groups/field-fyi-channel/))
    * [WW Field Sales Call](/handbook/sales/#bi-weekly-monday-sales-call) - For synchronous communication that includes only a brief mention to amplify the update and points the field to where to direct questions. 
    * Segment Updates for Field Leaders - To arm managers with the necessary information to discuss the update with their teams. 
    * [Field Flash newsletter](/handbook/sales/field-communications/field-flash-newsletter/) - To reiterate/recap the news and link to enablement resources. 
        * Note: Tier 2 announcements will be prioritized after Tier 1 announcements – i.e. if there are too many updates to include, field comms will highlight Tier 1 announcements first. 
    * Handbook - To maintain a single-source of truth, evergreen reference guide. 
* Considerations: 
    * Blackout restrictions applied for end-of-quarter. Tier 2 announcements not permitted in last two weeks of quarter.
    * The primary spokespeople for this update should be the relevant DRI. 
    * Since Tier 2 updates typically do not include enablement webinars or other live demo opportunities, DRIs should consider doing a short, asynchronous video recording to share with their update. 
        * i.e. Screenshare of the steps to take when implementing a new process. 

### Practical Application
Sales Ops has created a new tag that the field must start using in all opportunities in SFDC. They engage Field Enablement to update SQS materials and Field Communication to ensure that this Wednesday is available to communicate the update. Sales Ops records a quick, 3-minute screenshare briefly explaining the change and showing where to find it in SFDC. 

When the change goes live, Sales Ops posts a message in field-fyi on the agreed-upon date sharing the pre-recorded video as well as any new/updated Handbook links. They add mention of it to the WW Field Sales Call agenda. Field Communications includes the update in the update for sales leaders and in the Field Flash newsletter. 

## Tier 3 Update
This update has medium-low urgency and is not directly related to selling. It is likely relevant to only a niche segment of the field organization and/or is a more general business update that is not time sensitive. 

Examples: Outside (non-field) requests for feedback/input, team member changes, general team questions

### Qualifying Questions 
* Is this update a “nice to know” vs. “need to know”? 
* Does this update only apply to a small group of people within the field organization?
* Is this update a more general update, request, or question? 

### Communications Playbook 
* Cadence for Tier 3 updates should be one-dimensional: 
    * Next available opportunity on a singular communication channel 
* DRIs should engage these teams for input:
    * [Field Communications](/handbook/sales/field-communications/) - Heads-up on an update and/or request for help amplifying 
* The update should leverage only these communication channels so as to not create unnecessary noise:  
    * #sales general Slack channel - For asynchronous communication that gives context around the update or question. 
    * Handbook - To maintain a single-source of truth, evergreen reference guide. 
* Considerations: 
    * Blackout restrictions applied for end-of-quarter or noisy days. Tier 3 announcements not permitted in last two weeks of quarter or on days when 1-2 other higher-impact updates (Tier 1 and/or Tier 2) have gone out.
    * The primary spokespeople for this update should be the relevant DRI. 

#### Field Communications Playbook Flowchart
![field-communications-playbook-flowchart](/handbook/sales/field-communications/field-communications_playbook-flowchart.png)

[(Source file)](https://drive.google.com/drive/u/0/search?q=communication%20playbook%20flowchart)

#### Field Communications Channel Map
![field-communications-channel-map](/handbook/sales/field-communications/field-communications-channel-map.png)

[(Source file)](https://drive.google.com/drive/u/0/search?q=field%20communications%20channel%20map)

### Requesting Field Announcements
Once you've reviewed the playbook above and would like to request Field Communications support for a field-related announcement, please follow this process: 
- Submit an issue using [this issue template](https://gitlab.com/gitlab-com/sales-team/field-operations/enablement/-/issues/new?issuable_template=field-announcement-request)
   - Please provide as much context about your announcement as possible, specifically around the urgency and relevance to the field. Provide a recommended update tier and target communication channel(s) using the guidance in the playbook above. 
   - A placeholder tag `Field Announcement::Triage` will be applied to the issue. 
- Field Communications will triage the request as quickly as possible. Upon examining the request, they will assign it to a communication tier using one of three tags:
   1. `Field Announcement::Tier 1` 
   1. `Field Announcement::Tier 2` 
   1. `Field Announcement::Tier 3` 
- Field Communications will collaborate with the requester on the announcement details directly in the issue to finalize the roll-out plan. 

## Key Field Communications Programs
- [Field Flash Newsletter](/handbook/sales/field-communications/field-flash-newsletter/)
- [GitLab Monthly Release Email to Sales](/handbook/sales/field-communications/monthly-release-sales-email)

## Sharing Feedback
Ongoing feedback and participation from the field is imperative to the success of the Field Communications team and its programs. If you have feedback on the current processes or programs, requests for a certain type of content, and/or ideas for ways we can further improve communication with the field, please follow this process: 

- To share feedback or ideas, submit an issue using [this issue template](https://gitlab.com/gitlab-com/sales-team/field-operations/enablement/-/issues/new?issuable_template=field-communications-feedback)
- Field Communications will triage feedback as soon as possible, given availability and bandwidth. Additional stakeholders and subject matter experts will be pulled in as appropriate.
- During this review, Field Communications will determine next-steps for each issue submitted, placing it into one of three categories: 
   1. Accepted (tag: `FC Feedback::Accepted`) - Field Communications feedback that will be actioned on
   1. Deferred (tag: `FC Feedback::Deferred`) - Field Communications feedback that will be deferred until more information is gathered or until more bandwidth becomes available
   1. Declined (tag: `FC Feedback::Declined`) - Field Communications feedback that is declined (no action will be taken)
- Field Communications will respond to each issue with rationale behind its disposition and will keep issue stakeholders updated on progress or completion if the issue is accepted or deferred.


