---
layout: handbook-page-toc
title: "Sales Territories"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Process to Request Update 

### Territory Ownership (Sales) 

1. Create an issue in the **Sales Operations** project - utilizing the [Territory Change Request template](https://gitlab.com/gitlab-com/sales-team/field-operations/sales-operations/issues/new?issuable_template=Territory_Change_Request)
1. Follow the directions within the template & provide all the requested details
     - If **Individual Contributor** is requesting the change, ADD your manager to the `/assign` command
     - If **Manager** is requesting change, submit issue & it will auto-assign to Sales Ops
     - **Please note** Operations makes alignment changes **once** at the end of each month, only exception would be a Sales New Hire. 
1. `@tav_scott` to update SFDC
1. Change made on Territory Management document by `@tav_scott` **after** change in system has been made.
1. `@bethpeterson` to update LeanData download updated csv once change confirmed by `@tav_scott`.
1. Territory Management updates will be uploaded to LeanData by `@bethpeterson` **after** change in system has been made. 

### SDR Alignment (Marketing)

1. Create an merge request
1. Update `SDR` column with name of new SDR to cover territory.
     - If **Individual Contributor**, assign the merge request to your direct Manager for approval prior to assigning it to MktgOps
     - If **Manager**, assign the merge request to MktgOps
1. MktgOps processes these reqeusts on a **monthly** basis. 

#### Updating these tables without updating Operations will not be reflected in our various systems causing all reports and routing to be incorrect!   
{:.no_toc}

**Questions?** Ask in `#sales` slack channel pinging `@sales-ops`. 



## Region/Vertical
{:.no_toc}
  * **VP Commercial Sales** ([Mid-Market](#mid-market-segment) & [Small Business](#small-business-segment)): Ryan O'Nell
  * **[APAC](#apac)**: Anthony McMahon, Regional Director
  * **[Europe, Middle East and Africa](#emea)**: Jon Burghart, Regional Director
  * **[North America - US East](#us-east)**: Mark Rogge, Regional Director
  * **[North America - US West](#us-west)**: Haydn Mackay, Regional Director
  * **[Public Sector](#public-sector)**: Paul Almeida, Director of Federal Sales

## Territories

### Large

#### AMER

For the United States, the following rules apply to all accounts **except** government agencies or publicly-funded educational institutions or departments, including those at private universities (Johns Hopkins Applied Physics Lab, for example). Government agencies and publicly-funded educational institutions will be managed by our [Public Sector](#public-sector) team.

For other countries outside AMER, governments agencies will be handled by the territory owner.

**Named Account identifiers will be updated in near future and currently are being worked on by the SalesOps team (status as of 2020-01-23)**

##### Area Sales Manager
{:.no_toc}

* **NA East - Named Accounts**: Adam Johnson
* **NA East - South East**: Tom Plumadore
* **NA East - North East**: Sheila Walsh
* **NA West - Rockies/SoCal**: James Roberts
* **NA West - Bay Area**: Alan Cooke
* **NA West - PNW/MidWest**: Timm Ideker

| Sub-Region     | Area           | **Territory Name**                     | Sales              | SDR                |
| :------------- | :------------- | :------------------------------------- | :----------------- | :----------------- |
| NA East        | Northeast      | **Large-AMER-Eastern Canada**          | Peter McCracken    | Aashish Sharma     |
| NA East        | Northeast      | **Large-AMER-Ontario**                 | Peter McCracken    | Aashish Sharma     |
| NA East        | Northeast      | **Large-AMER-MA**                      | Tony Scafidi       | Bill Zaferopolos   |
| NA East        | Northeast      | **Large-AMER-Manhattan**               | Liz Corring        | Max Chadliev       |
| NA East        | Northeast      | **Large-AMER-New England**             | Tony Scafidi       | Bill Zaferopolos   |
| NA East        | Northeast      | **Large-AMER-Mid-Atlantic**            | Katherine Evans    | Kelsey Steyn       |
| NA East        | Northeast      | **Large-AMER-NY/NJ**                   | Paul Duffy         | Andrew Glidden     |
| LATAM          | Southeast      | **Large-AMER-Brazil**                  | Jim Torres         | Bruno Lazzarin     |
| LATAM          | Southeast      | **Large-AMER-Chile**                   | Jim Torres         | Bruno Lazzarin     |
| LATAM          | Southeast      | **Large-AMER-Mexico**                  | Carlos Dominguez   | Bruno Lazzarin     |
| LATAM          | Southeast      | **Large-AMER-SoCenAmer**               | Jim Torres         | Bruno Lazzarin     |
| NA East        | Southeast      | **Large-AMER-Central Gulf Carolinas**  | Chris Graham       | Shakarra MCGuire   |
| NA East        | Southeast      | **Large-AMER-SunshinePeach**           | Jim Bernstein      | Bill Zaferopolos   |
| NA East        | East Central   | **Large-AMER-Lake Michigan**           | Tim Kuper          | Marcus Stangl      |
| NA East        | East Central   | **Large-AMER-Ohio Valley**             | Ruben Govender     | Morgen Smith       |
| NA East        | East Central   | **Large-AMER-TOLA**                    | Matt Petrovick     | Brandon Brooks     |
| NA East |  | **Named Accounts** | Mark Bell | Steven Cull |
| NA East |  | **Named Accounts** | John May | Ryan Kimball |
| NA East |  | **Named Accounts** | Jordan Goodwin | Marcus Stangl |
| NA East |  | **Named Accounts** | David Wells | Kaleb Hill |
| NA East |  | **Named Accounts** | Larry Biegel | Geraldine Lee |
| NA East |  | **Named Accounts** | John Orvos | Ryan Kimball |
| NA West        | Midwest        | **Large-AMER-Midwest**                 | Timmothy Ideker*   | TBD                |
| NA West        | Midwest        | **ENT-MW-Named 1**                     | Philip Wieczorek   | Paul Oakley        | 
| NA West        | Midwest        | **ENT-MW-Named 2**                     | Timmothy Ideker*   | TBD                |
| NA West        | PNW            | **ENT-PNW-Named 1**                    | Joe Drumtra        | Eduardo Gonzalez   |
| NA West        | PNW            | **ENT-PNW-Named 2**                    | Chris Mayer        | Suzy Verdin        |
| NA West        | PNW            | **Large-AMER-PNW**                     | Timmothy Ideker*   | TBD                |
| NA West        | Southwest      | **Large-AMER-Southwest**               | Rick Walker        | Blake Chalfant-Kero|
| NA West        | Southwest      | **Named Accounts**                     | Chris Cornacchia   | Jesse Muehlbauer   |
| NA West        | Southwest      | **Named Accounts**                     | Yvonne Zwolinski   | Blake Chalfant-Kero|
| NA West        | Southwest      | **Named Accounts**                     | John Williams      | James Altheide     |
| NA West        | SoCal          | **Large-AMER-SoCal**                   | James Roberts*     | Matthew MacFarlane |
| NA West        | SoCal          | **Named Accounts**                     | Robert Hyry        | Jesse Muehlbauer   |
| NA West        | SoCal          | **Named Accounts**                     | Brad Downey        | Matthew MacFarlane |
| NA West        | NorCal         | **ENT-NC-Named SF1**                   | Moses Mederos      | TBD                |
| NA West        | NorCal         | **ENT-NC-Named SF2**                   | Mike Nevolo        | James Altheide     |
| NA West        | NorCal         | **ENT-NC-Named Santa Clara 1**         | Nico Ochoa         | Madison Taft       |
| NA West        | NorCal         | **ENT-NC-Named Santa Clara 2**         | Joe Miklos         | TBD                |
| NA West        | NorCal         | **ENT-NC-Named Santa Clara 3**         | Robert van Leer    | Madison Taft       |
| NA West        | NorCal         | **ENT-NC-Named Santa Clara 4**         | Michael Scott        | Aaron Young        |
| NA West        | NorCal         | **Large-AMER-NorCal**                  | Alan Cooke*        | Isaac Mondesir     |

#### Public Sector

| Sub-Region    | **Territory Name**                         | Strategic Account Leader | Inside Sales Rep  |
| :------------ | :------------------------------------------| :----------------------- | :---------------- |
| Public Sector | **Federal - Civilian-2**                   | Susannah Reed        | Christine Saah    |
| Public Sector | **Federal - Civilian-3**                   | Luis Vazquez         | Bill Duncan       |
| Public Sector | **Federal - Civilian-5**                   | Joel Beck            | Nathan Houston    |
| Public Sector | **Federal - Civilian-6**                   | Matt Kreuch          | Christine Saah    |
| Public Sector | **Federal - Civilian-7**                   | Rick Gravel          | Nathan Houston    |
| Public Sector | **State and Local (SLED East)**            | Dan Samson           | Alexis Shaw       |
| Public Sector | **State and Local (SLED West)**            | Dave Lewis           | Victor Brew       |
| Public Sector | **State and Local (SLED Central)**         | Matt Stamper         | Victor Brew       |
| Public Sector | **State and Local (SLED South)**           | Mark Williams        | Alexis Shaw       |
| Public Sector | **Federal - DoD-Air Force-1**              | Matt Jamison         | Craig Pepper      |
| Public Sector | **Federal - DoD-Air Force-2**              | Dan Foley            | Craig Pepper      |
| Public Sector | **Federal - DoD-Air Force-3**              | Stan Brower          | Craig Pepper      |
| Public Sector | **Federal - DoD-Navy-1**                   | Chris Rennie (temp)  | Patrick Gerhold|
| Public Sector | **Federal - DoD-Navy-2**                   | Chris Rennie         | Patrick Gerhold    |
| Public Sector | **Federal - DoD-Army-1**                   | Allison Mueller      | Peg Sheridan |
| Public Sector | **Federal - DoD-Army-2**                   | Allison Mueller (temp)| Peg Sheridan |
| Public Sector | **Federal - DoD-Agencies**                 | Scott McKee          | Peg Sheridan      |
| Public Sector | **Federal - NSG-1**                        | Marc Kriz            | Joe Fenter        |
| Public Sector | **Federal - NSG-2**                        | Mike Sellers         | Joe Fenter       |
| Public Sector | **Federal - NSG-3**                        | Ian Moore            | Christine Saah    |
| Public Sector | **Federal - NSG-4**                        | Russ Wilson          | Bill Duncan        |
| Public Sector | **Federal - NSG-5**                        | John McLean          | Bill Duncan        |
| Public Sector | **Federal - NSG-6**                        | TBD                  | Christine Saah     |


#### APAC

| Sub-Region     | Area           | **Territory Name**                     | Sales              | SDR                |
| :------------- | :------------- | :--------------------------------- | :----------------- | :----------------- |
| ANZ            | ANZ            | **Large-APAC-AUS Northern Terriories** | Sarah Orell        | Glenn Perez        |
| ANZ            | ANZ            | **Large-APAC-AUS Queensland**          | Sarah Orell        | Glenn Perez        |
| ANZ            | ANZ            | **Large-APAC-AUS South Australia**     | Danny Petronio     | Belinda Singh      |
| ANZ            | ANZ            | **Large-APAC-AUS Sydney**              | Sarah Orell        | Glenn Perez        |
| ANZ            | ANZ            | **Large-APAC-AUS Victoria**            | Danny Petronio     | Belinda Singh      |
| ANZ            | ANZ            | **Large-APAC-AUS Western Australia**   | Rob Hueston        | Belinda Singh      |
| ANZ            | ANZ            | **Large-APAC-ACT**                     | Rob Hueston        | Belinda Singh      |
| ANZ            | ANZ            | **Large-APAC-North Sydney**            | Sarah Orell        | Glenn Perez        |
| ANZ            | ANZ            | **Large-APAC-NZ**                      | Sarah Orell        | Belinda Singh      |
| Asia Central   | Asia Central   | **Large-APAC-Central Asia**            | Rob Hueston        | Glenn Perez        |
| Asia Central   | Asia Central   | **Large-APAC-Kazakhstan**              | Rob Hueston        | Glenn Perez        |
| Asia SE        | Southeast Asia | **Large-APAC-Cambodia**                | Claudia Cheong     | Glenn Perez        |
| Asia SE        | Southeast Asia | **Large-APAC-Indonesia**               | Claudia Cheong     | Glenn Perez        |
| Asia SE        | Southeast Asia | **Large-APAC-Malaysia**                | Claudia Cheong     | Glenn Perez        |
| Asia SE        | Southeast Asia | **Large-APAC-Myanmar**                 | Claudia Cheong     | Glenn Perez        |
| Asia SE        | Southeast Asia | **Large-APAC-Philippines**             | Claudia Cheong     | Glenn Perez        |
| Asia SE        | Southeast Asia | **Large-APAC-Thailand**                | Claudia Cheong     | Glenn Perez        |
| Asia SE        | Southeast Asia | **Large-APAC-Viet Nam**                | Claudia Cheong     | Glenn Perez        |
| Asia SE        | Singapore      | **Large-APAC-Singapore**               | Claudia Cheong     | Glenn Perez        |
| Asia South     | India          | **Large-APAC-India**                   | Danny Petronio     | Belinda Singh      |
| China          | China          | **Large-APAC-China**                   | Danny Petronio     | Aletha Alfarania   |
| China          | Taiwan         | **Large-APAC-Taiwan**                  | Danny Petronio     | Aletha Alfarania   |
| Japan          | Japan          | **Large-APAC-Japan**                   | Rob Hueston        | Minsu Han          |
| Korea          | Korea          | **Large-APAC-Korea**                   | Woosang Lee        | Minsu Han          |


#### EMEA

| Sub-Region     | Area           | **Territory Name**                     | Sales              | SDR                |
| :------------- | :------------- | :--------------------------------- | :----------------- | :----------------- |
| Europe Central | Europe Central | **Large-EMEA-BeNeLux**                 | Nasser Mohunlol    | Shay Fleming  |
| Europe Central | Europe Central | **Large-EMEA-CH/AUS**                  | Thomas Bosshard    | Peter Kunkli       |
| Europe Central | Germany        | **Large-EMEA-Germany**                 | Rene Hoferichter   | Gábor Zaparkanszky |
| Europe East    | Europe East    | **Large-EMEA-Eastern Europe**          | Vadim Rusin        | Arianna Bellino    |
| Europe South   | Europe South   | **Large-EMEA-France**                  | Aleksandar Bosnic  | Magali Bressan     |
| Europe South   | Europe South   | **Large-EMEA-Italy**                   | Vadim Rusin        | Arianna Bellino    |
| Europe South   | Europe South   | **Large-EMEA-Portugal**                | Vadim Rusin        | Camilo Villanueva    |
| Europe South   | Europe South   | **Large-EMEA-Spain**                   | Vadim Rusin        | Camilo Villanueva    |
| UKI   | UKI   | **Large-EMEA-UKI**                     | Robbie Byrne       | UKI Large^          |
| MEA            | MEA            | **Large-EMEA-MEA**                     | MEA Large Sales^^    | Camilo Villanueva    |
| Nordics        | Nordics        | **Large-EMEA-Nordics**                 | Annette Kristensen | Camilo Villanueva  |
|  |  | **Named Accounts** | Justin Haley | Chris Loudon |
|  |  | **Named Accounts** | Hugh Christey | Magali Bressan |
|  |  | **Named Accounts** | Timo Schuit | Peter Kunkli |
|  |  | **Named Accounts** | Phillip Smith | Camilo Villanueva |

^ `SDR` = `UKI Large`: Round robin group consisting of Chris Loudon, Daniel Phelan, Shay Fleming      
^^ `Sales` = `MEA Large Sales`: Round robin group consisting of Vadim Rusin, Phillip Smith      

### Mid-Market

#### AMER

| Sub-Region     | Area           | **Territory Name**                  | Sales                         | SDR              |
| :------------- | :------------- | :------------------------------ | :---------------------------- | :--------------- |
| LATAM          | LATAM          | **MM-AMER-EAST-LATAM**              | Romer Gonzalez                | Bruno Lazzarin   |
| NA East        | US East        | **MM-AMER-EAST-MidSoAtlantic**      | Jeff Lackey                   | Evan Mathis      |
| NA East        | US East        | **MM-AMER-EAST-NorthCentral**       | Kyla Gradin                   | Phillip Knorr    |
| NA East        | US East        | **MM-AMER-EAST-Northeast**          | Todd Lauver                   | Kelsey Claflin   |
| NA East        | US East        | **MM-AMER-EAST-NYPa**               | Steve Xu                      | Phillip Knorr    |
| NA East        | US East        | **MM-AMER-EAST-Southeast**           | Daniel Parry                  | Evan Mathis      |
| NA East        | US East        | **MM-AMER-EAST-TriState**            | Jenny Kline                   | Kelsey Claflin   |
| NA West        | US West        | **MM-AMER-WEST-MtnMidwest**         | Leigh Motheral                | Ariah Curtis     |
| NA West        | US West        | **MM-AMER-WEST-NorCal**             | Jennifer MacVicar             | Da'Neil Olsen |
| NA West        | US West        | **MM-AMER-WEST-PacWest**            | Rashad Bartholomew            | Josh Weatherford |
| NA West        | US West        | **MM-AMER-WEST-SF**                 | Laura Shand                   | Ariah Curtis     |
| NA West        | US West        | **MM-AMER-WEST-SoCal/AZ/HI**        | Douglas Robbin                | Da'Neil Olsen    |
| NA West        | US West        | **MM-AMER-WEST-Southwest**          | Scott Larson                  | Josh Weatherford |
| NA West        | US West        | **MM-AMER-Named 1**                 | Chris Chiappe                 | Da'Neil Olsen |
| NA West        | US West        | **MM-AMER-Named 2**                 | Matt Doerfleur                | Josh Weatherford |
| NA East        | US East        | **MM-AMER-Named 3**                 | Alyssa Belardi                | Gee Lee |
| NA East        | US East        | **MM-AMER-Named 4**                 | Sharif Bennett                | Shakarra McGuire |


#### APAC

| Sub-Region     | Area           | **Territory Name**                  | Sales                         | SDR              |
| :------------- | :------------- | :------------------------------ | :---------------------------- | :--------------- |
| ANZ            | ANZ            | **MM-APAC-ANZ**                     | Julie Manalo                  | Glenn Perez      |
| Asia Central   | Asia Central   | **MM-APAC-Central Asia**            | Julie Manalo                  | Glenn Perez      |
| China          | China          | **MM-APAC-China**                   | Julie Manalo                  | Aletha Alfarania |
| Japan          | Japan          | **MM-APAC-Japan**                   | Julie Manalo                  | Minsu Han        |
| Asia SE        | Southeast Asia | **MM-APAC-SE Asia**                 | Julie Manalo                  | Aletha Alfarania |
| Asia South     | Asia South     | **MM-APAC-South Asia**              | Ishan Padgotra / Julie Manalo | Belinda Singh    |
| Korea          | Korea          | **MM-APAC-South Korea**             | Julie Manalo                  | Minsu Han        |


#### EMEA

| Sub-Region           | Area    | **Territory Name**             | Sales                     | Outbound SDR     |
| :------------------- | :------ | :----------------------------- | :------------------------ | :--------------- |
| UKI                  | GB      | **MM-EMEA-UKI-London E**       | Chris Willis              | Kristof Eger.    |
| UKI                  | GB      | **MM-EMEA-UKI-London EC**      | Chris Willis              | Kristof Eger     |
| UKI                  | GB      | **MM-EMEA-UKI-London N**       | Chris Willis              | Kristof Eger     |
| UKI                  | GB      | **MM-EMEA-UKI-London NW**      | Chris Willis              | Kristof Eger     |
| UKI                  | GB      | **MM-EMEA-UKI-London SE**      | Anthony Ogunbowale-Thomas | Kristof Eger     |
| UKI                  | GB      | **MM-EMEA-UKI-London SW**      | Anthony Ogunbowale-Thomas | Kristof Eger     |
| UKI                  | GB      | **MM-EMEA-UKI-London W**       | Anthony Ogunbowale-Thomas | Kristof Eger     |
| UKI                  | GB      | **MM-EMEA-UKI-London WC**      | Anthony Ogunbowale-Thomas | Kristof Eger     |
| UKI                  | GB      | **MM-EMEA-UKI-GB**             | Conor Brady               | Kristof Eger     |
| UKI                  | IE      | **MM-EMEA-UKI-Ireland**        | Conor Brady               | Kristof Eger     |
| UKI                  | Rest Of | **MM-EMEA-UKI-R**              | Conor Brady               | Kristof Eger     |
| UKI.                 | Rest Of | **MM-EMEA-UKI-R**              | Conor Brady               | Kristof Eger     |
| France               | FR      | **MM-EMEA-France-FR 0x-6x**    | Israa Mahros              | Camille Dios     |
| France               | FR      | **MM-EMEA-France-FR 7x**       | Israa Mahros              | Camille Dios     |
| France               | FR      | **MM-EMEA-France-FR 8x-9x**    | Israa Mahros              | Camille Dios     |
| France               | FR      | **MM-EMEA-France-FR 8x-9x**    | Israa Mahros              | Camille Dios     |
| France               | Rest Of | **MM-EMEA-France-R**           | Israa Mahros              | Camille Dios     |
| Nordics              | FI      | **MM-EMEA-Nordics-FI**         | Hans Frederiks            | Johan Rosendahl  |
| Nordics              | NO      | **MM-EMEA-Nordics-NO**         | Hans Frederiks            | Johan Rosendahl  |
| Nordics              | DK      | **MM-EMEA-Nordics-DK**         | Hans Frederiks            | Johan Rosendahl  |
| Nordics              | SE      | **MM-EMEA-Nordics-SE**         | Hans Frederiks            | Johan Rosendahl  |
| Nordics              | Rest Of | **MM-EMEA-Nordics-R**          | Hans Frederiks            | Johan Rosendahl  |
| Benelux              | NL      | **MM-EMEA-Benelux-NL 10x-19x** | Hans Frederiks            | Johan Rosendahl  |
| Benelux              | NL      | **MM-EMEA-Benelux-NL 20x-29x** | Hans Frederiks            | Johan Rosendahl  |
| Benelux              | NL      | **MM-EMEA-Benelux-NL 30x-39x** | Hans Frederiks            | Johan Rosendahl  |
| Benelux              | NL      | **MM-EMEA-Benelux-NL 40x-99x** | Hans Frederiks            | Johan Rosendahl  |
| Benelux              | BE/LU   | **MM-EMEA-Benelux-BeLu**       | Lisa VdKooij              | Camille Dios     |
| Central Europe       | DE      | **MM-EMEA-Central-DE 0x**      | Chris Willis              | Rahim Abdullayev |
| Central Europe       | DE      | **MM-EMEA-Central-DE 1x**      | Chris Willis              | Rahim Abdullayev |
| Central Europe       | DE      | **MM-EMEA-Central-DE 2x**      | Conor Brady               | Rahim Abdullayev |
| Central Europe       | DE      | **MM-EMEA-Central-DE 3x**      | Chris Willis              | Rahim Abdullayev |
| Central Europe       | DE      | **MM-EMEA-Central-DE 4x**      | Chris Willis              | Rahim Abdullayev |
| Central Europe       | DE      | **MM-EMEA-Central-DE 5x**      | Chris Willis              | Rahim Abdullayev |
| Central Europe       | DE      | **MM-EMEA-Central-DE 6x**      | Chris Willis              | Rahim Abdullayev |
| Central Europe       | DE      | **MM-EMEA-Central-DE 7x**      | Anthony Ogunbowale-Thomas | Rahim Abdullayev |
| Central Europe       | DE      | **MM-EMEA-Central-DE 8x**      | Anthony Ogunbowale-Thomas | Rahim Abdullayev |
| Central Europe       | DE      | **MM-EMEA-Central-DE 9x**      | Anthony Ogunbowale-Thomas | Rahim Abdullayev |
| Central Europe       | AT      | **MM-EMEA-Central-AT**         | Conor Brady               | Kristof Eger     |
| Central Europe       | LI      | **MM-EMEA-Central-LI**         | Conor Brady               | Kristof Eger     |
| Central Europe       | CH      | **MM-EMEA-Central-CH**         | Conor Brady               | Kristof Eger     |
| Russia               | RU      | **MM-EMEA-Russia**             | Chris Willis              | Rahim Abdullayev |
| Southern Europe      | IL      | **MM-EMEA-Southern-IL**        | Anthony Ogunbowale-Thomas | Johan Rosendahl  |
| Southern Europe      | IT      | **MM-EMEA-Southern-IT**        | Anthony Ogunbowale-Thomas | Johan Rosendahl  |
| Southern Europe      | ES      | **MM-EMEA-Southern-ES**        | Anthony Ogunbowale-Thomas | Johan Rosendahl  |
| Southern Europe      | PT      | **MM-EMEA-Southern-PT**        | Anthony Ogunbowale-Thomas | Johan Rosendahl  |
| Southern Europe      | Rest Of | **MM-EMEA-Southern-R**         | Anthony Ogunbowale-Thomas | Johan Rosendahl  |
| Eastern Europe       | All     | **MM-EMEA-Eastern Europe**     | Israa Mahros              | Rahim Abdullayev |
| MEA                  | AE      | **MM-EMEA-MEA-AE**             | Israa Mahros              | Johan Rosendahl  |
| MEA                  | SA      | **MM-EMEA-MEA-SA**             | Israa Mahros              | Johan Rosendahl  |
| MEA                  | Rest Of | **MM-EMEA-MEA-R**              | Israa Mahros              | Johan Rosendahl  |
| Africas              | All     | **MM-EMEA-Africas**            | Israa Mahros              | Johan Rosendahl  |

^ Through 2020-05-31 Inbound SDR matches Outbound SDR; effictive 2020-06-01 Inbound SDR = `EMEA Commercial - MM`: a Round robin group consisting of the listed Outbound SDR resources.


### SMB

#### AMER

| Sub-Region     | Area           | **Territory Name**           | Sales                            | SDR                   |
| :------------- | :------------- | :----------------------- | :------------------------------- | :-------------------- |
| LATAM          | Brazil         | **SMB-AMER-Brazil**          | Romer Gonzalez                   | Bruno Lazzarin |
| LATAM          | Rest of LATAM  | **SMB-AMER-SoCenAmer**       | Romer Gonzalez                   | Bruno Lazzarin |
| NA East        | US East        | **SMB-AMER-Florida**         | Anthony Feldman                  | AMER Commercial - SMB^ |
| NA East        | US East        | **SMB-AMER-Quebec**          | Anthony Feldman                  | AMER Commercial - SMB^ |
| NA East        | US East        | **SMB-AMER-Ontario**         | Anthony Feldman                  | AMER Commercial - SMB^ |
| NA East        | US East        | **SMB-AMER-Great Lakes**     | Anthony Feldman                  | AMER Commercial - SMB^ |
| NA East        | US East        | **SMB-AMER-Illinois**        | Kaley Johnson                    | AMER Commercial - SMB^ |
| NA East        | US East        | **SMB-AMER-Northeast**       | Michael Miranda / Matt Walsh     | AMER Commercial - SMB^ |
| NA East        | US East        | **SMB-AMER-Mid-Atlantic**    | Matt Walsh                       | AMER Commercial - SMB^ |
| NA East        | US East        | **SMB-AMER-Southeast**       | Kaley Johnson                    | AMER Commercial - SMB^ |
| NA West        | US West        | **SMB-AMER-Mountain**        | Adam Pestreich / Michael Miranda | AMER Commercial - SMB^ |
| NA West        | US West        | **SMB-AMER-Northwest**       | Marsja Jones                     | AMER Commercial - SMB^ |
| NA West        | US West        | **SMB-AMER-Southwest**       | Adam Pestreich                   | AMER Commercial - SMB^ |
| NA West        | US West        | **SMB-AMER-Texas**           | Brooke Williamson                | AMER Commercial - SMB^ |
| NA West        | US West        | **SMB-AMER-Washington**      | Brooke Williamson                | AMER Commercial - SMB^ |
| TBD            | TBD            | **SMB-AMER-TBD**             | Carrie Nicholson                 | AMER Commercial - SMB^ |

^ `SDR` = `AMER Commercial - SMB`: Round robin group consisting of Jenny Chapman and Jada Rogers


#### APAC

| Sub-Region     | Area           | **Territory Name**           | Sales                            | SDR                   |
| :------------- | :------------- | :----------------------- | :------------------------------- | :-------------------- |
| ANZ            | ANZ            | **SMB-APAC-ANZ**             | Wayne Zhao                       | Belinda Singh         |
| Asia Central   | Asia Central   | **SMB-APAC-Central Asia**    | Wayne Zhao                       | Glenn Perez           |
| China          | China          | **SMB-APAC-China**           | Wayne Zhao                       | Aletha Alfarania      |
| Japan          | Japan          | **SMB-APAC-Japan**           | Wayne Zhao                       | Minsu Han             |
| Korea          | Korea          | **SMB-APAC-Korea**           | Wayne Zhao                       | Minsu Han             |
| Asia SE        | Southeast Asia | **SMB-APAC-SE Asia**         | Ishan Padgotra                   | Aletha Alfarania      |
| Asia South     | Asia South     | **SMB-APAC-South Asia**      | Ishan Padgotra                   | Minsu Han             |


#### EMEA

| Sub-Region     | Area           | **Territory Name**           | Sales                            | SDR                   |
| :------------- | :------------- | :----------------------- | :------------------------------- | :-------------------- |
| Europe Central | Europe Central | **SMB-EMEA-BeNeLux**         | Arina Voytenko                     | EMEA Commercial - SMB^ |
| Europe Central | DACH           | **SMB-EMEA-North Germany**   | Bastian van der Stel             | EMEA Commercial - SMB^ |
| Europe Central | DACH           | **SMB-EMEA-South Germany**   | Vilius Kavaliauskas              | EMEA Commercial - SMB^ |
| Europe Central | DACH           | **SMB-EMEA-Rest of DACH**    | Vilius Kavaliauskas              | EMEA Commercial - SMB^ |
| Europe East    | Eastern Europe | **SMB-EMEA-Eastern Europe**  | Arina Voytenko                     | EMEA Commercial - SMB^ |
| Europe South   | Europe South   | **SMB-EMEA-France**          | Ross Mawhinney                   | EMEA Commercial - SMB^ |
| Europe South   | Europe South   | **SMB-EMEA-Southern Europe** | Ross Mawhinney                   | EMEA Commercial - SMB^ |
| MEA            | MEA            | **SMB-EMEA-MEA**             | Vilius Kavaliauskas              | EMEA Commercial - SMB^ |
| Nordics        | Nordics        | **SMB-EMEA-Nordics**         | Arina Voytenko                     | EMEA Commercial - SMB^ |
| UKI            | UKI            | **SMB-EMEA-UKI**             | Ross Mawhinney                     | EMEA Commercial - SMB^ |

^ `SDR` = `EMEA Commercial - SMB`: Round robin group consisting of Wiam Aissaoui, Alexander Demblin, Dorde Sumenkovic and Johan Rosendahl
^ Nordics & UK&I are temporarily alignments while Daisy Miclat is on maternity leave

