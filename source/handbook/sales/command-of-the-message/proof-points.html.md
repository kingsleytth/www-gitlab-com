---
layout: handbook-page-toc
title: "Proof Points"
description: "GitLab proof points including customer references, case studies, industry awards, analyst reports and studies, GitLab reports and studies, and peer reviews"
twitter_image: '/images/tweets/research-panel.png'
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview
_[The original and internal-to-GitLab proof points Google Doc is online, here](https://docs.google.com/document/d/1Tuhg6LO0e6R-KP7Vfwb8cggHDiBzkC26pBl0lyLS7UY/edit) while some parts of this handbook page are being approved for publication._

Proof points with third-party validation are a key resource, whether from [customers](#customer-references-and-case-studies), [analysts](http://localhost:4567/handbook/sales/command-of-the-message/proof-points.html#analyst-reports-and-studies), [industry awards](http://localhost:4567/handbook/sales/command-of-the-message/proof-points.html#industry-awards), or [peer reviews](http://localhost:4567/handbook/sales/command-of-the-message/proof-points.html#peer-reviews), and they are augmented here with [Gitlab's own industry research](http://localhost:4567/handbook/sales/command-of-the-message/proof-points.html#gitlab-reports-and-studies).

Articulating value is a critical sales skill in achieving [Command of the Message](/handbook/sales/command-of-the-message/).

## GitLab Proof Points

### Customer References and Case Studies

#### Enterprise

<big>[Goldman Sachs](/customers/goldman-sachs/)</big><br>
**Problem:** Needed to increase developer efficiency and software quality<br>
**Solution:** GitLab Premium (CI/CD, SCM)<br>
**Result:** Improved from 1 build every two weeks to over a 1000/day, or releasing 6 times per day per developer, and an average cycle time from branch to merge is now 30 minutes; simplified workflow and simplified administration<br>
**Value Drivers:** Efficiency, Speed<br>
**Differentiators:** Single App<br>
**Usecase:** CI, CD, SCM, Simplify DevOps<br>
**Vertical:** Financial Services

----

<big>[Axway](/customers/axway/)</big><br>
**Problem:** Legacy SCM and complex toolchain limited worldwide collaboration<br>
**Solution:** GitLab Premium (SCM)<br>
**Result:** Result: 26x faster release cycle; from annual releases down to every two weeks<br>
**Value Drivers:** Speed<br>
**Differentiators:** Single App, SCM & CI<br>
**Usecase:** SCM<br>
**Vertical:** Computer Software<br>

----

<big>[Jaguar Land Rover](/blog/2018/07/23/chris-hill-devops-enterprise-summit-talk/)</big><br>
**Problem:** Slow delivery and release cycles, taking 4 to 6 weeks, leading to infrequent feedback for developers<br>
**Solution:** GitLab Premium (CI)<br>
**Result:** Increased delivery speed from 3-6 weeks to 30 minutes giving teams faster feedback<br>
**Value Drivers:** Efficiency<br>
**Usecase:** CI<br>
**Vertical:** Automotive<br>

----

<big>[Hemmersbach](/customers/hemmersbach/)</big><br>
**Problem:** Multiple tools and communication inefficiencies slowed application delivery<br>
**Solution:** GitLab Ultimate (Agile, CI, CD, SCM)<br>
**Result:** Increased build speed by 59x, 14.4% improvement in cycle time<br>
**Value Drivers:** Speed<br>
**Usecase:** CI, CD, SCM, Agile<br>
**Vertical:** Technology<br>

----

<big>[European Space Agency](/customers/european-space-agency/)</big><br>
**Problem:** Geographic separation led to software deployment that used to take weeks<br>
**Solution:** GitLab Starter (SCM, CI)<br>
**Result:** Toolchain simplified and blooming culture of collaboration; code deployed in minutes when it used to take weeks<br>
**Value Drivers:** Efficiency, Speed<br>
**Usecase:** CI, CD, SCM<br>
**Vertical:** Technology<br>

----

<big>[Siemens](/blog/2018/12/18/contributor-post-siemens/)</big><br>
**Problem:** Needed to improve and enhance their developer tools<br>
**Solution:** GitLab Premium (CI)<br>
**Result:** Actively contributing to GitLab project with upstream commits<br>
**Differentiators:** Open Source, Collaborative Customer Experience<br>
**Vertical:** Industrial Manufacturing<br>

----

<big>[CERN](/customers/cern/)</big><br>
**Problem:** Lack of large scale collaboration and visibility<br>
**Solution:** GitLab Starter (CI)<br>
**Result:** Over 12,000 users collaborate on GitLab, running 120,000 CI jobs each month<br>
**Differentiators:** SCM & CI, Open Source<br>
**Usecase:** CI, SCM<br>
**Vertical:** Science, Technology, and Education<br>

----

<big>[Ticketmaster](/blog/2017/06/07/continous-integration-ticketmaster/)</big><br>
**Problem:** Slow build process prevented innovation<br>
**Solution:** GitLab Premium (CI)<br>
**Result:** 15x faster - from over 2-hour builds to 8-minute builds; faster releases and better customer experiences with 5 star app reviews<br>
**Value Drivers:** Speed<br>
**Differentiators:** Single App, Kubernetes<br>
**Usecase:** CI<br>
**Vertical:** Applications Development<br>

----

<big>[Equinix](/customers/equinix/)</big><br>
**Problem:** Legacy developer tools prevented faster delivery<br>
**Solution:** GitLab Premium (SCM)<br>
**Result:** Consistent code reviews and visibility across the SDLC<br>
**Differentiators:** Visibility<br>
**Usecase:** SCM<br>
**Vertical:** Technology<br>

----

<big>[Ask Media Group](https://www.askmediagroup.com/microservices-in-practice/)</big><br>
**Problem:** Managing the process of building and deploying microservices<br>
**Solution:** GitLab Premium (CI)<br>
**Result:** Developers can immediately begin to contribute a new service, that can be deployed to AWS as soon as they start<br>
**Differentiators:** SW Anywhere<br>
_**Video:** [Ask Media Group shares with GitLab how they accomplished Cloud Native Transformation with an architecture leveraging GitLab Runners, Kubernetes, and more.](https://www.youtube.com/watch?time_continue=4&v=3ED5NrVoVzk)_<br>
**Usecase:** CI, Cloud Native<br>
**Vertical:** IT Services<br>

----

<big>[Fanatics](/customers/fanatics/)</big><br>
**Problem:** CI pipelines took up so much bandwidth, impacted ability to deliver new tools and features; spent inordinate amount of time on CI maintenance<br>
**Solution:** GitLab Premium (CI)<br>
**Result:** GitLab provided the stable CI that Fanatics was looking for, enabling the cloud team to focus on innovation instead of simply fighting fires<br>
**Value Drivers:** Efficiency<br>
**Differentiators:** Collaborative<br>
**Usecase:** CI<br>
**Vertical:** Retail<br>

----

<big>[Verizon Connect](https://www.youtube.com/watch?v=zxMFaw5j6Zs)</big><br>
**Problem:** Needed to implement automation to increase deployment speed<br>
**Solution:** GitLab Premium (CI, Agile)<br>
**Result:** Reduced deploy process to under 8 hours<br>
**Value Drivers:** Efficiency<br>
**Usecase:** CI, CD, SCM, GitOps, Agile<br>
**Vertical:** Telematics<br>

----

#### Mid-Market

<big>[Alteryx](/customers/alteryx/)</big><br>
**Problem:** Legacy developer tools slowed software delivery frequency<br>
**Solution:** GitLab Ultimate( CI, CD, Security, SCM)<br>
**Result:** Software builds now 30 minutes from over 3 hours, able to release 9 times a day<br>
**Value Drivers:** Efficiency<br>
**Differentiators:** Single App, SCM & CI, Security<br>
**Usecase:** CI, CD, SCM, DevSecOps<br>
**Vertical:** Technology<br>

----

<big>[Wag!](/blog/2019/01/16/wag-labs-blog-post/)</big><br>
**Problem:** Slow release process taking over 40 minutes<br>
**Solution:** GitLab Ultimate (CI, CD)<br>
**Result:** Release process reduced to 6 minutes, 8 releases per day with built-in security, and a full deployment pipeline to Amazon Elastic Container Service (ECS)<br>
**Value Drivers:** Efficiency, Risk<br>
**Differentiators:** Security, SW Anywhere, Kubernetes<br>
**Usecase:** CI, CD, GitOps<br>
**Vertical:** Applications Development<br>

----

<big>[EAB](/customers/EAB/)</big><br>
**Problem:** Slow builds, expensive both in cost and maintenance due to multiple tools<br>
**Solution:** GitLab Premium (SCM, CI)<br>
**Result:** Build time reduced from 6 hours to 20 minutes, reduced toolchain complexity, and lightweight maintenance<br>
**Usecase:** CI, SCM<br>
**Vertical:** Education<br>

----

<big>[Worldline](/customers/worldline/)</big><br>
**Problem:** Long cycle times and hard to collaborate between developers<br>
**Solution:** GitLab Community Edition (SCM, CI)<br>
**Result:** From 2 weeks to set up a new project to just minutes, 120x increase in code reviews<br>
**Value Drivers:** Efficiency, Risk<br>
**Usecase:** CI, SCM<br>
**Vertical:** Financial Services<br>

----

<big>[Paessler AG](/customers/paessler/)</big><br>
**Problem:** Unstable and fragile developer tools resulted in slow QA cycles and infrequent releases<br>
**Solution:** GitLab Premium (SCM, CI, CD)<br>
**Result:** 120x faster QA, 4x more frequent releases<br>
**Value Drivers:** Speed, Risk<br>
**Differentiators:** SCM & CI<br>
**Usecase:** CI, CD, SCM<br>
**Vertical:** Network Software<br>

----

<big>[Extra Hop Networks](/customers/extra-hop-networks/)</big><br>
**Problem:** Legacy developer tools prevented collaboration and faster delivery<br>
**Solution:** GitLab<br>
**Result:** Unified developers on a common platform, reduced the total number of tools, and eased the transition to CI/CD<br>
**Usecase:** CI, CD, SCM<br>
**Vertical:** IT Management<br>

----

<big>[BI Worldwide](/customers/bi_worldwide/)</big><br>
**Problem:** Legacy systems and a lack of security lead to deployment cycles of 18 months<br>
**Solution:** GitLab Ultimate<br>
**Result:** Daily releases and increased collaboration in single tool<br>
**Value Drivers:** Efficiency, Speed, Risk<br>
**Differentiators:** Security<br>
**Usecase:** CI, CD, DevSecOps, Simplify DevOps<br>
**Vertical:** Consulting<br>

----

#### SMB
<big>[iFarm](/customers/ifarm/)</big><br>
**Problem:** Desire to improve code quality and delivery speed with global development team<br>
**Solution:** GitLab Premium<br>
**Result:** Over 400 hours cut from ‘bug to resolution’ time, 7.5x faster engineering time per bug<br>
**Value Drivers:** Speed<br>
**Usecase:** SCM<br>
**Vertical:** Agriculture Technology<br>

----

<big>[Cloud Native Computing Foundation](/customers/cncf/)</big><br>
**Problem:** Needed new features in GitLab to meet their business and technical needs<br>
**Solution:** GitLab CE<br>
**Result:** Modified their version of GitLab to meet their specific requirements<br>
**Differentiators:** Open Source, Collaborative Customer Experience, Kubernetes<br>
**Usecase:** CI, CD, Cloud Native<br>
**Vertical:** Information Technology and Services<br>

----

<big>[Glympse](/customers/glympse/)</big><br>
**Problem:** Slow software development and delivery due to expensive and complex developer tool stack<br>
**Solution:** GitLab Gold (Security, Agile, CI, CD, SCM)<br>
**Result:** Teams over 2x more efficient, completing 50 days of work in 21 days; 8x faster deploys, from 4 hours to under 30 min; built-in security scanning making audit process easier<br>
**Value Drivers:** Risk<br>
**Differentiators:** SCM & CI, Security<br>
**Usecase:** CI, CD, SCM, DevSecOps, Simplify DevOps, Agile<br>
**Vertical:** Technology<br>


----

<big>[ANWB](/customers/anwb/)</big><br>
**Problem:** Complex and fragile developer tools hindered productivity<br>
**Solution:** GitLab Premium (SCM, CI, CD)<br>
**Result:** Regular deploys, collaboration, and stable and reliable CI/CD pipelines<br>
**Usecase:** CI, CD, SCM<br>
**Vertical:** Not-for-Profit / Roadside Assistance<br>

----

<big>[OW2](/customers/ow2/)</big><br>
**Problem:** Enabling global open source collaboration<br>
**Solution:** GitLab<br>
**Result:** Easier to use platform that improves productivity<br>
**Usecase:** Agile<br>
**Vertical:** Computer Software<br>

----

<big>[Trek10](/customers/trek10/)</big><br>
**Problem:** Hard to collaborate across silos and with customers on cloud projects<br>
**Solution:** GitLab Starter (CI)<br>
**Result:** GitLab and its integrated CI helps seamlessly and securely manage deployments across many AWS accounts<br>
**Differentiators:** SW Anywhere<br>
**Usecase:** CI<br>
**Vertical:** IT Services<br>

----

<big>[Athlinks](/blog/2019/12/17/athlinks-cuts-runtime-in-half-with-giltab/)</big><br>
**Problem:** Jenkins and GH proved to be a complicated, expensive, and inefficient toolchain<br>
**Solution:** GitLab Ultimate (SCM, CI, CD)<br>
**Result:** CI runtimes cut in half for 50% improvements over Jenkins<br>
**Value Drivers:** Efficiency<br>
**Usecase:** CI, CD, SCM, Agile<br>
**Vertical:** Applications Development<br>

----

### Industry Awards
_[The original and internal-to-GitLab proof points Google Doc is online, here](https://docs.google.com/document/d/1Tuhg6LO0e6R-KP7Vfwb8cggHDiBzkC26pBl0lyLS7UY/edit) while some parts of this handbook page are being approved for publication._

#### 2019

<big>[IDC Innovators: Tools Supporting Open (Unopinionated) Developer Platforms](https://www.idc.com/getdoc.jsp?containerId=US45074219)</big><br>
_demonstrated either a groundbreaking business model or an innovative new technology or both_<br>
**Value Drivers:** Efficiency, Speed, Risk<br>
**Differentiators:** Rapid Dev


----

<big>[Axo Soft - Top 20 Dev Tools for 2019](https://blog.axosoft.com/top-developer-tools-2019/)</big><br>
_GitLab is one of the most popular developer tools._<br>
**Differentiators:** Open Source<br>
**Usecase** SCM, Agile

----

#### 2018

<big>[IDC Innovators: Agile Code Development Technologies](https://www.idc.com/getdoc.jsp?containerId=US43675318)</big><br>
_demonstrated either a groundbreaking business model or an innovative new technology or both_<br>
**Usecase** Agile

----

<big>[Google Cloud Partner Awards](/blog/2018/07/27/google-next-2018-recap/)</big><br>
_Innovative Solution in Developer Ecosystem for the tight integration with GKE_<br>
**Value Drivers:** Efficiency, Speed, Risk<br>
**Usecase** Cloud Native

----

<big>[GitLab voted as G2 Crowd Leader](/is-it-any-good/#gitlab-has-been-voted-as-g2-crowd-leader-in-2018)</big><br>
_Over 170 public reviews with a 4.4 rating, including: "[Powerful team collaboration tool for managing software development projects](https://www.g2.com/products/gitlab/reviews/gitlab-review-1976773)," "[Great self-hosted, open source source control system](https://www.g2.com/products/gitlab/reviews/gitlab-review-436746)," "[Efficient, can trace back, easy to collaborate](https://www.g2.com/products/gitlab/reviews/gitlab-review-701837)," and "[Perfect solution for cloud and on-premise DevOps tool](https://www.g2.com/products/gitlab/reviews/gitlab-review-2388492)"_<br>
**Value Drivers:** Efficiency, Speed, Risk<br>
**Differentiators:** Rapid Dev, Visibility<br>
**Usecase** SCM, CI, CD

----

#### 2017

<big>[CNCF - The 30 Highest Velocity Open Source Projects](/blog/2017/07/06/gitlab-top-30-highest-velocity-open-source/)</big><br>
_developer velocity can help illuminate promising areas in which to get involved, and what is likely to be the successful platforms over the next several years_<br>
**Differentiators:** Open Source, Rapid Dev<br>
**Usecase** GitOps, Cloud Native

----

### Analyst Reports and Studies
_[The original and internal-to-GitLab proof points Google Doc is online, here](https://docs.google.com/document/d/1Tuhg6LO0e6R-KP7Vfwb8cggHDiBzkC26pBl0lyLS7UY/edit) while some parts of this handbook page are being approved for publication._

#### 2019

<big>[Forrester: Manage Your Toolchain Before it Manages You](/resources/whitepaper-forrester-manage-your-toolchain/)</big><br>
_Multiple tools and toolchains create visibility, security, and productivity challenges for development and operations teams. “Out-of-the-box” toolchain solutions are seen as a solution, increasing security, revenue, and quality._<br>
**Value Drivers:** Efficiency, Speed, Risk<br>
**Differentiators:** Visibility<br>
**Usecase** SCM, CI, CD, Simplify DevOps

----

<big>[Forrester: Software Composition Analysis - Challenger](/analysts/forrester-sca/)</big><br>
_Software composition analysis tools evaluate applications to uncover vulnerabilities in 3rd party and open source components._<br>
**Usecase** DevSecOps

----

#### 2018

<big>[Forrester: Continuous Delivery and Release Automation - Contender](/analysts/forrester-cdra/)</big><br>
_Release automation tools enable faster, higher-quality, more automated software delivery through modeling applications, infrastructure, middleware, and their supporting installation processes and dependencies. “GitLab’s release automation is ideal for cloud-native, Kubernetes-centric organizations.”_<br>
**Value Drivers:** Efficiency, Speed<br>
**Usecase** CD

----

<big>[Forrester: Value Stream Management - Strong Performer](/analysts/forrester-vsm/)</big><br>
_Value stream management provides visibility into project planning, health indicators, and analytics to remove waste and focus on customer value._<br>
**Value Drivers:** Efficiency, Speed, Risk<br>
**Differentiators:** Visibility<br>
**Usecase** Simplify DevOps

----

#### 2017

<big>[Forrester: Continuous Integration Wave - Leader](/analysts/forrester-ci/)</big><br>
_Continuous Integration streamlines and accelerates building and testing developer code, shortening delivery to minutes rather than weeks and months. Continuous Integration also automates compliance tasks and improves auditability._<br>
**Value Drivers:** Efficiency, Speed, Risk<br>
**Usecase** CI


----

### GitLab Reports and Studies

<big>[2019 Global Developer Report: DevSecOps](/developer-survey/)</big><br>
**Findings Included:** 50% of developers agree that security vulnerabilities are mostly discovered by the security team after code is merged and in a test environment.<br>
**Customer Value:** Building security into the DevOps pipeline improves overall security.<br>
**Value Drivers:** Risk<br>
**Usecase** DevSecOps

----

<big>[2019 Global Developer Report: DevSecOps](/developer-survey/)</big><br>
**Findings Included:** 49% of respondents encounter the most delays during the testing stage of the development lifecycle.<br>
**Customer Value:** Automated testing in the CI pipeline accelerates delivery, finding defects earlier.<br>
**Value Drivers:** Speed<br>
**Usecase** DevSecOps

----

<big>[2019 Global Developer Report: DevSecOps](/developer-survey/)</big><br>
**Findings Included:** Ops teams are 1.8x more likely to believe they get sufficient notice to support the developer side when their DevOps practice is very good.<br>
**Customer Value:** Visibility and transparency into future work leads to more efficient planning and execution.<br>
**Value Drivers:** Efficiency<br>
**Usecase** DevSecOps**

----

<big>[2019 Global Developer Report: DevSecOps](/developer-survey/)</big><br>
**Findings Included:** Security teams are 3x more likely to discover bugs before code is merged with a good DevOps practice in place.<br>
**Customer Value:** Include security teams in all phases of application delivery — in the same tools as developers.<br>
**Value Drivers:** Risk<br>
**Usecase** DevSecOps

----

<big>[2018 Developer Report](/developer-survey/previous/2018/)</big><br>
**Findings Included:** The top two sources of project delays were testing (52%) and planning (47%).<br>
**Customer Value:** Operating in silos prevents visibility, and causes delays and rework.<br>
**Value Drivers:** Speed<br>
**Usecase** CI, CD, SCM, Simplify DevOps

----

### Peer Reviews
_[The original and internal-to-GitLab proof points Google Doc is online, here](https://docs.google.com/document/d/1Tuhg6LO0e6R-KP7Vfwb8cggHDiBzkC26pBl0lyLS7UY/edit) while some parts of this handbook page are being approved for publication._

"[Powerful team collaboration tool for managing software development projects](https://www.g2.com/products/gitlab/reviews/gitlab-review-1976773)" — Information Technologist, Enterprise from [G2 Crowd Leader](/is-it-any-good/#gitlab-has-been-voted-as-g2-crowd-leader-in-2018)<br>
**Value Drivers:** Efficiency<br>
**Use Case:** SCM

----

"[Perfect solution for cloud and on-premise DevOps tool](https://www.g2.com/products/gitlab/reviews/gitlab-review-2388492)" — Lead Developer, Mid-Market from [G2 Crowd Leader](/is-it-any-good/#gitlab-has-been-voted-as-g2-crowd-leader-in-2018)<br>
**Value Drivers:** Efficiency<br>
**Use Case:** DevOps, Cloud Native

----

"[Great self-hosted, open source source control system](https://www.g2.com/products/gitlab/reviews/gitlab-review-436746)" — Administrator, Mid-Market from [G2 Crowd Leader](/is-it-any-good/#gitlab-has-been-voted-as-g2-crowd-leader-in-2018)<br>
**Value Drivers:** Efficiency<br>
**Use Case:** SCM

----

"[Efficient, can trace back, easy to collaborate](https://www.g2.com/products/gitlab/reviews/gitlab-review-701837)" — Industry Analyst / Tech Writer, SMB from [G2 Crowd Leader](/is-it-any-good/#gitlab-has-been-voted-as-g2-crowd-leader-in-2018)<br>
**Value Drivers:** Efficiency<br>
**Use Case:** SCM
