---
layout: handbook-page-toc
title: "TPM.2.05 - Master Service Agreements"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# TPM.2.05 - Master Service Agreements

## Control Statement

Customer and vendor commitments and responsibilities are documented in a Master Service Agreement which is signed upon customer or vendor onboarding.

## Context

## Scope

## Ownership

Control Owner:

* `Legal`

Process Owner:

* Procurement
* Business Ops

## Guidance


## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Vendor Non-Disclosure Agreement control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/927).

### Policy Reference
* [Legal Handbook page](https://about.gitlab.com/handbook/legal/)
* [Procure to Pay process](https://about.gitlab.com/handbook/finance/procure-to-pay/)
* [Uploading Vendor Contract into ContractWorks process](/handbook/legal/vendor-contract-filing-process/)
* [Contract types](https://about.gitlab.com/handbook/finance/procurement/#sts=2:%20Purchase%20Type:%20Professional%20Services%20and%20all%20other%20contract%20types)
* [NetSuite Vendor Report](https://app.periscopedata.com/app/gitlab/557709/Vendor-Reporting)
* [Process for modifying sales contracts](https://about.gitlab.com/handbook/legal/customer-negotiations/)
* [Process for negotiating terms](https://about.gitlab.com/handbook/business-ops/order-processing/#process-for-agreement-terms-negotiations-when-applicable-and-contacting-legal)

## Framework Mapping

* SOC
 * CC2.3
