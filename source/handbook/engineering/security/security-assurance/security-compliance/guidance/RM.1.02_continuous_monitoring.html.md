---
layout: handbook-page-toc
title: "RM.1.02 - Continuous Monitoring Control Guidance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# RM.1.02 - Continuous Monitoring

## Control Statement

GitLab established common controls framework is evaluated for design and operating effectiveness no less than annually. Corrective actions related to identified deficiencies are tracked to resolution.

## Context

GitLab's controls aim to protect the confidentiality, integrity, and availability of financial, customer, GitLab team-member, and partner data and the service provided to them. To ensure the controls remain current and relevant, and they're both being used in the way they were intended and have the expected impact, they should be regularly evaluated and improved when necessary.

## Scope

This control applies to all controls in the GitLab Control Framework (GCF).

## Ownership

* Control Owner: `Security Compliance`
* Process owner(s):
    * Security Compliance: `100%` 

## Guidance

The design and operating effectiveness of GCF controls will be evaluated as part of the continuous control testing performed by the Security Compliance team. 

Control testing identifies areas where the design or operating effectiveness of a control is insufficient to satisfy the control objectives and any observations (aka findings) are validated with control owners who are then responsible for remediation. This control can be tested by verifying the documented [control test manual](https://gitlab.com/groups/gitlab-com/gl-security/compliance/-/epics/106) is followed and that a testing worksheet was executed for each GCF control for in scope systems.

The intent is for the results of GCF control tests to be relied upon by the Internal Audit team and external certification auditors to reduce the need frequent independent control assessments and therefore the burden on control owners. Less frequent independent assessment(s) may then be performed. 

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Continuous Monitoring control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/867).

Examples of evidence an auditor might request to satisfy this control:

* A documented process to test the design and operating effectiveness of internal controls through a control test
* Sample control test workpaper

### Policy Reference

* [GCF Security Control Testing Manual](https://gitlab.com/groups/gitlab-com/gl-security/compliance/-/epics/106)

## Framework Mapping

* SOC2 CC
  * CC5.1
