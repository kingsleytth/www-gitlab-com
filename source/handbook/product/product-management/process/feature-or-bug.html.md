---
layout: handbook-page-toc
title: Feature or Bug
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

GitLab product issues will often have one of the two [type labels](https://gitlab.com/gitlab-org/gitlab/-/blob/master/doc/development/contributing/issue_workflow.md#type-labels) `~bug` or `~feature`. There are also other higher precedence labels, as documented by [Engineering throughput](/handbook/engineering/management/throughput/#implementation)

In GitLab itself, there are short definitions of [feature](https://gitlab.com/groups/gitlab-org/-/labels/10230929/edit) (internal link) and [bug](https://gitlab.com/groups/gitlab-org/-/labels/2278648/edit) (internal link) which are displayed when hovering over the labels. This page provides context and elaborates on these definitions.

GitLab's [Iteration](/handbook/values/#iteration) value means we often make small improvements to the product. We use this to get feedback on what features are important to people. It is not a bug when GitLab is missing functionality.

## Documentation

If we find that GitLab doesn't work as people expect, the documentation should be updated so this is no longer a surprise. This applies whether we classify it as a feature request or a bug.

Link back to the issue so it's easy to find out more details, workarounds, to see what progress we're making, and to contribute.

## Bug issues

Bug issues report undesirable or incorrect behavior, such as:

* Defects in shipped code.
* Part of GitLab not working according to the documentation or a universal expectation.
* Functionality inadvertently being broken, or changed from how it is supposed to work. This is also a [regression](https://gitlab.com/gitlab-org/gitlab/-/blob/master/doc/development/contributing/issue_workflow.html.md#regression-issues).
* A [security issue that is determined to be a vulnerability](/handbook/engineering/security/#severity-and-priority-labels-on-security-issues) should be classified as `~bug`.
* Loss of data while using the product as intended or as documented. [Data corruption/loss is one basis](https://gitlab.com/gitlab-org/gitlab/-/blob/master/doc/development/contributing/issue_workflow.md#severity-labels) for classifying a bug as `S1`.

## Feature issues

Feature issues identify work to support the implementation of a feature and/or results in an improvement in the user experience.

* Whether the code results in user facing updates or not, if it is part of building the feature it should be labelled as such.
* Performance improvements and user interface enhancements improve the experience for end users and should be labelled as `~feature`.
* API additions including both REST and GraphQL should also be labelled as `~feature`.

If people care about a missing feature, then ideally the issue should be marked as `~"Accepting merge requests"`

## Resolving ambiguity

* If there is doubt about whether you could expect something to be there or work, it's a missing feature.
* We iterate to deliver features, so we often don't have functionality that people expect. For this reason, 'people could reasonably expect this functionality' does not make it a bug.
