---
layout: handbook-page-toc
title: "Brand and Digital Design Handbook"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Brand & Digital Handbook
{:.no_toc}

Brand and Digital is part of the Marketing Growth department, and is comprised of the Brand and Digital teams. Our mission is to create brand and marketing experiences that attract, convert, nurture and delight customers throughout the customer journey, one MVC at a time.


### Brand team

The Brand team’s primary responsibilities are:

- Developing and managing GitLabs brand and visual identity.
- Developing and managing campaign brands and identities.
- Enabling Everyone to Contribute with a consistent voice and look and feel.
- Content marketing design support, such as editorial and video content.

Quick Links:
- [Request support from Brand](#working-with-us)
- [Meet the Brand team](#meet-the-brand-team)
- [Brand standards and guidelines](#brand-guidelines)

    
### Digital team

The Digital team’s primary responsibilities are:

- Developing and maintaining [about.gitlab.com](https://about.gitlab.com/).
- Optimizing online marketing funnels and other digital experiences for conversion.

Quick Links:
- [Request support from Digital](#working-with-us)
- [Meet the Digital team](#meet-the-digital-team)
- [Website standards and guidelines](#guidelines-website)


### What we do

The Brand and Digital team works with our GitLab peers and the wider GitLab community to rapidly co-design and release value generating experiences throughout the marketing funnel -from raising brand awareness and attracting strangers, to converting visitors to leads, leads to customers and customers to promoters:

- **Attract** - Blog, Keywords, Social Media
- **Convert** - Forms, CTAs, Landing Pages
- **Nurture** - Ebooks, White-papers, Case Studies, Webinars, Guides, Videos, Email Newsletters and Drip Campaigns, Free Trials, Demos
- **Delight** - Feedback Loops, Personalization


### Our focus

The Brand and Digital team’s [mission](#mission), [work](#how-we-work), and solutions revolve around:

- **GitLab’s** **CREDIT Values** - We incorporate [Gitlab's CREDIT values](/handbook/values/#credit) into everything we do
- **Data-driven decisions**  - We use data to plan, design and measure success
- **Understanding the Problem** - We strive to understand the *who*, *what* and *why,* so we can best solve the how
- **Value-driven** **MVCs** - We rapidly plan, design and release results-driven MVCs, and measure our success
- **Customer focus** - We promote customer-centric strategies and solutions throughout the org
- **Conversion, conversion, conversion** - We optimize for conversion throughout the buyer journey
- **Story telling** - We build our brand through compelling story telling
- **Decisive Collaboration** - We facilitate an open and inclusive, decision-driven design process
- **Cohesive** **Experiences** - We build cohesive, omni-channel experiences across diverse media and devices, one MVC touchpoint at a time
- **Everyone Can Contribute** - We support GitLab’s Everyone Can Contribute model by creating contribution paths aligned to our brand and business goals
- **Efficiency via DRY** - In the spirit of Don’t Repeat Yourself (DRY), we leverage strategic reuse to promote consistency and speed
- **Lean UX** - We Lean UX prototyping to iterate and learn at 1/10th the cost of dev-based iteration, and mitigate development delays and rework


### Contact us

- [Slack](https://gitlab.slack.com/app_redirect?channel=marketing-brand-and-digital)
- [Email Brand and Digital](mailto:brand-and-digital@gitlab.com)

# Mission

Our mission is to rapidly release value generating MVCs, together. To realize our mission, we strive to be collaborative, innovative, informative, efficient, and results-driven in all things we do:

- **Innovative** - We emphasize GitLab’s disruptive business and product innovation, and embrace new ideas and ways to achieve our goals.
- **Informative** - We support evidence-based solutions via brand and conversion design best practices, competitive and user research, and customer feedback loops.
- **Efficient** - We solve efficiently “boring” solutions, Lean UX tools and techniques, and design reuse (e.g. design systems, reusable assets, and self-serve templates).
- **Results focused** - We deliver results fast by rapidly releasing value-generating, customer-centric MVCs and measuring success via performance indicators (PIs).
- **Collaborative** - We facilitate an open and iterative design process, that welcomes contributions from Gitlabbers and the wider GitLab community end to end.


# Working with us

To work with Brand and Digital, please:
- Use the following issue templates and labels to submit your issue requests
- Submit issue requests with adequate lead time (the sooner the better)
- Use Growth Boards and Brand and Digital Boards to track your request 

## Labels Overview
Please see the Growth Marketing Handbook section on [labels](https://about.gitlab.com/handbook/marketing/growth-marketing/#labels) for status and communication labels. Below is more detailed labels specific to Brand and Digital Design. 

## Brand issues & labels

#### Brand issues
To request support from Brand, please select the Brand issue template best aligned to your needs:

- [Brand general request](https://gitlab.com/gitlab-com/marketing/corporate-marketing/-/issues/new?issuable_template=design-request-general) - <description and lead time>
- [Brand content request](https://gitlab.com/gitlab-com/marketing/corporate-marketing/-/issues/new?issuable_template=design-request-content-resource) - <description and lead time>
- [Brand integrated campaign request](https://gitlab.com/gitlab-com/marketing/corporate-marketing/-/issues/new?issuable_template=design-requirements-integrated-campaign) - <description and lead time>
- [Brand team backlog refinement](https://gitlab.com/gitlab-com/marketing/corporate-marketing/-/issues/new?issuable_template=brand-and-digital-design-grooming) - <description and lead time>

Note: All new requests must be submitted via a Brand issue template.

#### Brand labels
When you use a Brand issue template, the `design` and `marketing status` labels are automatically added to your issue request. Please review your issue to ensure relevant labels are added:

-  `Design` 
    - If you create an issue where Brand services are the primary need, please use this label.
    - Helps us find and track issues relevant to the Design team
- Group labels (ex: `Strategic Marketing`, `Corporate Marketing`)
    - Denotes it could be part of a Growth Marketing Sub-team scope
    - Is not a label to denote the status of an issue
    - Note: These labels must exist in the root `GitLab.com` group or the `www-gitlab-com` repository
- Subject matter labels (ex: `blog`,  `SEO`)
    - Identifies relevant team(s) and subject matter(s)
    - Note: These labels must exist in the root `GitLab.com` group or the `www-gitlab-com` repository

## Digital issues & labels

#### Digital issues
To request support from Brand, please select the Brand issue template best aligned to your needs:

- [Website bug report](https://gitlab.com/gitlab-com/www-gitlab-com/issues/new?issuable_template=-website-bug-report) - <description and recommended lead time>
- [Team backlog refinement](https://gitlab.com/gitlab-com/marketing/corporate-marketing/-/issues/new?issuable_template=brand-and-digital-design-grooming) - <description and recommended lead time>
- [Home page promo request](https://gitlab.com/gitlab-com/marketing/growth-marketing/brand-and-digital/brand-and-digital/-/issues/new?issuable_template=website-homepage-promotion-request) - <description and recommended lead time>

#### Digital labels
When you use a Brand issue template, the `mktg-website` and `mtg-status::triage` labels are automatically added to your issue request. Please review your issue to ensure relevant labels are added:

-  `mktg-website`
    - If you create an issue where the Website services are the primary need, please use this label.
    - Helps us find and track issues relevant to the Design team
- Group labels (ex: `Strategic Marketing`, `Corporate Marketing`)
    - Denotes it could be part of a Growth Marketing Sub-team scope
    - Is not a label to denote the status of an issue
    - Note: These labels must exist in the root `GitLab.com` group or the `www-gitlab-com` repository
- Subject matter labels (ex: `blog`,  `SEO`)
    - Identifies relevant team(s) and subject matter(s)
    - Note: These labels must exist in the root `GitLab.com` group or the `www-gitlab-com` repository


# Tracking work

We use issue boards to track work that has been triaged by Growth Marketing and assigned to Brand and Digital by milestone.  We offer a variety of issue boards so everyone can easily track Brand and Digital work that is relevant to them:

- **All Marketing Tracking** - All work assigned to Brand and Digital
- **Group Tracking** - Ex: All Remote, Corporate Marketing
- **Specialty Tracking** - Ex: OKR, CMO, vendors and design-handbook

Note: Issues must have the appropriate labels to be visible and trackable by Brand and Digital and other stakeholders. 


| Team or subject         | Brand                                                          | Digital                                                          | Prioritization lead       |
| ----------------------- | -------------------------------------------------------------- | ---------------------------------------------------------------- | ------------------------- |
| All of Marketing        | [Brand](https://gitlab.com/groups/gitlab-com/-/boards/1511332) | [Digital](https://gitlab.com/groups/gitlab-com/-/boards/1483370) | Todd Barr                 |
| Account Based Marketing | [Brand](https://gitlab.com/groups/gitlab-com/-/boards/1690658) | [Digital](https://gitlab.com/groups/gitlab-com/-/boards/1691347) | Emily Luehrs              |
| All Remote              | [Brand](https://gitlab.com/groups/gitlab-com/-/boards/1571555) | [Digital](https://gitlab.com/groups/gitlab-com/-/boards/1485066) | Jessica Reeder            |
| Blog                    | [Brand](https://gitlab.com/groups/gitlab-com/-/boards/1571570) | [Digital](https://gitlab.com/groups/gitlab-com/-/boards/1483337) | Erica Lindberg            |
| Blocked                 | -                                                              | [Digital](https://gitlab.com/groups/gitlab-com/-/boards/1485169) | -                         |
| Brand & Digital Team    | [Brand](https://gitlab.com/groups/gitlab-com/-/boards/1485124) | [Digital](https://gitlab.com/groups/gitlab-com/-/boards/1485124) | Shane Bouchard            |
| CMO                     | -                                                              | [Digital](https://gitlab.com/groups/gitlab-com/-/boards/1486533) | Todd Barr                 |
| Content Marketing       | [Brand](https://gitlab.com/groups/gitlab-com/-/boards/1571561) | [Digital](https://gitlab.com/groups/gitlab-com/-/boards/1483354) | Erica Lindberg            |
| Corporate Marketing     | [Brand](https://gitlab.com/groups/gitlab-com/-/boards/1541149) | [Digital](https://gitlab.com/groups/gitlab-com/-/boards/1485085) | Danielle Morrill          |
| Design Handbook         | [Brand](https://gitlab.com/groups/gitlab-com/-/boards/1498563) | [Digital](https://gitlab.com/groups/gitlab-com/-/boards/1498563) | Shane Bouchard            |
| Diversity, Inclusion and Belonging | [Brand](https://gitlab.com/groups/gitlab-com/-/boards/1621520) | [Digital](https://gitlab.com/groups/gitlab-com/-/boards/1632756) | Candace Byrdsong Williams |
| Events                  | [Brand](https://gitlab.com/groups/gitlab-com/-/boards/1541174) | [Digital](https://gitlab.com/groups/gitlab-com/-/boards/1485090) | Emily Kyle                |
| Field Marketing         | [Brand](https://gitlab.com/groups/gitlab-com/-/boards/1541162) | [Digital](https://gitlab.com/groups/gitlab-com/-/boards/1533790) | Leslie Blanchard          |
| Marketing Ops           | -                                                              | [Digital](https://gitlab.com/groups/gitlab-com/-/boards/1485111) | Dara Wade                 |
| Marketing Programs      | [Brand](https://gitlab.com/groups/gitlab-com/-/boards/1571580) | [Digital](https://gitlab.com/groups/gitlab-com/-/boards/1580521) | Jackie Gragnola           |
| OKR                     | -                                                              | [Digital](https://gitlab.com/groups/gitlab-com/-/boards/1483333) | Shane Bouchard            |
| Social                  | [Brand](https://gitlab.com/groups/gitlab-com/-/boards/1571585) | [Digital](https://gitlab.com/groups/gitlab-com/-/boards/1485179) | Natasha Woods             |
| Strategic Marketing     | [Brand](https://gitlab.com/groups/gitlab-com/-/boards/1571417) | [Digital](https://gitlab.com/groups/gitlab-com/-/boards/1489415) | Ashish Kuthiala           |
| Talent Brand            | [Brand](https://gitlab.com/groups/gitlab-com/-/boards/1571573) | [Digital](https://gitlab.com/groups/gitlab-com/-/boards/1580510) | Betsy Church              |
| Technical Evangelism    | [Brand](https://gitlab.com/groups/gitlab-com/-/boards/1596489) | [Digital](https://gitlab.com/groups/gitlab-com/-/boards/1485182) | -                         |
| Vendor                  | [Brand](https://gitlab.com/groups/gitlab-com/-/boards/1511334) | [Digital](https://gitlab.com/groups/gitlab-com/-/boards/1502288) | Shane Bouchard            |


# How we work


### Project Management

#### Issue boards
Brand and Digital project planning, management and tracking is primarily facilitated throught our [issue boards](#Tracking-work). 

#### Roadmap planning

- Process Marketing Growth assignments
    - Marketing Growth triages all Brand and Digital issue requests.
    - Issues approved by Marketing Growth for Brand and Digital engagement are scheduled on the project roadmap based on priority
- Sprint Roadmap
    - Issues are assigned sprint milestones based on the week we expect to work on the issue (format is `Fri: Apr 17, 2020`), typically placed in future milestones
    - Issues milestone assignments may be changed to address new asks, blockers and or priority pivots 
    - If issues fall out of priority, they are placed in the Brand and Digital backlog
- Prioritization - In order to ensure we’re focused on what matters most, we prioritize our roadmap and MVC requirements based on:
    - Input from Growth Marketing
    - Input from other stakeholders
    - Project deadlines
    - ROI based on prioritization criteria:
        - **Common**
            - Frequency of use - How often is the single application diagram used? Web traffic to this page?
            - Number of people impacted - How many GitLabbers would benefit from this asset? How many unique users would benefit from this page?
        - **Critical**
            - High customer risk - If we don't do this, what are the risks to customers? How severe are those risks?
            - High business risk - If we don't do this, how might it create risk for our business? Could it create a large volume of support calls? Make us non-GDPR compliant?
            - Business criticality - Part of high ROI opportunity or other business critical initiative?
            - Impact to important stakeholders - CEO or CMO request? Impacts bottom of funnel (BOFU) prospects very close to buying? Impacts key partners or customers?
        - **Differentiator**
            - Brand and or product differentiator - Creates value by positioning our brand and or product against competition.
        - **Reusable**
            - Can we reuse - If we build this, can we reuse it elsewhere to get more ROI. Perhaps it's low value score for this project, but high "lifetime" value via reuse.
        - **Time & Cost**
            - Time and cost required to complete the work.
        - **Deadlines**
            - Are there any hard deadlines due to contract or event obligations?
            - Assign to Sprints

#### Sprint planning
Before the sprint starts, Brand and Digital teams break high-level issue requests into task-based implementation issues, as needed. Implementation issues are assigned, weighted and related to other issues to denote relationships and dependencies (i.e. blocking issues).

- Implementation issues  
    - [**Strategy Issue**](https://gitlab.com/gitlab-com/marketing/growth-marketing/brand-and-digital/brand-and-digital/-/issues/new?issuable_template=team-implementation-strategy) - Issue to define strategic brief that defines project hypothesis, scope, business and brand requirements, target audience, CTAs and any other information key to success.
    - [Conversion Checklist Issue](https://gitlab.com/gitlab-com/marketing/growth-marketing/brand-and-digital/brand-and-digital/-/issues/new?issuable_template=team-implementation-conversion) - Issue that’s opened at the start of a conversion project, completed by design, development and testers throughout the project, and closed at the end of the project. 
    - [**Prototyping Issue**](https://gitlab.com/gitlab-com/marketing/growth-marketing/brand-and-digital/brand-and-digital/-/issues/new?issuable_template=team-implementation-prototyping) - Issue to facilitate co-design sessions using experience mapping and or lo- to mid-fi prototyping to rapidly align on big picture experience and value-driven MVCs.
    - [**Visual Design Issue**](https://gitlab.com/gitlab-com/marketing/growth-marketing/brand-and-digital/brand-and-digital/-/issues/new?issuable_template=team-implementation-visualdesign) - Issue to theme the approved conversion design prototypes.
    - [**Development Issue**](https://gitlab.com/gitlab-com/marketing/growth-marketing/brand-and-digital/brand-and-digital/-/issues/new?issuable_template=team-implementation-developement) - Issue to develop the approved design.
    - [**Testing Issue**](https://gitlab.com/gitlab-com/marketing/growth-marketing/brand-and-digital/brand-and-digital/-/issues/new?issuable_template=team-implementation-testing) - Issue to define, conduct and report outcome of tests.
- Weighting 
    - We use issue weight to plan and manage our work better. Team members are not held to weights, they’re simply a planning and learning tool. Brand and Digital weights are based on the following point scale: 1pt = .5 day
- Issue Assignments
    - All sprinted implementation issues are assigned to designer or developer. He or she is responsible for leading the issue end-to-end, including facilitating cross-functional completion, keeping the SSOT it up to date, responding to contributions, and completing requirements.
- Right-sizing Sprints
    - We strive to plan sprints we can complete. When we don’t, that is ok, so long as we learn from the experience and continually improve.  In order to achieve predictable sprint outcomes we focus on: 
        - Can I complete this work in a week? 
        - Have we been under or over estimating this type of work? Should we adjust the weight accordingly?
        - Do I need to break this MVC into smaller MVCs?
        - Do I anticipate any blockers, like cross-functional dependencies or technical challenges? If so, are they manageable or should I pivot to an alternate priority?

#### Sprint cycle

- **Weekly Meetings**
    - **Sprint Kick-off** - At the beginning of each week, we kick-off the sprint and each team member:
        - Shares their priorities for the weekly sprint in the meeting agenda. Priorities should be reflected on Brand and Digital issue boards.
        - Notes any blockers.
    - **Slack** **Stand-ups** 
        - We’re experimenting with async Slack stand-ups. If you’re not participating in Slack stand-ups and would like to, please reach out to your leader.
    - **1:1’s** 
        - Leaders check-in with team members on sprinted projects to provide feedback, address blockers and or pivot project priorities as needed.
        - Its recommended team members issue boards be used to facilitate these discussions. 
    - **Team Reviews**
        - Gathering iterative feedback is an essential part of the design and development process. Team reviews, allow team members to get input and ideas before work begins, quickly solicit feedback on works in progress (WIP), and validate final solutions. Because GitLab is an all-remote company, team reviews are also important for building shared understanding with the broader team and company.
            - Brand and Digital managers will incorporate peer reviews into their team processes, and provide feedback on MVC scope, processes, and WIP and final designs and development solutions.
            - Brand and Digital team members will present at least once a fiscal quarter, and record and share with the Marketing Growth team for awareness and async contribution.
            - Optionally, and only with presenter approval,  team review recordings can be posted  to GitLab Unfiltered.
            - Be sure to frame discussions around the customer and the problem being solved, not the ”pretty” visual design or functionality.  As the discussion unfolds, continually tie everything back to how we can best achieve Gitlab (ex: conversion) and target audience needs (ex: easily getting valuable content from trustworthy source).
    - **Demo Day!**
        - Team members showcase something they have completed during the sprint. 
        - Peers have the opportunity for Q&A async, or verbally if time permits.
- **Close out** - We close out the week's milestone each week by processing:
    - Closed issues - Closing completed issues.
    - Rollover issues - Moving issues forward if they were not closed out. Issues that consistently rollover may require attention.


### Strategy 


#### Define the opportunity

- Work with your stakeholders to clearly define *who* we're solving for, *what we want to achieve*, and *why* we’re solving for it.
- Help your stakeholders define the *who*/*what*/*why* as a user story. For example, "As a (who), I want (what), so I can (why/value)." If you’re asked to implement a non-evidence-based *how* (i.e. a specific solution), then ask the requestor to focus on the who/what/why, so everyone can work together to find the best *how*.
- Help stakeholders define [MVC](https://about.gitlab.com/handbook/values/#minimal-viable-change-mvc) success criteria, prioritizing MVC “must-haves” and non-MVC “should-haves” and “could-haves.” (Note that this success criteria is subject to change based on new learning from the iterative design process and customer feedback.)
    

### Design process


#### Before you design

**Generate ideas**
Part of the role of Brand and Digital is to lead and facilitate idea generation with stakeholders. We are all very busy working with stakeholders to solve known problems, but remember that there are also undiscovered problems out there that are definitely worth solving. Here are a few activities and resources to inspire you!

- Run a sync, async, or design session to generate ideas. Define the scope and goals for the session, and invite diverse participants for the best results.
- Reach out to other GitLabbers who may represent the target audience or simply have unique perspective and insights, by inviting them to meetings or via Slack, to generate new idea and or get feedback on existing ideas.
- Spin up a sync or async session to share competitive and comparative solutions, and then use them as an “iteration 0” like/dislike session with stakeholders.

#### Understand the space

- Consider lightweight competitive research an analysis to inform your work.
    - Google and or social search the subject you’re working on
    - Checkout competitor and comparative solutions
    - Put yourself in the target audiences shoes, as you get a sense of the competitive and market landscape.
    - Identify “boring” design conventions customers expect, such as interactive patterns or subject matter iconography, that we can embrace for marketing and release speed reasons. Only stray from industry conventions with strategic intent, such as capitalizing on [disruptive innovation](https://www.economist.com/the-economist-explains/2015/01/25/what-disruptive-innovation-means) opportunities.
    - Screen capture competitive and comparative solutions, you can use to generate ideas.
- Consider creating user flows or journey maps to help ensure you and stakeholders understand how the touchpoint you’re working on fits into the broader buyer journey. Where are prospects coming from? Where will they go next? How do we optimize the overall experience for conversion?
- Read the content you’re designing, so you can maximize the message with meaningful photos, illustrations and diagrams that are informative and engaging.
    
#### Investigate possible dependencies

It is our responsibility as Brand and Digital to ensure the touchpoints and experiences we deliver are well integrated into GitLab’s experiences.

- Proactively reach out to other Brand and Digital GitLabbers to align your work to theirs and ensure the solution delivered is on brand and conversion optimized.
- Identify the DRIs, cross-discipline peers and stakeholders early and make sure they’re aware of your work, their role and that they have what they need from you to contribute and avoid delaying your delivery. 


### Design


#### Ideate and iterate

- Share design ideas in the lowest fidelity that still communicates your idea. To keep the cost of change low, only increase fidelity as design confidence grows and implementation requires.
- Ask for feedback from stakeholders throughout the design process to help refine your understanding of the problem and align on the best possible solution. 
- Be sure to frame discussions around the customer and the problem being solved, not visual design or functionality. When presenting, walk through the proposed solution from the target audience’s point of view. As the discussion unfolds, continually tie everything back to how well proposed solutions achieve Gitlab and target audience goals. 
- Ask for feedback from your Brand and Digital team via Slack or in a Team Design & Development Review <link> to help improve your work. At minimum, you'll get objective feedback and new ideas that lead to better solutions. You might also get context you didn’t know you were missing, such as related GitLab initiatives and solutions you can factor into your solution.
- Engage Digital peers early and often. Their insight into technical costs and feasibility is essential to determining viable designs and MVCs. 
- Collaborate with your Content Marketing group early and often, to ensure design and copy work together to create a cohesive, compelling and conversion optimized experience.
- For important project, like a key content piece and or conversion path, include your leadership in feedback, as they might have input into the overall direction of the design or knowledge about initiatives that might impact your own work.
- Work with Growth Marketing peers to align on how to measure the success of the overall initiatives you’re a part of, and your MVCs.
- Make sure your solutions are aligned to design and dev standards and best practices, such as conversion best practices. 
    
#### Refine MVC

- MVC issues have a tendency to expand in scope. Work with your stakeholders to revisit which aspects of the solution are “must haves” versus those that can be pushed until later. Document non-MVC requirements in new issues, and relate the new issues to the original issue. If you’re ever unsure how to split apart large issues, work with your leader.
- If development needs to begin before you have completed your design, and a planning pivot to another issue is not an option, then look for high confidence and low risk elements dev can start work on while you finish the remainder of the design. To mitigate these scenarios and the dev delays and waste they can create, everyone should work together to plan ahead.
- Devs should be able to build an MVC is one sprint. If an MVC is too large to build within one release, work with your leadership and peers to split the MVC into smaller MVCs that can be closed at the end of the sprint. Note: MVC may be completed in a sprint, but released later due to dependencies on other dev MVCs.

#### Final MVC

- After you've facilitated and open and inclusive process, present your final design solution in the Design tab. 
- When sharing asynchronously in an issue, make sure your audience has the context necessary to understand how your proposal delivers on the who, what and why and our brand and business goals, and anything you need from them. Is it clear who will use the solution and how it meets Gitlab and target audience goals? Is this just the final interactive design or is visual design also final? Do you need feedback or assistance from stakeholders, like final content changes? To make reviewing easier, have you highlighted how you addressed final change requests, questions and concerns.
- Set the issue marketing status scope label to  `mktg-status::design-review` to open the solution up to final review and approval. If you are not able to get approval from your leadership, despite best efforts, do not let that hold up delivery.
- Anticipate questions that others might have, and try to answer them in your proposal comments. You don’t have to explain everything, but try to communicate a bit of your rationale every time you propose something. This is particularly important when proposing changes or challenging the status quo, because it reduces the feedback loop and time spent on unnecessary discussions. It also builds the UX Department’s credibility, because we deal with a lot of seemingly subjective issues.
- Keep the SSOT updated with what’s already agreed upon so that everyone can know where to look. This includes images or links to your design work.
- If you are proposing a solution that will introduce a design, or change an existing one, please consider the following:
    1. Will this design or interaction pattern be inconsistent with like experiences?
    2. Will like experiences need to be updated to match?
    3. Is upgrading this design or interaction pattern worth the cost?

#### Deliver

- Once your work is complete and all feedback is addressed, make sure that the issue description and SSOT are up to date, you’ve validated that you have followed any required best practices, and relevant parties are informed of what to do next.   
- As applicable, commit all final design assets and files to appropriate repositories.
- If the solution needs to be broken out into smaller issues for implementation, work with your Digital peer to do so. 

#### Follow through

- Encourage developers to scope down features into multiple merge requests for an easier, more efficient review process.
- When breaking down features into multiple merge requests, consider how the UX of the application will be affected. If merging only a portion of the total changes will negatively impact the overall experience, consider using a feature branch or feature flag to ensure that the full UX scope ships together.
- When breaking solutions into smaller MVCs, make sure customers do not get fragmented or incomplete experiences. Make sure everyone understands the full picture so dependent MVCs are released together.
- Keep the issue description updated with the agreed-on scope and requirements, even if doesn’t impact your work. This is everyone’s responsibility. The issue description and design files must be the Single Source Of Truth (SSOT), not the discussion or individual comments. If the developer working on the issue ever has any questions on what they should implement, they can ask the designer to update the issue description with the design.
- For obvious changes, make the SSOT description update directly. [You don't need to wait for consensus](https://about.gitlab.com/handbook/values/). Use your judgement.
- When the issue is actively being worked on, make sure you are assigned and subscribed to the issue. Continue to follow both the issue and related merge request(s), addressing any gaps or concerns that arise.
    
#### Design reviews of coded solutions

Ideally, design is part of the dev review process. Any MR that makes a significant change that is user-facing should be reviewed by a designer.

- UX reviews of coded product are a high priority. Tackle them as quickly as you are able.
- Test coded product, do not rely on screenshots.
- Be thorough. There should be as little back and forth as possible.
- If you are asked to review an MR for an issue you were not assigned to, remind the author who the assigned designer is and assign to original designer for review.
- When reviewing an MR, please use the following order of importance:
    - Functionality first: Does it work?
    - Edge cases: Are there any unexpected edge cases?
    - Visual consistency: Does it conform to brand identity?
- Remember to stick to the issue. Create issues for further updates to avoid scope creep.
- Once you have completed the review process, note your approval. You can then un-assign yourself from the MR. 


### Development process

(coming soon)


### Testing process

(coming soon)



# Brand guidelines

<details markdown="1">

<summary>show/hide this section</summary>

## Guidelines home

### Guidelines how we work
[ TODO : Document ]

#### Partnership with third parties

In certain cases, the help of a third party agency or design partner may be brought in for a project. The following serves as criteria for when to outsource design:

- Smaller-scale projects, such as stickers or requests that do not meet our current business prioritization, where the brand guidelines provide sufficient creative direction and parameters for the third party to work with. 
- Larger-scale projects where the Brand and Digital team need additional support given the timeline and/or scale of the request.

Whenever a third party is brought in to support design, the work must be shared with the Brand and Digital team to ensure brand integrity and that we are [working transparently](https://about.gitlab.com/handbook/values/#transparency) with one another.

For guidance on which third-party agency or designer(s) to work with, please reach out to [brand-and-digital@gitlab.com](mailto:brand-and-digital@gitlab.com).

#### Requesting design help

1. Create an [issue](https://about.gitlab.com/handbook/marketing/growth-marketing/brand-and-digital-design/#issue-templates) in the corresponding project repository.
    1. For tasks pertaining to [about.gitlab.com](/) create an issue in the [www-gitlab-com project](https://gitlab.com/gitlab-com/www-gitlab-com/issues).
    1. For all other marketing related tasks create an issue in the [corporate marketing project](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues).
1. Add all relevant details, goal(s), purpose, resources, and links in the issue description. Also `@` mention team members who will be involved.
1. Set due date (if possible) — please leave at least 2 week lead time in order to generate custom design assets. If you need them sooner, ping @luke in the #marketing-design Slack channel and we will make our best effort to accommodate, but can't promise delivery.
1. Add the `Design` and `Website Redesign` (if applicable) label(s) to your issue.

#### Team logo request guidelines

As the company continues to grow, incoming requests for internal team logos are increasing at a rate that is not scalable for the Brand Design team. We understand the desire for teams within GitLab to have their own identity, so we've created these guidelines to help direct your request:

* Teams can create their own logos that are for internal (non-public) use only.
* If you believe a public-facing team logo would be valuable to our business, please submit a [design request issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues) outlining how it will be used, who will see it, and it's perceived value. Logos will be created and approved on a case-by-case basis to capitalize on brand opportunities and ensure brand integrity.

### Privacy

[ TODO : Document ]

### License

[ TODO : Document ]

### What's new

[ TODO : Document ]

## Design principles

### Generate value

[ TODO : Document ]

### Omni-channel experiences

[ TODO : Document ]

### Everyone can contribute

[ TODO : Document ]

### Grow a community

[ TODO : Document ]

## Foundations

### Personality

GitLab's brand has a personality that is reflected in everything we do. It doesn't matter if we are hosting a fancy dinner with fortune 500 CIOs, at a hackathon, or telling our story on about.gitlab.com…across all our communication methods, and all our audiences, GitLab has a personality that shows up in how we communicate.

Our personality is built around four main characteristics.

- Human: We write like we talk. We avoid buzzwords and jargon, and instead communicate simply, clearly, and sincerely. We treat people with kindness.
- Competent: We are highly accomplished, and we communicate with conviction. We are efficient at everything we do.
- Quirky: We embrace diversity of opinion. We embrace new ideas based on their merit, even if they defy commonly held norms.
- 
Humble: We care about helping those around us achieve great things more than we care about our personal accomplishments.

These four characteristics work together to form a personality that is authentic to GitLab team-members, community, and relatable to our audience. If we were quirky without being human we could come across as eccentric. If we were competent without being humble we could come across as arrogant.

GitLab has a [higher purpose](https://about.gitlab.com/company/strategy/#mission). We want to inspire a sense of adventure in those around us so that they join us in contributing to making that mission a reality.

### Writing style

The following guide outlines the set of standards used for all written company communications to ensure consistency in voice, style, and personality, across all of GitLab's public communications.

See the [Blog Editorial Style Guide](https://about.gitlab.com/handbook/marketing/corporate-marketing/content/editorial-team/#blog-style-guide) for more.

The [tone of voice](https://about.gitlab.com/handbook/marketing/corporate-marketing/#tone-of-voice-1) we use when speaking as GitLab should always be informed by our [Content Strategy](https://gitlab.com/gitlab-com/marketing/blob/master/content/content-strategy.md#strategy). 

### Guidelines website

#### Using other logos

Logos used on the about.gitlab.com site should always be in full color and be used to the specifications provided by the owner of that logo, which can usually be found on the owners website. The trust marks component found throughout the site is the only exception and should use a neutral tone:

<img src="/images/handbook/marketing/corporate-marketing/design/trust-marks.png" class="full-width">

#### Text

Our website uses the [Source Sans Pro](https://fonts.google.com/specimen/Source+Sans+Pro?selection.family=Source+Sans+Pro) font family. Headers (h1, h2, etc.) always have a weight of 600 (unless used in special situations like large, custom quotes) and the body text always has a weight of 400. Headers should not be given custom classes, they should be used as tags and tags alone (h1, h2, etc.) and their sizes or weights should not be changed, unless rare circumstances occur. Here are typography tags.

`H1: Header Level 1`

`H2: Header Level 2`

`H3: Header Level 3`

`H4: Header Level 4`

`p: Body text`

#### Buttons

Buttons are an important facet to any design system. Buttons define a call to action that lead people somewhere else, related to adjacent content. Here are buttons and their classes that should be used throughout the marketing website:

**Note**: Text within buttons should be concise, containing no more than 4 words, and should not contain bold text. This is to keep things simple, straightforward, and limits confusion as to where the button takes you.

**Primary buttons**

Primary buttons are solid and should be the default buttons used. Depending on the color scheme of the content, purple or orange solid buttons can be used depending on the background color of the content. These primary buttons should be used on white or lighter gray backgrounds or any background that has a high contrast with the button color. They should also be a `%a` tag so it can be linked elsewhere and for accessibility. Buttons should also be given the class `margin-top20` if the button lacks space between itself and the content above.

<div class="flex-container flex-column flex-start margin-bottom20">
  <a href="/handbook/marketing/corporate-marketing/#primary-buttons" class="btn cta-btn orange margin-top20">Primary Button 1</a>
  <pre class="highlight shell">.btn.cta-btn.orange</pre>
  <p>OR</p>
  <a href="/handbook/marketing/corporate-marketing/#primary-buttons" class="btn cta-btn purple">Primary Button 2</a>
  <pre class="highlight shell">.btn.cta-btn.purple</pre>
</div>

**Secondary buttons**

There will be times when two buttons are needed. This will be in places such as [our jobs page](/jobs/), where we have a button to view opportunities and one to view our culture video. In this example, both buttons are solid, but one is considered the primary button (orange), and the other is the secondary button (white). The CSS class for the solid white button is <br> `.btn.cta-btn.btn-white`.

<img src="/images/handbook/marketing/corporate-marketing/design/jobs-buttons-example.png" class="full-width">

This is the proper use of two buttons, both being solid, but different colors based on hierarchy. If the background is white or a lighter color that doesn't contrast well with a white-backgound button, a ghost button should be used as a secondary button, and should match in color to the primary button beside it as shown below:

<div class="buttons-container flex-start">
  <a href="/handbook/marketing/corporate-marketing/#primary-buttons" class="btn cta-btn orange margin-top20">Primary Button</a>
  <a href="/handbook/marketing/corporate-marketing/#primary-buttons" class="btn cta-btn ghost-orange margin-top20">Secondary Button</a>
</div>

<div class="buttons-container flex-start margin-bottom20">
  <a href="/handbook/marketing/corporate-marketing/#primary-buttons" class="btn cta-btn purple margin-top20">Primary Button</a>
  <a href="/handbook/marketing/corporate-marketing/#primary-buttons" class="btn cta-btn ghost-purple margin-top20">Secondary Button</a>
</div>

DO NOT: Do not use these ghost buttons styles as standalone buttons. They have been proven to be less effective than solid buttons [in a number of studies](https://conversionxl.com/blog/ghost-buttons/). They should only be used as a secondary button, next to a solid primary button that already exists. Here are the classes for the secondary buttons:

<div class="flex-container flex-column flex-start margin-bottom20">
  <a href="/handbook/marketing/corporate-marketing/#primary-buttons" class="btn cta-btn ghost-orange margin-top20">Secondary Button 1</a>
  <pre class="highlight shell">.btn.cta-btn.ghost-orange</pre>
  <a href="/handbook/marketing/corporate-marketing/#primary-buttons" class="btn cta-btn ghost-purple">Secondary Button 2</a>
  <pre class="highlight shell">.btn.cta-btn.ghost-purple</pre>
</div>

### Iconography

Icons are a valuable visual component to the GitLab brand; contributing to the overall visual language and user experience of a webpage, advertisement, or slide deck. The GitLab iconography currently consists of "label icons" and "content icons", each are explained in further detail below:

**Label icons**

Label icons are intended to support usability and interaction. These are found in interactive elements of the website such as navigation and [toggles](/pricing/).

![Label icons example](/images/handbook/marketing/corporate-marketing/design/label-icons-example.png){: .medium.center}

**Content icons**

Content icons are intended to provide visual context and support to content on a webpage; these icons also have a direct correlation to our illustration style with the use of bold outlines and fill colors.

A few examples include our [event landing pages](/events/aws-reinvent/) and [Resources page](/resources/).

<img src="/images/handbook/marketing/corporate-marketing/design/content-icons-example.png" class="full-width">

### Logos

To download the GitLab logo (in various formats and file types) check out our [Press page](/press/).

The GitLab logo consists of two components, the icon (the tanuki) and the wordmark:

![GitLab logo](/images/handbook/marketing/corporate-marketing/design/gitlab-lockup.png){: .small.left}

GitLab is most commonly represented by the logo, and in some cases, the icon alone. GitLab is rarely represented by the wordmark alone as we'd like to build brand recognition of the icon alone (e.g. the Nike swoosh), and doing so by pairing it with the GitLab wordmark.

#### Logo safe space

Safe space acts as a buffer between the logo or icon and other visual components, including text. this space is the minimum distance needed and is equal to the x-height of the GitLab wordmark:

![Logo x-height](/images/handbook/marketing/corporate-marketing/design/x-height.png){: .medium.left}

![Logo safe space](/images/handbook/marketing/corporate-marketing/design/logo-safe-space.png){: .small.left}

![Icon safe space](/images/handbook/marketing/corporate-marketing/design/icon-safe-space.png){: .small.left}

The x-height also determines the proper spacing between icon and wordmark, as well as, the correct scale of the icon relative to the wordmark:

![Stacked logo safe space](/images/handbook/marketing/corporate-marketing/design/stacked-logo-safe-space.png){: .small.left}

#### Minimum logo size

Here are the recommended minimum sizes at which the logo may be reproduced. For legibility reasons, we ask that you stick to these dimensions:

**Logo**

![Horizontal logo minimum size](/images/handbook/marketing/corporate-marketing/design/logo-min-size.png){: .small.left}

- Digital: 100px wide
- Print: 1.25in (31.75mm) wide

**Stacked logo**

![Stacked logo minimum size](/images/handbook/marketing/corporate-marketing/design/logo-stacked-min-size.png){: .small.left}

- Digital: 60px wide
- Print: 0.75in (19mm) wide

**Icon**

![Icon minimum size](/images/handbook/marketing/corporate-marketing/design/icon-min-size.png){: .small.left}

- Digital: 30px wide
- Print: 0.50in (13mm) wide


#### The Tanuki

The [tanuki](https://en.wikipedia.org/wiki/Japanese_raccoon_dog) is a very smart animal that works together in a group to achieve a common goal. We feel this symbolism embodies GitLab's [mission](/company/strategy/#mission) that everyone can contribute, our [values](/handbook/values/), and our [open source stewardship](/company/stewardship/).

The tanuki logo should also not have facial features (eyes, ears, nose...); it is meant to be kept neutral, but it can be accessorized.

#### Brand oversight

Occasionally the [old GitLab logo](* https://gitlab.com/gitlab-com/gitlab-artwork/blob/master/_archive/logo/fox.png) is still in use on partner websites, diagrams or images, and within our own documentation. If you come across our old logo in use, please bring it to our attention by creating an issue in the [Marketing](https://gitlab.com/gitlab-com/marketing/general/issues) issue tracker. Please include a link and screenshot (if possible) in the description of the issue and we will follow-up to get it updated. Thanks for contributing to our brand integrity!

### Trademark

GitLab is a registered trademark of GitLab, Inc. You are welcome to use the GitLab trademark and logo, subject to the terms of the [Creative Commons Attribution Non-Commercial ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-nc-sa/4.0/). The most current version of the GitLab logo can be found on our [Press page](/press/).

Under the Creative Commons license, you may use the GitLab trademark and logo so long as you give attribution to GitLab and provide a link to the license. If you make any changes to the logo, you must state so, along with the attribution, and distribute under the same license.

Your use of the GitLab trademark and logo:

- May not be for commercial purposes;
- May not suggest or imply that you or your use of the GitLab trademark or logo is endorsed by GitLab, or create confusion as to whether or not you or your use of the GitLab trademark or logo is endorsed by GitLab; and
- May not suggest or imply or that you are affiliated with GitLab in any way, or create confusion as to whether or not you are affiliated with GitLab in any way.

Examples of improper use of the GitLab trademark and logo:

- The GitLab name may not be used in any root URL, including subdomains such as `gitlab.company.com` or `gitlab.citool.io`.
- The GitLab trademark and/or logo may not be used as the primary or prominent feature on any non-GitLab materials.

### Typography  

The GitLab brand uses the [Source Sans Pro](https://fonts.google.com/specimen/Source+Sans+Pro?selection.family=Source+Sans+Pro) font family.

### Color

While the brand is ever-evolving, the GitLab brand currently consists of six primary colors that are used in a wide array of marketing materials. RGB and CMYK swatch libraries can be found [here](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/tree/master/design/gitlab-brand-files/color-palettes). 

##### Hex/RGB

![GitLab Hex/RGB Colors](/images/handbook/marketing/corporate-marketing/design/gitlab-hex-rgb-colors.png)


### Illustration  

### Iconography

Icons are a valuable visual component to the GitLab brand; contributing to the overall visual language and user experience of a webpage, advertisement, or slide deck. 

**Badges**
[TODO: Document]

**Patterns**
[TODO: Document]

#### Illustration library
[ TODO : Document ]


#### Icon library

- [Line icons](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/presentation-decks/_general-assets/icons/line-icons) (.png)
- [Line icons](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/_resources/icons/svg) (.svg)
- [Icon illustrations](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/presentation-decks/_general-assets/icons/illustrated-icons) (.png)
- [Software Development Lifecycle](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/gitlab-brand-files/software-development-lifecycle/complete-lifecycle-icons/png)

#### Icon pattern

- [GitLab icon pattern](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/gitlab-brand-files/illustrations/icon-pattern)

#### Social media

- [Profile assets](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/social-media/profile-assets/png/assets)

### Photography

#### Photo library
[ TODO : Document ]

### Brand resources

- [GitLab icons](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/gitlab-brand-files/gitlab-logo) (web RGB & print CMYK)
- [GitLab logos](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/gitlab-brand-files/gitlab-wordmark) (web RGB & print CMYK)
- Print-ready [event one-pagers](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/print/event-one-pagers)
- Color palette and Typography can be found in the [Brand Guidelines](#brand-guidelines)
- [Authorized Reseller GitLab Virtuoso Badge](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/gitlab-brand-files/authorized-resellers/gitlab-virtuoso-badge)

### Templates

#### Issue templates

[ TODO : Merge with the "how to request work" section ]

To work more efficiently as an [asynchronous](/company/culture/all-remote/asynchronous/) team, everything should [begin](/handbook/communication/#everything-starts-with-a-merge-request) in an issue or merge request as opposed to Slack or email.

Creating an issue may seem like an added burden, but the long-term benefit of having [context](/company/culture/all-remote/effective-communication/#understanding-low-context-communication) around a given piece of work prevents [knowledge gaps](https://about.gitlab.com/company/culture/all-remote/asynchronous/#plugging-the-knowledge-leak) from occurring. Issue templates exist to make the process of creating issues easier. If you find yourself starting similar issues over and over, look through existing issue templates. If a suitable one does not exist, consider creating one in the appropriate repository.

For more on how to make beautiful templates, check out GitLab's [Markdown Guide](https://about.gitlab.com/handbook/markdown-guide/). To add an emoji in an issue, begin by typing `:` and the title of the emoji. A list of emoji markup is [here](https://gist.github.com/rxaviers/7360908).

Remember to always add `Related Issues` and `Epics` after you've created your issue so others have context on what issues connect to this work.

- Visit the [Corporate Marketing Issue Template Repository](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/tree/master/.gitlab/issue_templates) to view all available issue templates. You'll find templates covering [Corporate Events](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/blob/master/.gitlab/issue_templates/Corporate-Event-Request.md), [Bulk Swag Requests](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/blob/master/.gitlab/issue_templates/BulkSwagRequest.md), Social Requests for [Events](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/blob/master/.gitlab/issue_templates/social-event-request.md) and [General](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/blob/master/.gitlab/issue_templates/social-general-request.md), Video Requests, [Webpage Updates](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/blob/master/.gitlab/issue_templates/webpage-update.md), and more.
- If you're for a [general or universal issue template](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/blob/master/.gitlab/issue_templates/general-project-template.md) to begin tracking discussion and progress on any given piece of work, big or small, search for `general-project-template` in a newly-created [Corporate Marketing Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues). This template is pre-populated and beautified with emojis and descriptors for common sub-sections such as `Background`, `Details and reach`, `Goals and key messages`, and `Due dates, DRI(s) and next steps/to-dos`.


#### Presentation kits

- [General GitLab deck template](https://docs.google.com/a/gitlab.com/presentation/d/16FZd01-zCj_1jApDQI7TzQMFnW2EGwtQoRuX82dkOVs/edit?usp=sharing)
- [GitLab pitch deck template](https://docs.google.com/a/gitlab.com/presentation/d/1LC1lT-gxpl1oUZ2InX4Oni9T4MfR0DFF0RLi4uNxBBQ/edit?usp=sharing)

#### Event kits
[ TODO : Document ]

#### Swag kit
[ TODO : Document ]

#### Print templates
[ TODO : Document ]

</details>
















# Digital FAQ

<details markdown="1">

<summary>Why can't I download a resource?</summary>

### Why can't I download a resource?

Many of our resources are [gated content](https://instapage.com/blog/what-is-gated-content). In order to download resources, someone has to fill out a form. The third party we use to provide these forms is Marketo. Sometimes Marketo is blocked by an ad blocker or a strict web browser such as [Brave](https://brave.com/). GDPR and CCPA (among other laws) require us to obtain consent before setting cookies and those cookies are required for many third party functions.

If you are having trouble downloading a resource, please try <a href="javascript:Cookiebot.renew();">updating your cookie settings</a> to allow **personalization (personal information) cookies**. If that does not work, please try a different browser with a less strict adblocker and whitelist our domain and subdomains (gitlab.com, about.gitlab.com, and page.gitlab.com).

If you have tried the above solutions but are still having trouble downloading a resource, please [file a bug report](https://gitlab.com/gitlab-com/www-gitlab-com/issues/new?issuable_template=-website-bug-report). Please note that if you do not provide all of the requested information we may be unable to reproduce the bug and therefore unable to fix it.

</details>

<details markdown="1">

<summary>Why can't I see something in the review apps?</summary>

### Why can't I see something in the review apps?

This is potentially due to [cookiebot](#what-is-cookiebot). Because of how our infrastructure is setup, certain third party content (such as youtube embeds) or advanced javascript may not appear on review apps. If you are unsure of the cause, please [contact us](#contact-us) so we can help review what might be impacting your project.

</details>

<details markdown="1">

<summary>What is cookiebot?</summary>

### What is cookiebot?

Cookiebot is a third party tool we use to ensure compliance with GDPR, CCPA, and related cookie laws.

**How does cookiebot work?**

Before you consent to any cookies, cookiebot blocks all javascript on our site in order to prevent cookies from being set. This works by intercepting the and preventing the document ready event. Once cookie consent has been granted by the end user, cookiebot then restarts the javascripts.

Note that any javascript on about.gitlab.com requiring the use of document ready may have problems reinitializing after cookie consent has been granted and will not work before that timeframe.

If you are having difficulty programming around cookiebot, please [contact us](#contact-us) so we can assist.

**Does cookiebot work on review apps?**

Because of how our review app infrastructure is setup, and because cookiebot will not work on wildcard subdomain URLs, we cannot get cookiebot to work on review apps. This means that pages in a review app which require the use of: third party iframes, cookies, or advanced javascript may not work on review apps.

**Can we improve cookiebot?**

We have plans to [improve our cookiebot implementation](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/issues/2767) but implementation will take time.

</details>












# Our People

<details markdown="1">

<summary>show/hide this section</summary>

<details markdown="1">

<summary>Meet the Brand team</summary>

## Meet the Brand team

[**Shane Bouchard**](https://about.gitlab.com/company/team/?department%5Cu003dops-section/#sbouchard1)

* Title: Director of Brand and Digital Design
* Email: sbouchard@gitlab.com
* GitLab handle: sbouchard1
* Slack handle: sbouchard

[**Luke Babb**](https://about.gitlab.com/company/team/?department%5Cu003dops-section/#luke)

* Title: Manager, Creative
* Email: luke@gitlab.com
* GitLab handle: @luke
* Slack handle: @luke

[**Monica Galletto**](https://about.gitlab.com/company/team/?department%5Cu003dops-section/#monica_galletto) 

* Title: Associate Production Designer
* Email: mgalletto@gitlab.com
* GitLab handle: @monica_galletto
* Slack handle: @monicagalletto

[**Vic Bell**](https://about.gitlab.com/company/team/?department%5Cu003dops-section/#vicbell)	

* Title: Senior Illustrator
* Email: vbell@gitlab.com
* GitLab handle: @vicbell
* Slack handle: @vic

</details>

<details markdown="1">

<summary>Meet the Digital team</summary>

## Meet the Digital team

[**Shane Bouchard**](https://about.gitlab.com/company/team/?department%5Cu003dops-section/#sbouchard1)

* Title: Director of Brand and Digital Design
* Email: sbouchard@gitlab.com
* GitLab handle: sbouchard1
* Slack handle: sbouchard

[**Brandon Lyon**](https://about.gitlab.com/company/team/?department%5Cu003dops-section/#brandon_lyon)

* Title: Website Developer and Designer
* Email: skarpeles
* GitLab handle: @brandon_lyon
* Slack handle: @Brandon Lyon 

[**Lauren Barker**](https://about.gitlab.com/company/team/?department%5Cu003dops-section/#laurenbarker) 

* Title: Fullstack Engineer
* Email: lbarker@gitlab.com
* GitLab handle: laurenbarker
* Slack handle: lbarker

[**Stephen Karpeles**](https://about.gitlab.com/company/team/?department%5Cu003dops-section/#skarpeles)	

* Title: Website Developer and Designer
* Email: skarpeles@gitlab.com
* GitLab handle: @skarpeles
* Slack handle: @Stephen Karpeles

</details>

<details markdown="1">

<summary>Meet the Growth team</summary>

## Meet the Growth team

[**Shane Rice**](https://about.gitlab.com/company/team/#shanerice)

* Title: Manager, Growth Marketing
* GitLab handle: shanerice
* Slack handle: shanerice

[**Niall Cregan**](https://about.gitlab.com/company/team/#niallcregan)

* Title: Growth Marketing Associate
* GitLab handle: ncregan
* Slack handle: Niall Cregan

### Website analytics dashboards

We use Google dataStudio to create an easy to share and repeatable process for sharing analytics data for about.gitlab.com. dataStudio allows us to combine data from Google Analytics, Google Ads, and a variety of other sources to create reports with key data for any GitLab team.

When we connect data from Google Sheets everyone on the DMP team needs to able to access and edit data as needed. To accomplish this we add all Google Sheets used in dataStudio to the Shared Drive in the [dataStudio sub-folder](https://drive.google.com/drive/folders/1pO0fVLM-K0KrLNu8MWvzI-i96QXFdgeR). Be sure you only share your Google Sheets in the shared drive because dataStudio does not currently support sharing data sources from shared drives.

### Self-serve Website analytics dashboards
* Google Analytics traffic: If you would like to check the traffic, referrals, or clicks off of a page hosted on the marketing site (about, docs, etc) just enter your page URL or URLs into the `Page` field in this dataStudio report [Page-level Google Analytics data](https://datastudio.google.com/u/0/reporting/11pNZyzJ1JEudO4jRWaX989etLLcR6wUn/page/KNqS).
* Google Search results keywords: If you would like to check the keywords from a specific about.gitlab.com page in Google search, just enter your page URL into the `Landing Page` field in this dataStudio report [Google Search Console keyword lookup](https://datastudio.google.com/open/1No1sSsCH2EHkqPyLl-AEEQ1be3_p_kpj).
* Google Search results pages: If you would like a list of URLs surfacing for a specific keyword surfacing for about.gitlab.com in Google search (about, docs, etc) just enter your keyword into the `Query` field in this dataStudio report [Google Search Console landing page lookup](https://datastudio.google.com/open/10Qa9AgGt11xJWaycUDfvFq1Nn714sJQq).

#### Active dataStudio dashboards

- [Marketing metrics dashboard](https://datastudio.google.com/reporting/1mvDffnzlIWsr-2S_cvkpRx0X25hiM_TI/page/1M) — Used to generate data for our monthly marketing metrics deck. A few elements update manually, ping the Digital Marketing team in #digitalmarketing if you need updated numbers.
- [Content marketing dashboard](https://datastudio.google.com/reporting/1NIxCW309H19eLqc4rz0S8WqcmqPMK4Qb/page/FCsh) — Blog content reports, primarily used by the content team to track OKR progress.
- [Just Commit dashboard](https://datastudio.google.com/reporting/1dbt-3WI6KzySYrnolIUfCufvvtba20f9/page/kWdQ) — Tracks progress of Just Commit integrated campaign.
- [Job pages dashboard](https://datastudio.google.com/reporting/1w6TwUeGjkQpPZz4jvp9Hye8vdGP6MYel/page/JcPY) — Provides context around job page interactions.
- [Security releases dashboard](https://datastudio.google.com/reporting/1bP748BOhYmgWRcfeoRiSCOHz7q4NUMkV/page/l7vj) — Website analytics data for security release blog posts.
- [about.gitlab.com CrUX dashboard](https://datastudio.google.com/reporting/1f-akzELoGzJRdBFPgMTzgHPrSOshUgki/page/cJUR) — *Public* See [Chrome User Experience Report](https://developers.google.com/web/tools/chrome-user-experience-report/) for definition of report dimensions. This dashboard shows CrUX data for about.gitlab.com, assembled with PageSpeed Insights and Public Google BigQuery.  

### Links on about.gitlab.com

We should link to resources that will help our readers. Be sure to include links to blog posts, guides, and other reference material. You can also include links to company or product websites if they are relevant to your topic. These links do not need to be "nofollowed" if they are informational.

However, we should use [Google's guidelines on nofollowing links](https://webmasters.googleblog.com/2016/03/best-practices-for-bloggers-reviewing.html) when we exchange a link for a product or service. It's also a best practice to ask for a nofollow link when we sponsor and disclose our links to sponsored content.

### Website Health Check

Regular website health checks should be performed. These checks are meant to ensure that there are no issues with the website that could cause issues with traffic to the site. These are the following things to check to ensure the health of the site:

- [Google Search Console](https://www.google.com/webmasters/tools/)
- [Google Analytics](https://analytics.google.com/analytics/web/)

Issues to look for in each of these tools:

- **Google Search Console**: Check the dashboard and messages for any important notifications regarding the website. Also check under `Search Traffic` > `Manual Actions` for any URLs that have been identified as spam or harmful content. Forward security warnings to the Abuse team and follow the [DMCA complaint process](/handbook/support/workflows/dmca.html) with the support team.
- **Google Analytics**: Compare organic site traffic from the most previous week compared to the previous week and look for any large fluctuations.

### Using robots metadata to manage search index

There are times we need to keep pages out of search indexes. For example, we might duplicate most of a page to improve conversion for an ad campaign. It's relatively rare to use this, but it's an important tool that helps us increase organic reach and paid advertising efficiency.

All pages are set to `meta name="robots" content="index, follow"` by default. To exclude a page from the index add `noindex: true` to the frontmatter, and this will set the robots metadata to `meta name="robots" content="noindex, follow"`.

### about.gitlab.com redirects

Occasionally we need to change URL structures of the website, and we want to make sure that people can find pages they need. We use 301 redirects to send people to the right URL when it's appropriate. 

#### about.gitlab.com redirect policy

We will redirect URLs included in Google's search index. A simple test to see if a page is indexed is to search for the URL with a site modifier, `site:url`, using Google.

We want to reduce the number of internal redirects, which means we need to update links across about.gitlab.com when we change URLs. When you request a redirect please indicate whether or not you were able to search across about.gitlab.com and update the links for a page you're moving.

#### Request an about.gitlab.com redirect

The Digital Marketing Programs team can set up and manage all redirects for about.gitlab.com.

To redirect an outdated page, open an issue with the [set up a new redirect template in the Digital Marketing Programs project](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/issues/new?issuable_template=set-up-a-new-redirect). You'll need to provide the following:

- Old URL that needs to be redirected
- New URL where users should now be sent
- Were you able to update existing links to the old URL across about.gitlab.com?

If you have any questions or concerns regarding your redirect request, ask for help in the `#digital-marketing` channel on Slack.

#### Redirect process documentation

The Digital Marketing Programs team uses these [technical details for the redirect process](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/doc/redirects_on_about_gitlab_com.md) on about.gitlab.com.

### GitLab Google Tag Manager system

We use Google Tag Manager(GTM) to simplify activity tracking on about.gitlab.com with Google Analytics. This documents the system the Digital Marketing Programs team uses with our Google Tag Manager container.

#### Naming convention

We use semantic names for tags, triggers, and variables to make it simpler to determine what these components are supposed to do. Use an em dash (shift+option-_) to divide the components of each GTM component name.

Tags start with the platform, followed by the tag’s purpose, and are finally contextualized to about.gitlab.com. Any tags related to timed events need the timeframe indicated in a note attached to the tag in GTM to make it clear when to remove a tag.

###### Tag naming examples

- Google Analytics Event — Free Trial CTA click  
- Google Ads —  conversion tracker  
- Facebook — base pixel
- Drift — snippet v 0.3.1

Triggers start with a description of the action triggering a tag, followed by contextualization for about.gitlab.com.

###### Trigger naming examples

- Link click — Learn more 100M carousel  
- CTA click — cta-btn  
- Custom event — mktoformSubmit free trial  

Variables start with the tag or trigger they reference, followed by contextual data about their purpose.

###### Variable naming examples

- Google Analytics — GitLab Universal Analytics ID  
- dataLayer — postType custom dimension  

If you are making changes to the GTM container and have questions about what to name one of these components the Digital Marketing Programs team can help.

#### dataLayer values

Today we’re using the dataLayer sparingly on about.gitlab.com, with our primary use focused on [differentiating blog content with `postType`](/handbook/marketing/blog/#definitions). We’ll expand how we use the dataLayer to create content groupings for improved customer journey insights and document those updates here.

#### Event tags

We need consistent tags across Google Analytics events and have introduced the following structure to our event tags. Our goal is to cover important visitor events with the smallest number of tags in Google Tag Manager. Reducing the number of tags and the overall complexity of our Google Tag Manager container helps us spot and fix coverage issues faster.

![GitLab Google Tag Manager event structure](/images/handbook/marketing/GTM-event-structure.png)

These three components help us organize and identify specific event data. **Event Categories** help us group specific customer journey steps, **Event Actions** describe visitor interactions with about.gitlab.com, and **Event Labels** provide contextual details for reporting our performance.

**Example Event Label**  
{{Page URL}} | {{Referrer}} | {{Click text}} | {{Click URL}} | {{Click Class}}

Google Analytics limits event label fields to around 2000 characters, and we'll update the handbook if we start to see truncated event labels.

### Google Tag Manager inventory

We're using Simo Ahava's Googl Sheets Add-On to sync notes for our tags and create a [GitLab Google Tag Manager inventory](https://docs.google.com/spreadsheets/d/1oT5AQQ0nH4-7iS-QY-UJP4vbFxv2GCGy9XiKpj8ebuU/edit#gid=1443259273) This simplifies scanning and searching over the Google Tag Manager web app.

### Changes of note
Whenever we make major changes to tags through Google Tag Manager we document them in [changes of note](/handbook/marketing/revenue-marketing/digital-marketing-programs/digital-marketing-management/changes-of-note/). Examples of changes we document are adding or removing tags, changing tag sequencing, or changing when a tag is fired.

### Using marketing trend data

Search term volume is hard to estimate. Different tools use different methodolgies and models for reporting this data. AdWords provides data from Google, but the ranges are broad and terms can be combined into a single phrase. Google Trend data normalizes search trends from 1 to 100 based on the terms you're exploring, which doesn't give us any idea of how many people are using a particular phrase.

For our keyword research we rely on Moz Keyword Explorer data because it provides a narrower volume with full phrase integrity. When we report keyword volume, this is the tool we use for this data.

### Google Analytics Crash Course

#### Dimensions vs Metrics

Dimensions are the different attributes of your data. For example, the landing page is a dimension that is the first page a person views when they come to the site.

Metrics are the numbers that are being measured, such as number of page views or number of sessions.

Each dimension and metric has a scope, so it’s important to understand the three different scopes:

1. User-level
1. Session-level
1. Page-level

Due to the scoping, not every dimension can be combined with every metric. In most cases, the dimensions and the metrics should match the scope.

#### Understanding Reporting

###### Setting a date range

You can use the calendar in the top right to set the active date range. You can also select the `compare to` box to compare metrics from different time periods. This will allow you to see month over month or year over year growth for the desired metrics.

###### Annotations

Annotations are used to mark a point in time in Google Analytics. They can be used to mark an important event such as a change to the setup of Google Analytics, or an event that heavily impacted traffic positively or negatively.

To create an annotation, double-click on a date. The double-click will bring up the annotation field where you can enter details, and select `private` or `public`. Public annotations can be seen by anyone that has access to that view within Google Analytics, and private annotations can only be seen by you.

###### Data Tables

Most reports have a data table below the graph. The data tables contain a dimension and associated metrics.

The `Primary Dimension` of the data tables can be changed by selecting a different primary dimension from above the table.

A secondary dimension can also be added by clicking on the `Secondary Dimension` button above the table and selecting the secondary dimension you’d like to add to the table.

#### Audience Reports

The audience reports are used to understand characteristics of your users such as location, and browser used, and user behavior over multiple visits such as average session time.

#### Acquisition Reports

The acquisition reports help you know how people find the website. These reports will help you to analyze the benefits of the different digital marketing efforts that you are involved in.

###### Channels Report

The `All Traffic > Channels` report breaks down all the different channels that are sending traffic to the site. You can click on any of the channels to drill down  and get more granular data about that specific channel. For example, if you click on the `Referral` channel, you will see which sites are referring traffic to your site.

#### Behaviors Reports

The behaviors section is about how users use the website. This includes what pages of the site people are looking at as well as how they flow through the site.

###### All Pages Report

The `All Pages` report shows the number of times a given page was viewed within the selected period of time. You can change the primary dimension to `Page Title` if it is easier to tell what the page is by looking at the title rather than the URL.

`Avg. Time on Page` and `Bounce Rate` can be used to find underperforming content or content that is very engaging to users and can be used in future marketing efforts.

###### Landing Pages and Exit Pages

The `Landing Pages` and `Exit Pages` reports are scoped at the session and tell us how many people are beginning a session at a certain page (landing page) and how many people are ending a session at a certain page (exit page).

These reports can be valuable to see what content is bringing people to the site and what content is causing people to leave the site.

###### Events

Events can be set up to track actions that people take on the website, such as clicking links or selecting drop downs. These events can be set up in Google Tag Manager and for the most part won’t require any additional code to be placed on the website.

</details>

</details>



















# Other

<details markdown="1">

<summary>show/hide this section</summary>

## Calendars
{:.no_toc}

<details markdown="1">

<summary>show/hide this section</summary>

[ TODO : Document ]

* [TODO: Homepage merchandising schedule](#)

</details>

## Tools

<details markdown="1">

<summary>show/hide this section</summary>

- [Adobe Creative Cloud / Suite](https://www.adobe.com/): Adobe Creative Cloud is a set of applications and services from Adobe Inc. that gives subscribers access to a collection of software used for graphic design, video editing, web development, photography, along with a set of mobile applications and also some optional cloud services.
- [Sketch](https://www.sketch.com/): Create, prototype, collaborate and turn your ideas into incredible products with the definitive platform for digital design.
- [Mural](https://mural.co/): MURAL is an Online Virtual Collaboration Space, Easy to Use Specially Designed for Teams. You can Post Stickies, Share Ideas, Brainstorm and Run Product Sprints.
- [Google Analytics](https://analytics.google.com/analytics/web/): Google Analytics lets you measure your advertising ROI as well as track your Flash, video, and social networking sites and applications.
- [Sisense ( previously Periscope )](https://www.sisense.com/product/data-teams/): Sisense for Cloud Data Teams (previously Periscope Data) empowers data teams to quickly connect to cloud data sources, then explore and analyze data in a matter of minutes. Extend cloud investments with the Sisense analytics platform to build, embed, and deploy analytics at scale.
- [Hotjar](https://www.hotjar.com/): Hotjar is a powerful tool that reveals the online behavior and voice of your users. By combining both Analysis and Feedback tools, Hotjar gives you the ‘big picture’ of how to improve your site's user experience and performance/conversion rates.
- [Launch Darkly](https://launchdarkly.com/): LaunchDarkly is a Feature Management Platform that serves over 100 billion feature flags daily to help software teams build better software, faster.
- [Google Optimize](https://optimize.google.com/optimize/home/): Google Website Optimizer was a free website optimization tool that helped online marketers and webmasters increase visitor conversion rates and overall visitor satisfaction by continually testing different combinations of website content.
- [Swiftype](https://swiftype.com/): Swiftype is our search provider for the about site and handbook site. We are on a legacy "business" plan where we are allowed 100,000 documents to index, 3 engines, 24 hours for partial recrawls (edited documents), and 7 days for full recrawls (new & deleted documents).

</details>

## Experiments

<details markdown="1">

<summary>show/hide this section</summary>

This section is related to A/B and multivariate testing on the marketing website, about.gitlab.com. It is a work in progress while we assess new testing tools for integration into our toolkit.

Until the toolkit assessment is finalized, please reference digital marketing's [testing documentation](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/digital-marketing-management/#create-a-culture-of-testing-and-optimization).

Going forward, we hope newly created issues align with the [growth team's testing template](https://gitlab.com/gitlab-org/growth/product/-/blob/master/.gitlab/issue_templates/Growth%20experiment.md).

**Before you experiment**

Always gather relevant heatmaps, analytics, metrics, KPI (key performance indicators), etc.

**Testing Tools**

We are in the process of establishing a new toolset for running experiments. Our hybrid suite of tools will include:

*Testing via feature flags*

This is where we plan to do the bulk of our testing. We can run several of these at the same time. For full-page, partial-page, component, and small changes.

*Feature flag best practices*

Feature flags should be implemented in code similarly to the includes system. Example:

* Login to our third party service and create a feature flag and related configuration variables.
* Assign ownership of that flag from within the interface.
* Edit a page.
* Put the existing contents of the page into an include file named `/source/experiments/1234-control.html.haml`, where experiments is the folder name instead of includes and 1234 is the id number of the associated issue. "Control" refers to the baseline measurement you are testing against.
* Duplicate that include file with the name `/source/experiments/1234-test.html.haml`
* Make your changes and validate the feature toggle works locally and/or on a review app before deployment.
* Ensure you'll be able to collect all the data you need. Setup appropriate tools like heatmaps and analytics.
* Note that one advantage of feature flags is that they can be released to production without being turned on.
* When ready, enable the test. Start gathering data.

**Testing via CDN**

This is an advanced tool meant to test large-scale changes at a systemic level. For now we plan to run only one of these at a time.

**Testing via WYSIWYG**

This is a rudimentary tool for small-scale changes with few safeguards and important caveats. We can use this for small items like colors and copy but not layout. This is mainly meant as a tool for non-developers.

</details>

</details>
