---
layout: handbook-page-toc
title: "Marketing Operations"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Marketing Operations

Marketing Operations (MktgOps) supports the entire Marketing team to streamline processes and manage related tools. Due to those tools, we often support other teams at GitLab as well. MktgOps works closely with Sales Operations (SalesOps) to ensure information between systems is seamless, data is as accurate as possible and terminology is consistent in respective systems. Not only are we assisting with marketing operations but we are also involved in the operations of marketing, such as the budget and strategies.

## Marketing Operations README's

* Dara Warde
* Claudia Beer
* Beth Peterson
* Amy Waller
* Robert Kohnke
* Nichole LaRue
* Jameson Burton
* [Sarah Daily](/handbook/marketing/marketing-operations/readme/sarah-daily.html.md)

## Important Resources
- [Marketing Metrics](/handbook/marketing/marketing-operations/marketing-metrics)
- [Marketing Owned Provisioning Instructions](/handbook/marketing/marketing-operations/marketing-owned-provisioning)
- [List Imports](/handbook/marketing/marketing-operations/list-import)

## Tech Stack  

For information regarding the tech stack at GitLab, please visit the [Tech Stack Applications page](/handbook/business-ops/tech-stack-applications/#tech-stack-applications) of the Business Operations handbook where we maintain a comprehensive table of the tools used across the company.   

The main tools used by Marketing and integrated with Salesforce are:
- [Marketo](/handbook/marketing/marketing-operations/marketo)
- [Outreach.io](/handbook/marketing/marketing-operations/outreach)
- [Drift](/handbook/marketing/marketing-operations/drift)
- [LeanData](/handbook/marketing/marketing-operations/leandata)
- [PathFactory](/handbook/marketing/marketing-operations/pathfactory)
- Sigstr
- Demandbase
- DiscoverOrg
- LinkedIn Sales Navigator
- Bizible

Other tools directly used by Marketing and maintained by Marketing Operations:  
- [Bizzabo](/handbook/marketing/marketing-operations/bizzabo)
- Cookiebot
- Disqus
- Eventbrite
- Frame.io
- Google Adwords
- Google Analytics
- Google Search Console
- Google Tag Manager
- [Litmus](/handbook/marketing/marketing-operations/litmus)
- MailChimp
- Moqups
- Rev.com
- RushTranslate
- Screaming Frog
- SEMrush
- Sprout Social
- Swiftype
- Survey Monkey
- Tweetdeck
- [YouTube](/handbook/marketing/marketing-operations/youtube/)
- Vimeo

### Requesting a new tool

If you are interested in or would like to request a new tool be added to the tech stack, [please submit an issue using the tools eval issue template](https://gitlab.com/gitlab-com/marketing/marketing-operations/issues/new?issuable_template=tools_eval) in the Marketing Operations repository. Marketing Operations should be included in new tool evaluations to account for system integrations, budget, etc.

## Working with Marketing Operations

### MktgOps Motto: If it isn't an Issue, it isn't OUR issue.   

### MktgOps Issues
The MktgOps team works from issues and issue boards. If you are needing our assistance with any project, please open an issue and use the ~MktgOps::0 - To Be Triaged label anywhere within the GitLab repo. 

With [Agile Delivery](https://about.gitlab.com/solutions/agile-delivery/) being one of the solutions that GitLab (as a product) addresses, the Marketing Operations team aims to follow many of the agile methodologies. To help us in that effort, please create any Marketing Operations issues in the following format. 

**Note:** This format is the [Agile user story](https://www.agilealliance.org/glossary/user-story-template/) format and helps the issue-requester (you) and the MOps team by clearly stating **what** as well as **why** for each request and concern.  

**Format:** `As a _____ (role in marketing), I would like to ____________(need), so that ________________(reason).`

MktgOps uses a [global issue board](https://gitlab.com/groups/gitlab-com/-/boards/825719) and will capture any issue in any group/sub-group in the repo since we work with so many other teams. There is also a [`Marketing Operations` project](https://gitlab.com/gitlab-com/marketing/marketing-operations) within the [`Marketing` project](https://gitlab.com/gitlab-com/marketing).     


We use Labels for several purposes. One is to categorize the tool or area that is affected. Another is to show priority. And lastly, we may use them to identify the stage they are in, either before or after being put into a milestone/iteration. We generally use the stage labels for our kanban Columns.

**Categories:**
- `MktgOps - FYI`: Issue is not directly related to operations, no action items for MktgOps but need to be aware of the campaign/email/event
- `MktgOps - Reporting`: Any reporting request for MktgOps
- `MktgOps - List Import`: Used for list imports of any kind - event or general/ad hoc
- `Marketo`, `Bizible`, `Cookiebot`, `Demandbase`, `DiscoverOrg`, `Drift`, `GDPR`, `LeanData`, `LinkedIn Sales Navigator`, `Outreach-io`, `PathFactory`, `Periscope`, `Sigstr`: used to highlight one of our tech stack tools

**Priorities:**
- `MktgOps-Priority::1 - Top Priority`: Issue that is related to a breaking change, OKR focus, any other prioritized project by MktgOps leadership. This category will be limited because not everything can be a priority.
- `MktgOps-Priority::2 - Action Needed`: Issue has a specific action item for MktgOps to be completed with delivery date 90 days or less from issue creation date. This tag is to be used on projects/issues not owned by MktgOps (example: list upload).
- `MktgOps-Priority::3 - Future Action Needed`: Issue has a specific action item for MktgOps, the project/issue is not owned by MktgOps and delivery or event date is 90 days or more from issue creation.

**Stages:**

*Backlog*

- `MktgOps::0 - To Be Triaged`: Issue initially created, used in templates, the starting point for any label that involves MktgOps
- `MktgOps::1 - Planning`: Issues that are currently being scoped/considered but are not being actively worked on.
- `MktgOps::2 - On Deck`: Issues that have been scoped/considered and will be added to the In Process queue next. 
- `MktgOps::5 - On Hold`: Issue that is not within existing scope of Mktg OPS current targets, blocked by MktgOps-related task/issue, or external (non-GitLab) blocker. May be a precursor to being closed out.
- `MktgOps::6 - Blocked`: Issue that is currently being worked on by Mktg Ops and at least one other team wherein MktgOps is waiting for someone else/another team to complete an action item before being able to proceed.

*In Milestone*
- `MktgOps::3 - In Process`: Issues that are actively being worked on in the current two-week/sprint
- `MktgOps::4 - UAT`: Issues that MktgOps has completed its required tasks for and is ready for User Acceptance Testing/review and approval by the Requester/Approver.
- `MktgOps::7 - Completed`: MktgOps has completed their task on this issue although the issue may not be closed. The hope is that we will be using this one less as we simply scope an issue so that MktgOps can just close it when complete.

## Operations Work Cadence   

The MktgOps team works in two week sprints/iterations which are tracked as **Milestones** at the `GitLab.com` level. Each Ops individual contributor (IC) is responsible for adding issues to the milestone that will be completed in the two week time frame. If needed, the IC will separate the main issue into smaller pieces that are *workable* segments of the larger request.   

The MktgOps team will only create a milestone one beyond the current iteration, so at any given time there will be the **current** milestone and **upcoming** milestone, any other issue that is not included will be added into future milestones &/or added as work is completed in the current milestone.   

A milestone cannot be closed nor marked complete until all associated handbook updates have been completed. Within every milestone there is an issue to track all changes and keep a running record of the handbook updates made. The handbook change issue will be created using the `milestone_handbook_update` issue template and automatically added to the [Handbook Change Epic](https://gitlab.com/groups/gitlab-com/-/epics/140).  

## No Meeting Mondays

The Marketing Operations team started a new experiment on 2020-04-20 to commit to no internal meetings on Mondays. Please try not to schedule meetings for team members on Mondays so they can devote time for deep work in milestone-related issues. Thanks!

### Operational Timeline of Changes  

Periodically Marketing Operations makes significant changes to our system and processes that affect overall tools, data and reporting or uncovers significant changes that affected reporting. As such we have an [Operational timeline of events](https://drive.google.com/open?id=1vhGvEszndMJ4B9EshGFSdTTABwUzBzDObz93vkMSFGA). The MktgOps team updates this document as needed as changes are made. 

## Marketing Expense Tracking

| GL Code | Account Name | Purpose |
| :--- | :--- | :--- |
| 6060 | Software Subscriptions |All software subscriptions |
| 6100 | Marketing|Reserved for Marketing GL accounts|
| 6110 | Marketing Site|Not used - All agency fees and contract work intended to improve the marketing site |
| 6120 | Advertising|All media buying costs as well as agency fees and software subscriptions related to media buying |
| 6130 | Events|All event sponsorships, booth shipping, event travel, booth design, event production as well as agency fees and software costs related to events |
| 6140 | Email|All 3rd party email sponsorships as well as agency fees and software costs related to mass email communications and marketing automation |
| 6150 | Brand|All PR, AR, content, swag and branding costs |
| 6160 | Prospecting|Not used - All costs related to prospecting efforts |

### Invoice Approval

Marketing Operations approves any invoices that have not already been coded and approved through a Finance issue or that exceed the original cost estimate. We make use of Tipalti for this process. Team leads will confirm that services were performed or products were received also through Tipalti. Campaign tags are used to track costs related to events and campaigns.

## Lead Scoring, Lead Lifecycle, and MQL Criteria
A Marketo Qualified Lead is a lead that has reached a certain threshold, we have determined to be 90 points accumulated, based on demographic/firmographic and/or behavioral information. The "MQL score" is comprised of various actions and/or profile data that are weighted with positive or negative point values. Every time a `Person Score` is updated, LeanData will run a check to see if the record needs to be processed through the flow.

### MQL Scoring Model
The overall model is based on a 100 point system. Positive and negative points are assigned to a record based on their demographic and/or firmographic information, and their behavior and/or engagement with GitLab marketing. You can find granular scoring on the [Marketo Page](/handbook/marketing/marketing-operations/marketo#mql-scoring-model).


## Campaign Cost Tracking
Marketing Program Managers track costs associated with campaigns - such as events, content, webcasts, etc. Campaign tags can be applied to Expensify reports, corporate credit card charges, and vendor bills processed by Accounts Payable. Campaign expenses that are incurred by independent contractors should be clearly noted with the appropriate tag and included in their invoices to the company. We then use these accumulated campaign tag costs for budget to actual analysis as well as to update the Salesforce campaigns with actual costs.

**The following steps are used to create and manage campaign tags:**

1. Event Owners create the campaign tag in the budget document as well as add a link to a Finance issue if it exists.
1. Finance is notified by the budget document to open the tag in NetSuite, which then updates Expensify nightly.
1. Event Owners create the Finance issue for approvals and the MPM issue for tracking and add the same tag to both.
1. MPM receives confirmation from responsible team (i.e. Field Marketing, Content, etc.) that the budget for the campaign has been approved and uses this as the exact same name to set up the Salesforce campaign.

**Things to Note:**

* All costs, including travel expenses for those working the event, must be tagged in order to capture the true cost of campaigns. Although travel expenses related to putting on the event hit a different GL code, they should be budgeted within the event line.
* Tagging expenses that are processed by Accounts Payable require Marketing to provide explicit instruction on when to apply tags, which should happen during the normal course of reviewing and approving vendor invoices.
* For event or campaign expenses that do not have a tag, include a note to Accounts Payable clearly stating that campaign tags are not applicable to the expense. In some cases, a general tag like Swag_Corporate may be more a more appropriate tag to track against budget.

## Marketing Gearing Ratios
Gearing ratios are used as business drivers to forecast long term financial goals by function. Refer to the [FP&A handbook](/handbook/finance/financial-planning-and-analysis/#business-drivers) for further details on how gearing ratios enable planning and forecasting. 

The gearing ratios for marketing are as follows:

- **Inquiries per MQL**: at the top of the marketing funnel, this is the conversion rate at which [Inquiries](/handbook/business-ops/resources/#glossary) become Marketo Qualified Leads ([MQL's](/handbook/business-ops/resources/#mql-definition)).

- **MQL to SAO**: this is the mid-funnel conversion rate at which MQL's become Sales Accepted Opportunities ([SAO's](/handbook/business-ops/resources/#criteria-for-sales-accepted-opportunity-sao)).

- **SAO to Closed-Won**: at the bottom of the marketing funnel, this is the rate at which SAO's move through the full pipeline to [closed won](/handbook/sales/#forecast-categories-definitions), resulting in a sale. 

- **Pipe-to-Spend**: is the ratio of pipeline created that is attributed to demand generation program spend. The target ratio is 5:1 for pipe-to-spend.

- **XDR MQL Disposition/Month**: is the capacity of an ([XDR](/handbook/marketing/revenue-marketing/xdr/)) to convert MQL's into SAO's. Total MQL's converted to SAO's / Qty of XDR's

- **New IACV Average Deal Size**: is the average deal size of new customer transactions by segment and geography

- **Marketing E/R and long-term profitability target**: is marketing operating expense divided by revenue (ratable) as shown on the income statement. The long term target profitability target for Marketing E/R is 13%. This target includes the operating expense related to free usage of gitlab.com. See gitlab financial model for yearly targets of this gearing ratio.

## Email Management

Email database management is a core responsibility for MktgOps. Ensuring GitLab is following email best practices, in compliance with Global spam laws and overall health of active database are all priorities.   

Email creation, campaigns, follow up reporting and sending is the responsibility of the Marketing Program Managers. To request an email of any kind, please see the [instructions](/handbook/business-ops/resources/#requesting-an-email) in the Business Ops section of the handbook.


### Email Communication Policy  

At GitLab, we strive to communicate with people in a way that is beneficial to them. Most of our email marketing communications follow an explicit opt-in policy, although at times, we will communicate via email to people who have not explicitly opted-in. We do this to offer something of value (ex. an invite to a workshop, dinner, the opportunity to meet an industry leader, etc. not an email inviting to read a blog post) to the person. We always include the unsubscribe link in our communications, and we respect the unsubscribe list. In addition to the unsubscribe button at the bottom of all of our emails, we have available our [Email Subscription Center](/company/preference-center/), where people can control their email communication preferences. There are currently four [email segments](/handbook/marketing/marketing-sales-development/marketing-operations//#email-segments).

### Email Segments

Database segments and how someone subscribes to specific segment:  

- **Newsletter**: Users can [subscribe to the newsletter](/company/contact/) through the blog, Contact us page, and CE download page.
- **Security Alerts**: [Subscribe to security notices](/company/contact/#security-notices) on the GitLab Contact us page.
- **Webcasts**: When someone registers to a live or on-demand webcast
- **Live Events**: When someone registers to attend a live event, meet up or in-person training. Use of this segment is narrowed down by geolocation so notification and invitation emails are specific to related area.  

### Types of Email

**Breaking Change Emails**  
These are transactional emails, almost always to our user base, that provide very selective needed information. This is an operational-type email that overrides the unsubscribe and would not need to comply with marketing email opt-out. Usage example: GitLab Hosted billing change, Release update 9.0.0 changes, GitLab Page change and Old CI Runner clients.
It is very important to have Engineering and/or Product team (whoever is requesting this type of email) help us narrow these announcements to the people that actually should be warned, so we are communicating to a very specific targeted list.

**Newsletter**  
Sent bi-monthly (every 2 weeks). Content Team is responsible for creating the content for each Newsletter.  

**Security Alerts**  
Sent on an as needed basis containing important information about any security patches, identified vulnerabilities, etc. related to the GitLab platform. These emails are purely text based and again are transactional in nature.

**Webcasts**   
Invitation and/or notification emails sent about future webcasts.   

**Live Events**   
Invitation emails to attend a live event (VIP or Executive Lunch), meet-up, or in-person training. These emails are sent to a geo-locational subset of the overall segment. This type of email is also used when we are attending a conference and want to make people aware of any booth or event we may be holding and/or sponsoring.


## Website Form Management

The forms on about.gitlab are embedded Marketo forms. Any changes to the fields, layout, labels and CSS occur within Marketo and can be pushed live without having to make any changes to the source file on GitLab. When needing to change or embed a whole new form, ping MktgOps on the related issue so appropriate form and subsequent workflows can be created.

Each Marketo form should push an event after successful submission to trigger events in Google Analytics. We use the following event labels to specify which events to fire.

1. `demo` for static demos on `/demo/` and `/demo-leader/`
1. `webcasts` for forms on any page in `/webcast/`
1. `trial` for the form on `/free-trial/`
1. `resources` for forms on any page in `/resources/`
1. `events` for forms on any page in `/events/`
1. `services` for form on `/services/`
1. `sales` for form on `/sales/`
1. `public-sector` for forms on `/solutions/public-sector/`
1. `mktoLead` legacy custom event label used on Newsletter subscription form submission events. Currently used for primary, security, and all-remote newsletter form submissions.

We add the following line above `return false` in the form embed code. Please update the event label from `demo` to reflect the appropriate form completion.

```
dataLayer.push(
{
  'event' : 'demo', 
  'mktoFormId' : form.getId(),
  'eventCallback' : function()
  {}, 'eventTimeout' : 3000
});
```

In the event Marketo has an outage and/or the forms go offline, the forms with highest usage/visibility (Free Trial and Contact Us) have been recreated as Google forms that can be embedded on the related pages as a temporary measure to minimize any effect till the outage is past.

## Initial Source
`Initial Source` is first "known" touch attribution or when a website visitor becomes a known name in our database, once set it should never be changed or overwritten. For this reason Salesforce is set up so that you are unable to update both the `Initial Source` and `Lead Source` fields. If merging records, keep the `Initial Source` that is oldest (or set first). When creating Lead/Contact records and you are unsure what `Initial Source` should be used, ask in the `#lead-questions` Slack channel.

The values listed below are the only values currently supported. If you attempt to upload or import leads or contacts into Salesforce without one of these initial sources you will encounter a validation rule error. If you think that there needs to be a new Initial Source added to this list and into Salesforce please slack the appropriate team member(s) listed in the [Tech Stack](/handbook/business-ops/tech-stack-applications/#tech-stack-applications).

The `Initial Source` table below is current as of 9 October 2019.

Status in the table below means:
- Active = can be selected from picklist
- Inactive = cannot be selected from picklist, but a record may exist with this source

| Source                          | Definition and/or transition plan                                                                            | Status*  |
|:--------------------------------|:-------------------------------------------------------------------------------------------------------------|:---------|
| Advertisement                   | to be evaluated                                                                                              | Active   |
| AE Generated                    | Sourced by an Account Executive through networking or professional groups                                    | Active   |
| CE Download                     | Downloaded CE version of GitLab                                                                              | Active   |
| CE Usage Ping                   | Created from CE Usage Ping data                                                                              | Active   |
| CE Version Check                | to be evaluated                                                                                              | Inactive |
| Clearbit                        | transition to `Prospecting` -> sub field `Clearbit`                                                          | Active   |
| Conference                      | Stopped by our booth or received through event sponsorship                                                   | Active   |
| CORE Check-Up                   | will be activated for new records created by the Instance Review in-product                                  | Inactive |
| Datanyze                        | transition to `Prospecting` -> sub field `Datanyze`                                                          | Active   |
| Demo                            | Filled out form to watch demo of GitLab                                                                      | Active   |
| DiscoverOrg                     | transition to `Prospecting` -> sub field `DiscoverOrg`                                                       | Active   |
| Education                       | Filled out form applying to the Educational license program                                                  | Active   |
| EE Version Check                | to be evaluated                                                                                              | Inactive |
| Email Request                   | Used when an email was received through an alias (*will be deprecated*)                                      | Active   |
| Email Subscription              | Subscribed to our opt-in list either in preference center or various email capture field on GitLab website   | Active   |
| Employee Referral               | to be evaluated                                                                                              | Active   |
| Event partner                   | to be evaluated                                                                                              | Inactive |
| Field Event                     | Paid events we do not own but are active participant (Meetups, Breakfasts, Roadshows)                        | Active   |
| Gated Content - General         | Download an asset that does not fit into the other Gated Content categories                                  | Active   |
| Gated Content - eBook         | Download a digital asset categorized as an eBook                                 | Active   |
| Gated Content - Report          | Download a gated report                                                                                      | Active   |
| Gated Content - Video           | Watch a gated video asset                                                                                    | Active   |
| Gated Content - Whitepaper     | Download a white paper                                                                                       | Active   |
| Gemnasium                       | Previous a Gemnasium customer/prospect merged into our database when acquired                                | Active   |
| GitLab Hosted                   | GitLab Hosted customer/user                                                                                  | Active   |
| GitLab Subscription Portal      | Account created through the Subscription app (check for duplicates & merge record if found)                  | Inactive |
| GitLab.com                      | Registered for GitLab.com account                                                                            | Active   |
| Gitorious                       | Previous a Gitorios customer/prospect merged into our database                                               | Active   |
| gmail                           | unknown, to be deprecated                                                                                    | Inactive |
| InsideView                      | transition to `Prospecting` -> sub field `InsideView`                                                        | Inactive |
| Leadware                        | transition to `Prospecting` -> sub field `Leadware`                                                          | Active   |
| Legacy                          | to be evaluated                                                                                              | Active   |
| LinkedIn                        | transition to `Prospecting` -> sub field `LinkedIn`                                                          | Active   |
| Live Event                      | transition to correct category based on first event attended -> `Owned Event`; `Field Event` or `Conference` | Active   |
| MovingtoGitLab                  | to be evaluated                                                                                              | Inactive |
| Newsletter                      | to be evaluated                                                                                              | Active   |
| OnlineAd                        | to be evaluated                                                                                              | Inactive |
| OSS                             | Open Source Project records related to the OSS offer for free licensing                                      | Active   |
| Other                           | Should never be used but is a legacy source that will be deprecated                                          | Active   |
| Owned Event                     | Events that are created, owned, run by GitLab                                                                | Active   |
| Partner                         | GitLab partner sourced name either through their own prospecting and/or events                               | Active   |
| Promotion                       | to be evaluated                                                                                              | Active   |
| Prospecting                     | Account research and development prospecting work                                                           | Pending  |
| Prospecting - LeadIQ            | transition to `Prospecting` -> sub field `LeadIQ`                                                            | Active   |
| Public Relations                | to be evaluated                                                                                              | Active   |
| Referral                        | to be evaluated                                                                                              | Inactive |
| Registered                      | transition to correct event type source                                                                      | Inactive |
| Request - Contact               | Filled out contact request form on GitLab website                                                            | Active   |
| Request - Professional Services | Any type of request that comes in requesting to engage with our Customer Success team                        | Active   |
| Sales                           | to be evaluated                                                                                              | Inactive |
| SDR Generated                   | Sourced by an SDR through networking or professional groups                                                  | Active   |
| Security Newsletter             | Signed up for security alerts                                                                                | Active   |
| Seminar - Partner               | not actively used - transition to `Owned Event` or `Field Event`                                             | Active   |
| SocialMedia                     | to be evaluated                                                                                              | Inactive |
| Swag Store                      | to be evaluated                                                                                              | Inactive |
| Trial - Enterprise              | In-product or web request for self-hosted Enterprise license                                                 | Active   |
| Trial - GitLab.com              | In-product SaaS trial request                                                                                | Active   |
| Unknown                         | need to evaluate what records are in this status - it should never be used                                   | Inactive |
| Unsubscribe Form                | to be evaluated                                                                                              | Inactive |
| Web                             | transition to `Web Direct`                                                                                   | Active   |
| Web Chat                        | Engaged with us through website chat bot                                                                     | Active   |
| Web Direct                      | Created when purchase is made direct through the portal (check for duplicates & merge record if found)       | Active   |
| Webcast                         | Register for any online webcast (not incl `Demo`)                                                            | Active   |
| Word of Mouth                   | to be evaluated                                                                                              | Active   |
| Zoominfo                        | transition to `Prospecting` -> sub field `Zoominfo`                                                          | Inactive |

## Lead and Contact Statuses
The Lead & Contact objects in Salesforce have unified statuses with the following definitions. If you have questions about current status, please ask in #lead-questions channel on Slack.

| Status | Definition |
| :--- | :--- |
| Raw | Untouched brand new lead |
| Inquiry | Form submission, meeting @ trade show, content offer |
| MQL | Marketo Qualified through systematic means |
| Accepted | Actively working to get in touch with the lead/contact |
| Qualifying | In 2-way conversation with lead/contact |
| Qualified | Progressing to next step of sales funnel (typically OPP created & hand off to Sales team) |
| Unqualified | Contact information is not now or ever valid in future; Spam form fill-out |
| Nurture | Record is not ready for our services or buying conversation now, possibly later |
| Bad Data | Incorrect data - to potentially be researched to find correct data to contact by other means |
| Web Portal Purchase | Used when lead/contact completed a purchase through self-serve channel & duplicate record exists |

### Marketo Program and Salesforce Campaign set-up

#### Steps to Setup Marketo programs and Salesforce Campaigns

The Marketo programs for the corresponding campaign types have been prebuilt to include all the possible necessary smart campaigns, email programs, reminder emails and tokens that are to be leveraged in the building of the program.

For **Content Syndication**, follow the instructions documented in [the Content Syndication section](https://about.gitlab.com/handbook/marketing/marketing-operations/#steps-to-setup-content-syndication-in-marketo-and-sfdc).

For all other campaign types, follow Steps 1-5 below. All steps are required.

##### Step 1: Clone the Marketo program indicated below
* Sponsored Webcast: [YYYYMMDD_HostName_Topic (External Webcast Template)](https://app-ab13.marketo.com/#ME4634A1)
* Virtual Conference: [YYYYMMDD_Vendor_VirtualConfName (Virtual Conference Template)](https://app-ab13.marketo.com/#ME4739A1) 
* Self-hosted Webcast or Self-hosted Webcast with Promotion: [YYYYMMDD_WebcastTopic_Region (Single time slot)](https://app-ab13.marketo.com/#ME3845A1)   
* (MPM use only) Conference: [YYYYMMDD_Conference_Template](https://app-ab13.marketo.com/#ME5100A1)
* (MPM use only) Conference Speaking Session: [YYYYMMDD_SpeakingSession_Template](https://app-ab13.marketo.com/#ME5092A1)
* (MPM use only) Field Event: [YYYYMMDD_FieldEvent_Template](https://app-ab13.marketo.com/#ME5083A1)
* (MPM use only) Gated Content: [YYYY_Type_Content_Template](https://app-ab13.marketo.com/#PG5111A1)
* (MPM use only) PathFactory Listener: [TEMPLATE - `PF - Asset Type - Name of Asset`](https://app-ab13.marketo.com/#PG3875A1)
* (MPM use only) Integrated Campaign: [FY20IntegratedCampaign_Template](https://app-ab13.marketo.com/#PG4924A1)
* (MPM use only) Owned Event: [YYYYMMDD_OwnedEvent_Template](https://app-ab13.marketo.com/#ME4722A1)
* (MPM use only) GitLab Hosted Webcast (single timeslot): [YYYYMMDD_WebcastTopic_Region (Single time slot)](https://app-ab13.marketo.com/#ME3845A1)
* (MPM use only) GitLab Hosted Webcast (multi timeslot): [YYYYMMDD_WebcastTopic_Region (Multiple Time Slot)](https://app-ab13.marketo.com/#MF3195A1)
* **Name the program using the campaign tag**

##### Step 2: Sync to Salesforce
* At the program main screen in Marketo, where it says `Salesforce Sync` "not set", click on "not set"
    * Click "Create New." The program will automatically populate the campaign tag, so you do not need to edit anything.
    * Click "Save"

##### Step 3: Update Marketo tokens
* Complete the information for each token. Instructions for what to enter for each token are included in the template.
   * Note that it is important that all tokens are completed as the "Interesting Moments" Smart Campaigns pushes information to Salesforce based on the tokens. Depending on the campaign, some auto-responders and emails rely on tokens as well.
   * Self-hosted Webcast or Self-Hosted Webcast with Promotion: You do not need to update the following tokens:
        * {{my.apiKey}} - Leave this as-is, no changes
        * {{my.apiSecret}} - Leave this as-is, no changes
        * ((my.email header image url}} - You will need this if you had custom images created. Otherwise, an MPM can help on a generic image.
        * {{my.ondemandurl}} - This will be entered AFTER the event date. It is the link to the recorded webcast.
        * {{my.zoomWebinarId}} - Leave this blank. 

* Update the utm_campaign field using the following format: Campaign Tag, with no spaces, capitalization, underscores, or special characters.

##### Step 4: Activate Marketo smart campaign(s)
* Click the "Smart Campaigns" folder
* Select the `Interesting Moments` smart campaign. 
* The correct program should automatically apply when cloned, so *you don't need to do anything here.* However, you can confirm that the campaign tag appears on in the Smart List and Flow. If the name of the template appears anywhere, replace it with the campaign tag.
* Click to the "Schedule" tab and click `Activate`.

##### Step 5: Update the Salesforce campaign

* Now go to Salesforce.com and check the [All Campaigns by create date](https://gitlab.my.salesforce.com/701?fcf=00B4M000004oVF9) view. Sort by create date and your campaign should appear at the top. You may also search for your campaign tag in the search box. Select the campaign.
    * Change the `Campaign owner` to your name
    * Change the `Enable Bizible Touchpoints` to `Include only "Responded" Campaign Members`    
    * Confirm that start date and end date populated correctly (this is automated). For events and webcasts, start date is 30 days prior to the event date and 60 days after. For all other campaigns, the start date is the date of launch, end date is 90 days from the date of launch (or if the campaign runs longer, update to the appropriate end date).
    * Update the event epic
    * Update the description (if any)
    * Update `Budgeted Cost`
    * Update `Region` and `Sub-region`, if these are local or targeted to a specific region
    * Click "Save"

#### Steps to Setup Content Syndication in Marketo and SFDC

##### Step 1: [Clone this program](https://app-ab13.marketo.com/#PG5149A1)
* Use format `YYYY_Vendor_NameofAsset`
* If the content syndication is part of a package with an external vendor, promoting several assets or webcasts, keep all of the Marketo programs together in a folder for easy access as part of a single vendor program.

##### Step 2: Sync to Salesforce
* At the program main screen in Marketo, where it says `Salesforce Sync` with "not set", click on "not set"
    * Click "Create New." The program will automatically populate the campaign tag, so you do not need to edit anything.
    * Click "Save" 

##### Step 3: Update Marketo tokens
* Change the `Content Title` to be the title as it appears in the Content Syndication program
* Change the `Content Type` to be the type of content
   * The only available options are `Whitepaper`, `eBook`, `Report`, `Video`, or `General`
   * If you add a Content Type value other than the above, the record will hit an error when syncing to Salesforce because these are the only currently available picklist items for `Initial Source`

##### Step 4: Activate Marketo smart campaign
* In the `01 Downloaded` smart campaign, the "Smart List" should be listening for `Added to List > Vendor List`. This list is under the Asset folder in the program. It will contain all of the members that were uploaded who downloaded the content.
   * The correct program should automatically apply when cloned, so *you don't need to do anything here.*
* In the `01 Downloaded` smart campaign, the "Flow" will trigger a program status change `Content Syndication > Downloaded`, that will trigger a scoring update. An interesting moment to be applied, the `Person Source` (note: this maps to `Initial Source` in Salesforce) will update IF a `Person Source` does not already exist (i.e. it is blank), the `Acquisition Program` will set if blank, the Marketo `Initial Source` will populate if blank, and the `Person Status` will update to `Inquiry` if `Blank` or `Raw`.
* Click to the "Schedule" tab and click `Activate`. It should be set that a person can only run through the flow once.
    * When the leads are loaded to the campaign by Marketing Ops, the leads will immediately have an interesting moment, +15 score, and initial source, person source and person status update as needed.

##### Step 5: Update the Salesforce campaign
* Now go to Salesforce.com and check the [All Campaigns by create date](https://gitlab.my.salesforce.com/701?fcf=00B4M000004oVF9) view. Sort by create date and your campaign should appear at the top. You may also search for your campaign tag in the search box. Select the campaign.
    * Change the `Campaign Owner` to your name
    * Change the `Enable Bizible Touchpoints` to `Include only "Responded" Campaign Members`
    * Update the event epic
    * Update the description
    * Update `Start Date` to the date of launch
    * Update `End Date` to 90 days from date of launch (if this is an ongoing campaign, update appropriately)
    * Upaded `Budgeted Cost` if you have the data available
    * Click Save

## Campaigns
Campaigns are used to track efforts of marketing tactics - field events, webcasts, content downloads. The campaign types align with how marketing tracks spend and align the way records are tracked across three of our core systems (Marketo, Salesforce and Bizible) for consistent tracking. Leveraging campaign aligns our efforts across Marketing, Sales and Finance.

#### Salesforce campaigns

### Campaign Type & Progression Status
A record can only progress **one-way** through a set of event statuses. A record *cannot* move backward though the statuses.

i.e. Record is put into `Registered` cannot be moved backward to `Waitlisted`


#### Cohort
A method of tracking a group (cohort) of targeted known records and/or records included in an ABM strategy. All touchpoints related to this specific campaign are excluded from Bizible tracking.

| Member Status                   | Definition                                                                            | Success  |
|:--------------------------------|:--------------------------------------------------------------------------------------|:---------|
| No Action                       | default starting position for all records                                             |          |
| Sales Nominated                 | ACCOUNTS/CONTACTS Sales has identified for inclusion that Marketing would otherwise be suppressing because of late-stage open opps &/or active sales cycle |        |
| Marketing List               | ACCOUNTS/CONTACTS Marketing has identified for inclusion based on the target audience, the "ABM list", demographic, etc.                    |          |
| Organic Engaged           | LEADS/CONTACTS added to the campaign through the listening campaigns that engage with the pages &/or assets for the integrated campaign that do not contain `utm_` params        | Yes      |


#### Conference
Any large event that we have paid to sponsor, have a booth/presence and are sending representatives from GitLab (example: AWS). This is tracked as an *offline* Bizible channel.

| Member Status                   | Definition                                                                            | Success  |
|:--------------------------------|:--------------------------------------------------------------------------------------|:---------|
| No Action                       | default starting position for all records                                             |          |
| Sales Invited                   | Invitation/Information about event sent by Sales/SDR                                  |          |
| Sales Nominated                 | Sales indicated record to receive triggered event email sent by Marketing             |          |
| Marketing Invited               | Marketing geo-targeted email                                                          |          |
| Meeting Requested               | Meeting set to occur at conference                                                    |          |
| Meeting No Show                 | Scheduled meeting at conference was cancelled or not attended                         |          |
| Meeting Attended                | Scheduled meeting at conference was attended                                          | Yes      |
| Visited Booth                   | Stopped by booth for any reason                                                       | Yes      |
| Follow Up Requested             | Requested additional details about GitLab to be sent post event                       | Yes      |

#### Field Event
This is an event that we have paid to participate in but do not own the registration or event hosting duties (example: Rancher event). This is tracked as an *offline* Bizible channel.

| Member Status                   | Definition                                                                            | Success  |
|:--------------------------------|:--------------------------------------------------------------------------------------|:---------|
| No Action                       | default starting position for all records                                             |          |
| Sales Invited                   | Invitation/Information about event sent by Sales/SDR                                  |          |
| Sales Nominated                 | Sales indicated record to receive triggered event email sent by Marketing             |          |
| Marketing Invited               | Marketing geo-targeted email                                                          |          |
| Waitlisted                      | Holding state if registration is full will be moved to `Registered` if space opens    |          |
| Registered                      | Registered for event                                                                  |          |
| No Show                         | Registered but did not attend event                                                   |          |
| Attended                        | Attended event                                                                        |          |
| Visited Booth                   | Stopped by booth for any reason                                                       | Yes      |
| Follow Up Requested             | Requested additional details about GitLab to be sent post event                       | Yes      |

#### Gated Content
White Paper or other content offer. This is tracked as an *online* Bizible channel.

| Member Status                   | Definition                                                                            | Success  |
|:--------------------------------|:--------------------------------------------------------------------------------------|:---------|
| No Action                       | default starting position for all records                                             |          |
| Downloaded                      | Downloaded content                                                                    | Yes      |

#### Geographic  
We have specific geographic DMA lists that are used for field marketing and marketing programs to target event invitations. This is **not** tracked with Bizible touchpoints or channels. This campaign type is only used for visibility of our DMA lists - [click to see full list](/handbook/marketing/marketing-operations/marketo#geographic-dma-list) of DMAs available. 


| Member Status | Definition       | Success |
| :------------ | :-----------     | :------ |
| Member        | A record is a member of this DMA list | n/a this is a cohort campaign type not awarding touchpoints | n/a - this campaign type *does not* have a success metric |


#### Inbound Request
Any type of inbound request that requires follow up. This is tracked as an *online* Bizible channel.

| Member Status                   | Definition                                                                            | Success  |
|:--------------------------------|:--------------------------------------------------------------------------------------|:---------|
| No Action                       | default starting position for all records                                             |          |
| Requested Contact               | Filled out Contact, Professional Services, Demo or Pricing Request                    | Yes      |


#### List Build
A static list built for ad hoc requests by the FMM or MPM team. This campaign type **does not** apply any touchpoints and is **not** tracked as a Bizible channel. 

| Member Status                   | Definition                                                                            | Success  |
|:--------------------------------|:--------------------------------------------------------------------------------------|:---------|
| No Action                       | default starting position for all records                                             |          |
| Sales Nominated               | Any record proactively identified by Sales to be included in the campaign                    |        |
| Marketing List                       | Any record identified by targeting filters applied by Marketing Operations to build the initial list                                             |          |
| Organic Engaged               | Occasionally used when we are wanting to track & include any records engaging with specific marketing web pages                    | Yes      |


#### Owned Event
This is an event that we have created, own registration and arrange speaker/venue (example: Gary Gruver Roadshow). This is tracked as an *online* Bizible channel.


| Member Status                   | Definition                                                                            | Success  |
|:--------------------------------|:--------------------------------------------------------------------------------------|:---------|
| No Action                       | default starting position for all records                                             |          |
| Sales Invited                   | Invitation/Information about event sent by Sales/SDR                                  |          |
| Sales Nominated                 | Sales indicated record to receive triggered event email sent by Marketing             |          |
| Marketing Invited               | Marketing geo-targeted email                                                          |          |
| Waitlisted                      | Holding state if registration is full will be moved to `Registered` if space opens    |          |
| Registered                      | Registered for event                                                                  |          |
| No Show                         | Registered but did not attend event                                                   |          |
| Attended                        | Attended event                                                                        | Yes      |
| Follow Up Requested             | Requested additional details about GitLab to be sent post event                       | Yes      |


#### PathFactory Listener
This campaign type is used to track consumption of specific PathFactory assets. This is tracked as an *offline* Bizible Channel and touchpoint. Details related to types of assets being tracked can be found on the [Marketing Operations - PathFactory](/handbook/marketing/marketing-operations/pathfactory/#listening-campaigns) page. 

| Member Status                   | Definition                                                                            | Success  |
|:--------------------------------|:--------------------------------------------------------------------------------------|:---------|
| No Action                       | default starting position for all records                                             |          |
| Content Consumed                   | Status when the corresponding Marketo listener picks up the contents consumption.                                  | Yes         |



#### Referral Program
This campaign type is used for our third party prospecting vendors or meeting setting services (Like BAO, DoGood). This is tracked as an *offline* Bizible Channel and touchpoints.

| Member Status                   | Definition                                                                            | Success  |
|:--------------------------------|:--------------------------------------------------------------------------------------|:---------|
| No Action                       | default starting position for all records                                             |          |
| Target List               | Identified as persona we want to speak with                    |          |
| Meeting Set               | Vendor has set & confirmed a meeting time                    |          |
| Meeting No Show               | Scheduled meeting was cancelled or not attended                    |          |
| Meeting Attended               | Scheduled meeting at conference was attended                     | Yes      |


#### Speaking Session
This campaign type can be part of a larger Field/Conference/Owned event but we track engagement interactions independently from the larger event to measure impact. It is something we can drive registration. It is for tracking attendance at our speaking engagements. This is tracked as an *offline* Bizible channel.


| Member Status                   | Definition                                                                            | Success  |
|:--------------------------------|:--------------------------------------------------------------------------------------|:---------|
| No Action                       | default starting position for all records                                             |          |
| Sales Invited                   | Invitation/Information about event sent by Sales/SDR                                  |          |
| Sales Nominated                 | Sales indicated record to receive triggered event email sent by Marketing             |          |
| Marketing Invited               | Marketing geo-targeted email                                                          |          |
| Registered                      | Registered or indicated attendance at the session                                     |          |
| No Show                         | Registered but did not attend event                                                   |          |
| Attended                        | Attended speaking session event                                                       | Yes      |
| Follow Up Requested             | Had conversation with speaker or requested additional details to be sent post event   | Yes      |


#### Trial
Track cohort of Trials for each product line (Self-hosted or SaaS) to see their influence. In-product trials are tracked as an **offline** Bizible touchpoint. Webform Self-hosted trials are an **online** Bizible touchpoint.

| Member Status                   | Definition                                                                            | Success  |
|:--------------------------------|:--------------------------------------------------------------------------------------|:---------|
| No Action                       | default starting position for all records                                             |          |
| Target List               | LEAD or CONTACT record that has been identified for marketing campaign prospecting                    | Yes      |


#### Virtual Sponsorship
A virtual event that we sponsor and/or participate in that we do not own the registration but will generate a list of attendees, engagement and has on-demand content consumption post-live virtual event. This is tracked as an *offline* Bizible channel.

| Member Status                   | Definition                                                                            | Success  |
|:--------------------------------|:--------------------------------------------------------------------------------------|:---------|
| No Action                       | default starting position for all records                                             |          |
| Sales Invited                   | Invitation/Information about event sent by Sales/SDR                                  |          |
| Sales Nominated                 | Sales indicated record to receive triggered event email sent by Marketing             |          |
| Marketing Invited               | Marketing targeted email                                                              |          |
| Waitlisted                      | Holding state if registration is full will be moved to `Registered` if space opens    |          |
| Registered                      | Registered for event                                                                  |          |
| No Show                         | Registered but did not attend event                                                   |          |
| Attended                        | Attended event                                                                        |          |
| Visited Booth                   | Stopped by booth for any reason                                                       | Yes      |
| Follow Up Requested             | Requested additional details about GitLab to be sent post event                       | Yes      |
| Attended On-demand              | Watched/consumed the presentation materials post-event on-demand                      | Yes      |

#### Webcast
Any webcast that is held by GitLab or a sponsored webcast with a partner. This is tracked as an *online* Bizible channel.

| Member Status                   | Definition                                                                            | Success  |
|:--------------------------------|:--------------------------------------------------------------------------------------|:---------|
| No Action                       | default starting position for all records                                             |          |
| Sales Invited                   | Invitation/Information about event sent by Sales/SDR                                  |          |
| Sales Nominated                 | Sales indicated record to receive triggered event email sent by Marketing             |          |
| Marketing Invited               | Marketing geo-targeted email                                                          |          |
| Registered                      | Registered through online form                                                        |          |
| No Show                         | Registered but did not attend live webcast                                            |          |
| Attended                        | Attended the live webcast                                                             | Yes      |
| Attended On-demand              | Watched the recorded webcast                                                          | Yes      |


## Bizible Attribution

 In 4Q18, we are making updates to the Bizible Channel rules, but currently, these channels and subchannels are pulled into Salesforce and can be further filtered by using `medium` for those channels with overlap or with `Ad Campaign name` to search for specific UTMs or campaigns:

| Bizible Online Channel or subchannel | Type of marketing |SFDC Marketing Channel-path |
|---|---|---|
|`CPC`|Google Adwords or other Paid Search|CPC.Adwords|
|`Display`|Display ads in Doubleclick, Terminus, etc|Display|
|`Paid Social`|Ads in Facebook or LinkedIn |Paid Social.[Name of site]|
|`Organic`|Organic search|Marketing Site.Organic|
|`Other`|Not specifically defined |[Name of channel].Other|
|`Partners`|Google or events|	Marketing Site.Web Direct|
|`Email`|Nurture, Newsletter, Outreach emails|Email.[Name of email type]|
|`Field Event`|From Field event, will show Salesforce campaign as touchpoint source|Email.[Field Event]|
|`Conference`|From conference, will show Salesforce campaign as touchpoint source|Conference|
|`Social`|Any referral from any defined social media site| Social.[Name of site]|
|`Sponsorship`|Paid sponsorships, Display, and Demand gen as well as Terminus|Sponsorship|
|`Web Direct`|Unknown or direct (NOTE: this is not the same as Web direct/self-serve in SFDC, this is a Web referral where the original source was not captured)|Marketing Site.Web Direct|
|`Web Referral`|Referral from any site not otherwise defined|Marketing Site.Web Referral|

