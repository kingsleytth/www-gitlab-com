---
layout: markdown_page
title: "Use case: Version Control and Collaboration"
noindex: true
---



## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

#### Who to contact

| Product Marketing | Technical Marketing |
| ---- | --- |
| [Jordi Mon Companys](https://gitlab.com/jordi_mon)  | [William Galindez Arias](https://gitlab.com/warias) |

## Version Control and Collaboration

As organizations accelerate delivery, through DevOps, controlling and managing different versions of the application assets from code to configuration and from design to deployment is incredibly important.
Velocity without robust version control and traceability is like driving a car with out a seatbelt.

Version Control and Collaboration is more than simply tracking changes, versions, and branches of code. Effectively, it includes practices such as:

- Enabling development teams to work in distributed and asynchronously
- Managing changes and versions of code and artifacts
- Enabling *Review* and *Collaboration* of code and other assets
- Tracking approvals of proposed changes
- Resolving merge conflicts and related anomalies

In general, Version Control and Collaboration is required because software is constantly changing. Regardless of the stage of development, there will be change to deal with.

> No matter where we are in the system life cycle, the system will change, and the desire to change it will persist throughout the life cycle.

> E.H. Bersoff, 1980.

Changes are of different nature, here a short list:

| Change | Outcome |
| --- | --- |
| Requirements or features may change in scope | Business changes |
| New team members may join | Stakeholder changes |
| Docs are updated all the time | Technical changes |
| Dependencies may have changed | Technical changes |
| The actual code will change!| Technical changes |

The multiplicity of changes involve a myriad of users and stakeholders which bring to the picture their motivations and initiatives to the usage of version control and collaboration. The next exercise tries to condense them into user and buyer personas.

## Personas

### User Persona
Being the entry point to GitLab means that many user personas find utility and a solution to their problem in Version Control and Collaboration. Let's go through the list of power user personas and describe briefly their key motivations to use Version Control and Collaboration in GitLab:

#### [Parker the Product Manager](/handbook/marketing/product-marketing/roles-personas/#parker-product-manager)
- PMs coordinate feature development and project success among other things. The ability to monitor progress through commits, review app and validate those changes and provide feedback is key to they success of their role
- These changes, in time will generate valuable statistical insight for the PM to assess accurately development efforts of planned new features

#### [Delaney the Development Team Lead](/handbook/marketing/product-marketing/roles-personas/#delaney-development-team-lead)
- Just like the PM, Team Leads need also to understand their team's capacity to assign upcoming tasks to meet goals on time accordingly
- Approval workflows in Code Review allows them to become faster real team work enablers

#### [Sasha the Software Developer](/handbook/marketing/product-marketing/roles-personas/#sasha-software-developer)
- Sasha takes advantage of both Command Line Tools and GitLab's GUI to have complete control of every commit he does to complete his tasks
- Even when scope changes, a frequent hurdle and a source of frustration, branching, merging and conflict resolution will be performed in Version Control and Collaboration and will trigger CI for fast resolution

#### [Devon the DevOps Engineer](/handbook/marketing/product-marketing/roles-personas/#devon-devops-engineer)
- All information relevant to the role's goals is congregated in Version Control and Collaboration to take action on it. Time to resolution and, in general, any other key metric in DevOps is measured and its performance tracked in Version Control and Collaboration
- Any improvement applied to the development process will reflect in Version Control and Collaboration's interfaces, whether its the Merge Requests or Issues.

### Buyer Personas
Version Control and Collaboration purchasing typically do not require executive involvement. It is usually acquired and installed via our freemium offering without procurement or IT's approval. This process is commonly known as shadow IT and its a great opportunity for us to eventually become a paid for service. When the upgrade is required the [VP of IT](https://docs.google.com/presentation/d/17Ucpgxzt1jSCs83ER4-LdDyEuermpDuriugPNYrz8Rg/) is the most frequent decision maker. The influence of the [VP Application Development](/handbook/marketing/product-marketing/roles-personas/buyer-persona/#alex---the-application-development-manager) is notable too to the owner of the budget.

## Industry Analyst Resources

Research relevant to this use case can be found in the [Analyst Reports - Use Cases](https://docs.google.com/spreadsheets/d/1vXpniM08Ql0v0yDd22pcNmXpDrA-NInJOwj25PRuHXA/edit#gid=2123394945) spreadsheet.


## Market Requirements

Market Requirements is a collection of capabilites we recognize are present in the Version Control & Collaboration use case. People looking to solve this use case will consider fundamental that at least one, if not all of these requirements is present in the solution they implement. To gather these in a way that represents the market with acuracy we collect data from several sources like analysts, users, competitors and thought leaders.  

| Market Requirement | Description | Typical features that enable this capability | Value / ROI |
|---|---|---|---|
| **Protect and secure assets** | The solution provides mechanisms to host assets (repos), place and manage different change permissions for the users that access those repos as well as keep a detailed chain of custody of all changes these assets are subject of. | Single sign-on, code ownership, change reviews, change approvals, IP Allowlist/Denylist, Activity stream, GPG signed commit, Reject unsigned commits, Protected branches, branching, committer access rules, Compliance dashboard etc. | Secures IP and valuable assets. Provides information on project history changes   |
| **Enterprise Ready** | The solution is robust enough to make critical functionality available at large scale, widely distributed teams in highly regulated markets  | High Availability, Git protocol v2 support, Deduplicate Git objects for forked repositories, Download single repository files, fast and responsive application, project templates, file templates, access controls, and traceability.  | Prevents outages and disruptions of development team work. Enables traceability to authors of changes to address defects or bugs in the product and auditability throughout |
| **Supports numerous assets** | The solution is able to manage and maintain the version history of the diverse assets and support the development patterns that each asset implies | Component reuse, traceability, design management, branching, diffing, merging, object storage, design versioning | Able to manage assets and files for the entire development team, no matter how diverse, creating a single source of truth for the product configuration and making visibility and communication available at every level |
| **Foster Collaboration** | The solution is designed to enable and foster collaboration among team members. It also streamlines agreed collaboration with automation of repetitive tasks | Create fast new branches of the project, add new files/assets, collaborate on proposed changes, review comments, suggest changes, webIDE, suggestion approvals, conflict resolution, merge, diffing, hand-offs, Design management and operations, workflow automation, Wiki, snippets, version controlled snippets, Automatically update or close related issue(s) when a merge request is merged, Configurable issue closing pattern, display merge request status for builds in CI system, visibility into security scans and build stats. | Code quality increase and improved release velocity through team review and validation. |
| **Secure Development** | The solution allows for security practices to be enabled at the creation phases of the project | Dependency scanning, SAST, License scanning, DAST, Container scanning | Increasing resilience to external attacks, internal threats and ability to resume activity promptly |
| **System and Architecture design** | The solution should support all the architectural decisions made and provide functionality to make concurrent work with the different modules easy. It should also connect the higher level of abstraction that a software architecture is to the lower level system components. The solution should play well with different levels of component decoupling, from large monorepos to service-oriented architectures (microservices being an example of it)  | [Repo API](https://docs.gitlab.com/ee/api/repositories.html)[Git Submodules](https://docs.gitlab.com/ee/ci/git_submodules.html), [Submodules API](https://docs.gitlab.com/ee/api/repository_submodules.html), [PlantUML](https://docs.gitlab.com/ee/administration/integration/plantuml.html) | By providing a way to map and connect the structure of each component in codebase as a whole, SCM solutions make concurrent work easier, onboarding faster and help define goals in the shape of deliverable units.

## How GitLab Meets the Market Requirements

A collection of short demonstrations that show GitLab's SCM capabilities.
* Protect and secure assets
   * [Control changes to product development assets](https://youtu.be/l6K3Xn2MPJw)
   * [Manage, track and maintain access](https://youtu.be/nRxCz4vMv5Q)
* Enterprise ready
* Supports numerous assets
   * [Design Management](https://www.youtube.com/watch?v=f9zmxOfP7AQ)
* [Foster collaboration](https://youtu.be/OFNUjvgm2_4)
* Secure Development
* System design and architecture browsing


## Top 3 Differentiators

| Differentiator | Value | Proof Point  |
|-----------------|-------------|---------------|
| **Distributed version control** | It allows for asynch, remote, collaborative work to flourish since a single copy of the complete project's history can be stored in any machine, branching is easy and powerful so almost endless workflow possibilities open in opposition to centralized VCS like Perforce or CVS. All the information different teams produce while collaborating on source code and other digital assets in GitLab can be easily analyzed, authorized and streamlined from the Merge Request with clockwork precision. This, in turn, allows for team leads to correctly implement best practice workflows like [GitLab Flow](https://docs.gitlab.com/ee/topics/gitlab_flow.html) | –  Stackoverflow's 2018 survey says [87% of respondents use Git](https://insights.stackoverflow.com/survey/2018#work-_-version-control) (jump from 69% in 2015) as opposed to other centralized and distributed VCSs. Similar trend is captures in the [Open Hub data](https://softwareengineering.stackexchange.com/questions/136079/are-there-any-statistics-that-show-the-popularity-of-git-versus-svn/136207#136207). In 2019 they [didn't even ask the question](https://insights.stackoverflow.com/survey/2019#development-practices) in the same survey.  –  [Gartner's Market Guide for Software Change and Configuration Management](https://www.gartner.com/en/documents/3118917/market-guide-for-software-change-and-configuration-manag) from 2015 lays out clearly the advantages of DVCS. In 2019 Gartner assess SCM as part of Application Release Orchestration of which [GitLab is a challenger as of 2019](/analysts/gartner-aro19/) – [Google trends since 2004](https://trends.google.com/trends/explore?date=all&q=git,svn,perforce,mercurial,tfs) compared to other DVCS and CVCSs.
| **Single Application** | The ability to connect every phase of the Software Development Lifecycle in one single DevOps platform. One data layer, one design system, one set of commands to manage all the different stages of software design, development, build and deployment | [General proof points](/handbook/sales/command-of-the-message/proof-points.html) of the single app  |
| **Product Development Management** | GitLab is the only product that increasingly provides collaboration functionality to Product teams that work not only with source code but also and IP, graphic assets, animations and binaries to mention a few.  | Forrester's [Adopt Product Management to Connect Design and Development](https://www.forrester.com/report/Adopt+Product+Management+To+Connect+Design+And+Development/-/E-RES149995) clearly states that "Siloed Design And Dev Teams Deliver Subpar Software" |

---
## [Message House](./message-house/)

A message house contains all the pieces of content that will clearly convey GitLab's values and differentiators in each touch point with prospects and clients. Each touchpoint should be worth their while and should also be relevant in their relationship with GitLab in order to move the needle in their decision-making process. In other words, every single item of the message house is in context with each persona, connect with their intents, resonates with their inflection points (value drivers) and provides an easy narrative of the SCM use case to lay out next steps and make progress.

The message house for the use case provides a structure to describe and discuss the value and differentiators for the use case.

## Customer Facing Slides
<figure class="video_container">
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRHa6VC1L15UsUPpFHU3Ob4QOyKZJw9WT2AmAx8ttbHmi_AMFVEDWEvXcNsMsQ9jwmY3PRxWAUDsqqO/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
</figure>

### Discovery Questions
tbd

## Competitive Comparison
Amongst the many competitors in the DevOps space, [GitHub, Perforce, Azure DevOps, SVN](/direction/create/source_code_management/#competitive-landscape) are the closest competitors offering SCM capabilities. [Phabricator, Gerrit, GitHub, BitBucket, Azure DevOps, Crucible, Review Board, Reviewable, CodeStream, GitLens, VS Live Share and Gitpod](direction/create/code_review/#competitive-landscape) are the closest offering Code Review capabilities. [Cloud9, Codesandbox, Repl.it, Koding, StackBlitz, Theia, Gitpod, Coder, VS Online](direction/create/web_ide/#competitive-landscape) are the closest competitors offering a webIDE. [Psatebin, Blocks, Gist.io, Bitbucket Snippets, Codesandbox, JSBin, JSFiddle, Codepen](direction/create/snippets/#competitive-landscape) are the closest offering snippets. [Invision, Figma, UX Pin](direction/create/design_management/#competitive-landscape) are the closest competitors offering Design Management capabilities. 

### Industry Analyst Relations (IAR) Plan
- The IAR Handbook page has been updated to reflect our plans for [incorporating Use Cases into our analyst conversations](https://about.gitlab.com/handbook/marketing/product-marketing/analyst-relations/#how-we-incorporate-use-cases-into-our-analyst-conversations).
- For  details specific to each use case, and in respect of our contractual confidentiality agreements with Industry Analyst firms, our engagement plans are available to GitLab team members in the following protected document: [IAR Use Case Profile and Engagement Plan](https://docs.google.com/spreadsheets/d/14UthNcgQNlnNfTUGJadHQRNZ-IrAe6T7_o9zXnbu_bk/edit#gid=1430251677).

For a list of analysts with a current understanding of GitLab's capabilities for this use case, please reach out to Analyst Relations via Slack (#analyst-relations) or by submitting an [issue](https://gitlab.com/gitlab-com/marketing/product-marketing/issues/new) and selecting the "AR-Analyst-Validation" template.   

## Proof Points - Customer Recognitions

[General proof points](/handbook/sales/command-of-the-message/proof-points.html) and [Customer Recognition](/customers/marketplace/)

### Quotes and reviews

#### Gartner Peer Insights

*Gartner Peer Insights reviews constitute the subjective opinions of individual end users based on their own experiences and do not represent the views of Gartner or its affiliates. Obvious typos have been amended.*

>"The software is intuitive and quite easy to use. Since many software development projects require more than one person, this makes it easy to create teams and collaborate."
>
> - **Quality Engineer**, [Gartner Peer Review](https://www.gartner.com/reviews/market/application-release-orchestration-solutions/vendor/gitlab/product/gitlab/review/view/1037713)
>
>"Improves productivity of engineers by providing easy and fast ways to keep feature branches and merge them quickly and efficiently."
>
> - **Engineering Manager**, [Gartner Peer Review](https://www.gartner.com/reviews/market/application-release-orchestration-solutions/vendor/gitlab/product/gitlab/review/view/1060524)
>
>"Keeps your software projects under control. Rogue developers are kept at bay via enforced review processes and pipelines."
>
> - **Engineer**, [Gartner Peer Review](https://www.gartner.com/reviews/market/application-release-orchestration-solutions/vendor/gitlab/product/gitlab/review/view/1063180)
>
>"For managing git repositories it is the best product available right now in the market."
>
> - **Sr. Software Engineer**, [Gartner Peer Review](https://www.gartner.com/reviews/market/application-release-orchestration-solutions/vendor/gitlab/product/gitlab/review/view/1074452)
>
>"This has really aided in our ability to automate software delivery and return wasted overhead back to the pool of resources! This is a very simple to use and fast delivery tool to assist your code pipeline."
>
> - **Project Manager**, [Gartner Peer Review](https://www.gartner.com/reviews/market/application-release-orchestration-solutions/vendor/gitlab/product/gitlab/review/view/1078302)
>
>"We use this platform in our company to version our source [code] ensure they are up to date and as a backup option. It enables us build scalable and high quality products. Ease of use and compatible with most development environments."
>
> - **Sr. Software Developer**, [Gartner Peer Review](https://www.gartner.com/reviews/market/application-release-orchestration-solutions/vendor/gitlab/product/gitlab/review/view/1135664)
>
>"I appreciate its ability to run limitless. It has various features like issue tracker, protected branches and merge requests, which gives very nice experience."
>
> - **Sr. Software Engineer**, [Gartner Peer Review](https://www.gartner.com/reviews/market/application-release-orchestration-solutions/vendor/gitlab/product/gitlab/review/view/1142879)
>
>"GitLab is a very useful SCM. In our [organization] we have used it as a source code repository. We have extensively used branching and tags creation feature. As we work in a sprints, we have several sprint and feature branches."
>
> - **Lead Developer**, [Gartner Peer Review](https://www.gartner.com/reviews/market/application-release-orchestration-solutions/vendor/gitlab/product/gitlab/review/view/1144638)
>
>"GitLab is a superb version control and collaboration [provider]."
>
> - **Systems Engineer**, [Gartner Peer Review](https://www.gartner.com/reviews/market/application-release-orchestration-solutions/vendor/gitlab/product/gitlab/review/view/1194415)
>
>"Before GitLab, we used to make local copies of code or backup the code and then pass on the code through the server. But if our organization knew about GitLab from start, we would have immediately integrated with our development practises for ease of deployment."
>
> - **Software Developer**, [Gartner Peer Review](https://www.gartner.com/reviews/market/enterprise-agile-planning-tools/vendor/gitlab/product/gitlab/review/view/1016152)

### Case Studies

**[ESA (European Space Agency)](/customers/european-space-agency/)**
* **Problem**  Geographic separation led to software deployment that used to take weeks.
* **Solution** GitLab Core (SCM,CI)  allow opportunities for collaboration, synergies and multiple exploitations of efforts in visible way.
* **Result** More than 140 groups adopted GitLab and more than 1500 software projects have been created. These range from mission control systems, onboard software for spacecraft, image processing and monitoring tools for Labs.
* **Sales Segment:** Enterprise

**[Goldman Sachs](/customers/goldman-sachs/)**  
* **Problem** Needed to increase developer efficiency and software quality
* **Solution:** GitLab Premium (CI/CD, SCM)
* **Result:** Improved from **1 build every two weeks to over a 1000/day**, or releasing 6 times per day per developer, and an average cycle time from branch to merge is now 30 minutes; simplified workflow and simplified administration
All the new strategic pieces of ’software development platforms are tied into GitLab. GitLab is used as a complete ecosystem for development, source code control and reviews, builds, testing, QA, and production deployments.
* **Sales Segment:** Enterprise


**[Worldline](/customers/worldline/)**
* **Problem** Worldline faced bottlenecks and lack of ownership when it was using Subversion. It took 1-2 weeks to get a source code repo.
* **Solution** GitLab Core (SCM)
* **Result:** With GitLab Core it now takes a few seconds. **Within six months, over 1,000 users were active users because GitLab is so easy to use.** People excited to contribute code reviews with GitLab Merge Requests. Previous code review tools had 10-20 developers using them, while Worldline currently has 3,000 active users of GitLab - an adoption rate increase of 12,000 percent.
* **Sales Segment** Enterprise

**NorthWestern Mutual [Commit San Francisco 2020: Why we chose GitLab as our Enterprise SCM](https://youtu.be/kPNMyxKRRoM)** [Deck](https://docs.google.com/presentation/d/1vxiFJShRjU98kvwK87PZnYnMzBVPT5VJTr1chUEiM_Y/edit#slide=id.g6d6088e69c_0_15)
* **Problem**  Code base was fragmented and dev permissions were also complex to handle. This prevented devs from collaborating, deploying faster and fixing bugs and security holes.  
* **Solution** GitLab Premium (SCM, CI)
* **Result**  A full migration to their Enterprise environment was completed in 8 months. After implementation they managed to reduce friction with the ease of use of GitLab's CI.
* **Sales Segment:** Enterprise


### References to help you close
[Links to Salesforce References](https://gitlab.my.salesforce.com/a6l4M000000kDwu) Note: Sales team members should have access to this report. If you do not have access, reach out to the [customer reference team](https://about.gitlab.com/handbook/marketing/product-marketing/customer-reference-program/#which-customer-reference-team-member-should-i-contact) for assistance.

## Adoption Guide

The following section provides resources to help TAMs lead capabilities adoption, but can also be used for prospects or customers interested in adopting GitLab stages and categories.

### Playbook Steps

- To be added...

### Adoption Recommendation

This table shows the recommended use cases to adopt, links to product documentation, the respective subscription tier for the use case, and telemetry metrics.

| Feature / Use Case                                           | F/C  | Basic | S/P  | G/U  | Notes                                                        | Telemetry |
| ------------------------------------------------------------ | :--: | :---: | :--: | :--: | :----------------------------------------------------------- | :-------: |
| Adopt GitLab Flow                                            |  X   |   X   |  X   |  X   |                                                              |           |
| Native Highly Available / Performance Git Storage Support    |  X   |   X   |  X   |  X   | Technical Support for Gitaly Cluster only availabe for Premium/Utlimate |           |
| Simplify Repository Management                               |      |   X   |  X   |  X   | CodeOwners file, push rules                                  |           |
| Merge Request Approval Workflow                              |      |   X   |  X   |  X   | MergeApproval                                                |           |
| Templates for efficient workflows                            |      |       |  X   |  X   | Project templates, Group/Instance templates for gitlab-ci.yml etc |           |
| Commit Protection                                            |      |       |  X   |  X   | Reject Unsigned Commits, Verified Committer                  |           |
| Large distributed team                                       |      |       |  X   |  X   | GitLab Geo                                                   |           |
| Repository Protection                                        |      |       |      |  X   | IP Allowlist/Denylist                                              |           |

The table includes free/community and paid tiers associated with GitLab's self-managed and cloud offering.

- F/C = Free / Core
- Basic = Bronze/Starter 
- S/P = Silver / Premium
- G/U = Gold / Ultimate

### Additional Documentation Links

- [Create Stage Documentation](https://docs.gitlab.com/ee/#create)
- [GitLab Cluster](https://docs.gitlab.com/ee/administration/gitaly/praefect.html#gitaly-cluster)
- [GitLab Geo](https://docs.gitlab.com/charts/advanced/geo/#gitlab-geo)

### Enablement and Training

The following will link to enablement and training videos and content.

- [Customer Use Case Sales Webinar](https://www.youtube.com/watch?v=GN891Bqc6QY) 
- [Value of GitLab SCM vs. GitHub]( https://www.youtube.com/watch?v=kNlaCq7qxzM)

### Professional Service Offers

GitLab offers a [variety of pre-packaged and custom services](https://about.gitlab.com/services/) for our customers and partners. The following are service offers specific to this solution. For additional services, see the [full service catalog](https://about.gitlab.com/services/catalog/).

- [Git and Gitlab Basics training](https://about.gitlab.com/services/education/gitlab-basics/) - Training for team members new to Git SCM and the gitlab application
- [Gitlab training for Project Managers](https://about.gitlab.com/services/education/gitlab-basics/) - Training to teach Project Managers how to use gitlab issues and how that relates to the create stage in the development lifecycle 
- [DevOps Fundamentals Training](https://about.gitlab.com/services/education/devops-fundamentals/) - Training to teach Project Managers how to use gitlab issues and how that relates to the create stage in the development lifecycle 
- [SCM Migration Services](https://about.gitlab.com/services/migration/) - Data and user migration from a previous software Version Control System to GitLab self-managed or cloud-delivered solutions



## Key Value (at tiers)

### Premium/Silver
- Describe the value proposition of why Premium/Silver for this Use Case

### Ultimate/Gold
- Describe the value proposition of why Ultimate/Gold for this Use Case

## Resources
### Presentations
* WIP [Customer deck coming]

### Version Control and Collaboration Videos
* [Source Code Walk Through, January 2020](https://www.youtube.com/watch?v=wTQ3aXJswtM) by James Ramsey, Group Product Manager for the Create Stage
* [GitLab Flow pattern](https://youtu.be/InKNIvky2KE?list=WL)
* [Design Management Walkthrough, January 2020](https://youtu.be/LzFRBMGl2SA) by Christen Dybenko, PM Knowledge Group
* [Web IDE walkthrough](https://youtu.be/6VI_SqkcQIQ?t=366) by William Chia sPMM CI/CD
* [Web IDE walkthrough, January 2020](https://www.youtube.com/watch?v=oDZu71nWctc&list=PL05JrBw4t0Kp0LPy37-rcLf9KYppouxPR&index=11&t=2s) by Kai Armstrong, PM Editor Group
* [Merge Request and Source Control as part of the Software Development Life Cycle](https://youtu.be/UuX-GnYWNwo?t=274) by William Chia sPMM CI/CD
* [GitLab Namespaces: users, groups and subgroups](https://www.youtube.com/watch?v=r0sJgjR2f5A)	by Brendan O'Leary, Tech Evangelist
* [Rich Change Controls for Building Workflows you can Trust](https://youtu.be/uW95PV8d-w8?t=186) by Darwin Sanoy, Solutions Architect

### Git Training
* [Git in Gifs](https://www.youtube.com/playlist?list=PLFGfElNsQthZcx-NEyMsPl-dl3Q_p-3yv)
* [Why You Should Move To Git](https://www.youtube.com/watch?v=iVUqKJpHc5s)

### Integrations Demo Videos
* [Jira & Jenkins Integration Video](https://www.youtube.com/embed/Jn-_fyra7xQ)
* [How to setup the Jira Integration](https://www.youtube.com/watch?v=p56zrZtrhQE)
* [GitHub Integration Video](https://www.youtube.com/embed/qgl3F2j-1cI)

### Clickthrough & Live Demos
* [All Marketing Click Through Demos](/handbook/marketing/product-marketing/demo/#click-throughs)
* [All Marketing Live Demos](/handbook/marketing/product-marketing/demo/#live-instructions)

### Talks and customer References
* [GitLab All the Way Down - A small startup's tale of growing with GitLab](https://youtu.be/t0Eh1sq9r5s?t=221)
* [GitLab Permissions as Code (Controlling permission to access repos)](https://youtu.be/W1YMBc6kwUE?t=74)
* [What not to do while using GitLab](https://youtu.be/Qc8caRTcSa4?t=221)
* [Why we chose GitLab as our Enterprise SCM (Northwestern Mutual)](https://www.youtube.com/watch?v=kPNMyxKRRoM)
