---
layout: markdown_page
title: "Stages of remote work"
twitter_image: "/images/opengraph/all-remote.jpg"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

On this page, we're detailing the various stages of remote work. This should be viewed as a sliding scale.

For companies transitioning into a remote environment, visit GitLab's guide to understanding the [phases of remote adaptation](/company/culture/all-remote/phases-of-remote-adaptation/).

## No remote

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/_HhlqwJsNyM" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

*In the [GitLab Unfiltered](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A) video above, Darren (GitLab) and Anna-Karin (Because Mondays) discuss a number of challenges and solutions related to remote work, transitioning a company to remote, working asynchronously, and defaulting to documentation.*

Some enterprises do not allow for any remote work. This could be due to a leadership mandate, or could be attributed to the nature of the business. For example, medical care, broadcasting from live events, and certain forms of manufacturing do not lend themselves to being successfully completed while remote. 

It is worth noting, however, that even businesses which have historically been impossible to complete remotely are seeing new opportunity arise thanks to technological advancements. 

The da Vinci Surgical System, for instance, has been [used in telesurgery](http://www.bbc.com/future/story/20140516-i-operate-on-people-400km-away), and communications infrastructure is robust enough in some locales to support [remote broadcasting](https://ftw.usatoday.com/2015/06/espn-broadcasts-remote-integration-save-millions). 

Multinational corporations with multiple offices across the globe are inherently remote, even if they do not allow remote work. An employee in one office is inherently remote to another employee in another office, and a refusal to recognize this reality can create dysfunction when attempting to collaborate across offices. 

## Remote-allowed

Also referenced as remote-tolerated, this stage of remote allows all approved employees in a company to work *some* (but not *all*) days from home, or a place of their choosing. 

This is commonly seen in agency and corporate environments where "remote Fridays" are sold as a perk to employment. In such scenarios, it is clear that leadership is not piloting remote work as a means to judge the feasability of all-remote, but rather compromising with employee demands for greater flexibility. 

Such employers are tolerant of some amount of working outside of the office, but still expect an individual to spend the bulk of their time in the office.

## Hybrid-remote

There are many flavors of [hybrid-remote](/company/culture/all-remote/part-remote/). The key attribute of a hybrid-remote arrangement is that *some* employees — but not all — are allowed to work remotely 100% of the time. These organizations have at least one physical office where most of the company physically commutes to each working day. 

While such organizations work well for some employees, it's important to note that many hybrid-remote companies are not comfortable being publicly labeled as such. Their job vacanies are oftentimes not listed as remote-friendly. Should you apply for such a role, be prepared to lobby for assuming the role in a remote environment every step of the way.

## Remote, biased towards one time zone

Certain companies allow employees to work remotely, but maintain "core team hours." [InVision Studio](https://www.invisionapp.com/inside-design/studio-remote-design-team/), for example, has members spread across multiple countries and time zones, but aims to achieve "at least a 4-hour overlap with InVision’s core team hours, 10am–6pm Eastern Standard Time." 

This tends to attract employees who are in relative close proximity to one another, or at the very least, in a nearby time zone even if located in a different hemisphere. 

## All-remote, asynchronous across time zones

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/jdN5mj5ieLk?start=2115" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

As part of a Harvard Business School case study [interview](https://youtu.be/jdN5mj5ieLk) (shown above), GitLab co-founder and CEO Sid Sijbrandij spoke with Professor Prithwiraj Choudhury on the various stages of remote work.

> We think this model will be called all-remote. 
>
> We think it's time for clear differentiation between companies which *allow* remote, and companies that will not allow you to come into an office.
>
> People will start understanding the difference. If there is no headquarters, you're not at a satellite office. — *GitLab co-founder and CEO Sid Sijbrandij*

GitLab is a [100% remote company](/company/culture/all-remote/), where each individual works remotely and there are no company-owned offices. With [team members](/company/team/) in over 65 countries, many time zones are accounted for. 

In such companies, there is no effort placed on trying to coordinate team members to a given time zone. Rather, a bias towards [asynchronous communication](/company/culture/all-remote/asynchronous/) encourages documentation, discourages syncronous meetings as a default for collaboration, and provides greater flexibility for each member to determine the working hours that best suites their [lifestyle](/company/culture/all-remote/people/). 

This goes beyond enabling a work from home arrangement; all-remote creates a work from *anywhere* arrangement. 

### What "all-remote" does not mean

Let's address some of the common misconceptions about all-remote work.

First things first: An all-remote company means there is *no* office where multiple people are based. The only way to not have people in a satellite office is not to have a main office. It's not that we don't have a headquarters, it is that we have 1,100+ (and growing) headquarters!

The terms "remote" and "distributed" are often used interchangeably, but they're not quite the same. We prefer the term "remote" because "distributed" suggests multiple physical offices. "Remote" is also the [most common term](https://www.google.com/search?ei=4IBsXKnLDIGRggftuqfAAQ&q=distributed+companies&oq=distributed+companies&gs_l=psy-ab.12...0.0..5177...0.0..0.0.0.......0......gws-wiz.6xnu76aJWr4) to refer to the absence of a physical workspace, and being able to do your job from anywhere.

For employees, being part of an all-remote company does not mean working independently or being isolated, because it's not a substitute for human interaction.

Technology allows us to [stay closely in touch](/company/culture/all-remote/informal-communication/) with our teams, whether asychronously in text or in real time with high-fidelity conversations through video. Teams should collaborate closely, [communicate](/company/culture/all-remote/informal-communication/) often, [build relationships virtually](/blog/2019/07/31/pyb-all-remote-mark-frein/), and feel like valuable members of a larger team.

Working remotely also doesn't mean you're physically constrained to home. You're free to work [wherever you want](/company/culture/all-remote/people/#travelers). That could be at home with family, a coffee shop, a coworking space, or your local library while your little one is enjoying storytime. It could mean that you're [location independent](/company/culture/all-remote/people/#nomads), traveling around and working in a new place each week. You can have frequent video chats or virtual pairing sessions with coworkers throughout the day, and you can even meet up with other coworkers to work together in person if you're located near each other.

At the organizational level, "all-remote" does not mean simply offshoring work. Instead, it means you're able to [hire the best talent from all around the world](/company/culture/all-remote/hiring/). It's also not a management paradigm. You still have a hierarchical organization, but with a [focus on output instead of input](/company/culture/all-remote/management/).

All in all, remote is fundamentally about *freedom* and *individual choice*. At GitLab, we [value your results](/handbook/values/#results), regardless of where you get your work done.

## GitLab Knowledge Assessment: Stages of Remote Work 

Anyone can test their knowledge on the Stages of Remote Work by completing the [knowledge assessment](https://docs.google.com/forms/d/e/1FAIpQLSdVT7qsBLQmx5KE5D-oZ80Byv-H9scDirYWYZZsud46N7Kz1g/viewform). Earn at least an 80% or higher on the assessment to receive a passing score. Once the quiz has been passed, you will receive an email acknowledging the completion from GitLab. We are in the process of designing a GitLab Remote Certification and completion of the assessment will be one requirement in obtaining the [certification](/handbook/people-group/learning-and-development/certifications). If you have questions, please reach out to our [Learning & Development](/handbook/people-group/learning-and-development) team at `learning@gitlab.com`.

## Is this advice any good?

![GitLab all-remote team illustration](/images/all-remote/gitlab-com-all-remote-1280x270.png){: .shadow.medium.center}

GitLab is the world's largest all-remote company. We are 100% remote, with no company-owned offices *anywhere* on the planet. We have over 1,200 team members in more than 65 countries. The primary contributor to this article ([Darren Murph](/handbook/marketing/readmes/dmurph/), GitLab's Head of Remote) has over 14 years of experience working in and reporting on colocated companies, [hybrid-remote](/company/culture/all-remote/hybrid-remote/) companies, and all-remote companies of various scale. 

Just as it is valid to [ask if GitLab's product is any good](/is-it-any-good/), we want to be transparent about our expertise in the field of remote work. 

## Contribute your lessons

GitLab believes that all-remote is the [future of work](/company/culture/all-remote/vision/), and remote companies have a shared responsibility to show the way for other organizations who are embracing it. If you or your company has an experience that would benefit the greater world, consider creating a [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/) and adding a contribution to this page.

----

Return to the main [all-remote page](/company/culture/all-remote/).
