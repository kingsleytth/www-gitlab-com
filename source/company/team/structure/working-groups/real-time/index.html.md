---
layout: markdown_page
title: "Real-Time Working Group"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property        | Value           |
|-----------------|-----------------|
| Date Created    | March 12, 2020 |
| Target End Date | May 31, 2020 |
| Slack           | [#wg_real-time](https://app.slack.com/client/T02592416/CUX9Z2N66) (only accessible from within the company) |
| Google Doc      | [Real-Time Working Group Agenda](https://docs.google.com/document/d/1eqwiGKifpnE4XTog0dB4Lgb-Bx0cc8g8OejmWDoZabs/edit#) (only accessible from within the company) |
| Epic & Design Doc | [Use ActionCable for real-time features](https://gitlab.com/groups/gitlab-org/-/epics/3056)             |
| Feature Issue   | [View real-time updates of assignee in issue / merge request sidebar](https://gitlab.com/gitlab-org/gitlab/-/issues/17589) |
| Associated OKRs | [Plan: Support incremental ACV](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6594) |

## Business Goal

To ship one real-time feature to self-hosted customers.

### Exit Criteria

 (✅ Done, ✏️ In-progress)    

##### [One Working Real-Time Feature, Usable by Self-Hosting Customers](https://gitlab.com/gitlab-org/gitlab/issues/17589) `=> 71.4%`

- Omnibus includes ability to start ActionCable Puma Server with config/cable.yml ✅
- GDK allows configuration of ActionCable and starts Puma server ✅
- Workhorse Proxies ActionCable requests ✅
- Backend work is complete to upgrade websocket connections and push signal when assignees are updated on an issue ✅
- Frontend work is complete to respond to WebSockets with an update to the sidebar ✅
- [Establish reference architectures for supporting WebSocket connections at scale](https://gitlab.com/gitlab-org/quality/performance/-/issues/256#note_348137517)
- Feature flag defaulted to on, with suitable fallback

##### [Working, Real-Time Feature Available on .com](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/228) `=> 29%`

- [Deployment to a non-production environment for manual, full-stack testing](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/878)
- [Containerization of Real-Time Feature including ActionCable and Puma](https://gitlab.com/gitlab-org/gitlab/-/issues/213861) ✅
- [Update Helm charts to allow use of multiple Redis instances](https://gitlab.com/gitlab-org/charts/gitlab/-/merge_requests/1287) ✅
- Update of Helm charts to allow deployment of new actioncable image
- [Use of Prometheus Exporter for observability](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/228#note_335844131)
- [Prepared for deployment via Kubernetes](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/228)
- Deployed to production for testing on small, internal project behind Feature Flag on production


## Roles and Responsibilities

| Working Group Role    | Person                | Title                          |
|-----------------------|-----------------------|--------------------------------|
| Executive Sponsor     | Christopher Lefelhocz | Senior Director of Engineering |
| Facilitator           | John Hope             | Engineering Manager, Plan      |
| Functional Lead       | Heinrich Lee Yu       | Senior Backend Engineer, Plan  |
| Functional Lead       | Gabe Weaver           | Senior Product Manager, Plan   |
| Functional Lead       | Sean McGivern         | Staff Backend Engineer, Scalability   |
| Member                | Markus Koller         | Backend Engineer, Create       |
| Member                | Scott Stern           | Frontend Engineer, Plan        |
| Member                | Ben Kochie            | Site Reliability Engineer      |
| Member                | Natalia Tepluhina     | Senior Frontend Engineer, Create |
| Member                | Matthias Käppler      | Senior Engineer, Memory        |
| | | |

## Meetings

Meetings are recorded and publicly available on
YouTube in the [Real-Time Working Group playlist][youtube].

<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PL05JrBw4t0KoMOc_LID1fKWWR4H_2n2hQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

[youtube]: https://www.youtube.com/playlist?list=PL05JrBw4t0KoMOc_LID1fKWWR4H_2n2hQ
