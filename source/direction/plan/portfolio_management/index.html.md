---
layout: markdown_page
title: "Product Section Direction - Portfolio Management"
---

- TOC
{:toc}

Last Reviewed: 2020-04-23

## Overview
<!-- Provide a general overview of the section and what is covered within it. Include details on our current market share (if available), the total addressable market (TAM), our competitive position, and high-level feedback from customers on current features. -->

Portfolio Management is key to driving strategic alignment across your organization, ensuring teams have the right information, and are empowered to execute efficiently. As companies scale, the management of project and product effort across multiple teams become increasingly difficult and vastly more critical to overall business success. The demand for lean and powerful planning solutions is only growing as more organizations move to [adopt product-centric delivery models](https://www.gartner.com/en/newsroom/press-releases/2019-02-19-gartner-survey-finds-85-percent-of-organizations-favor-a-product). This makes balancing strategic planning against continuous delivery all the more challenging. [By 2023 75% of product managers are expected to leverage portfolio management and rodadmaping solutions](https://www.gartner.com/document/3894127) to report and communicate with stakeholders and business leaders, we believe this is a critical area of opportunity for us to serve our customers and provide the necessary functions to help them build better software.   

As an end to end DevOps platform, GitLab is uniquely positioned to deliver a portfolio management solution that enables business leaders to drive their vision and development teams to quickly deliver value while improving how they work iteration by iteration. Our unification of the DevOps process allows us to interlink data across every stage of development; from initial anlysis, to planning, implementation, deployment, and monitoring. This shared environment allows us to directly relate individual contributions and merge requests to larger company goals and directives. Leveraging these relations, GitLab aspires to provide a Portfolio Management solution that bridges the gap between individual contributors and the executive suite. We aim to surface the right information at the right level, empowering your business to react quickly without multiple integrations and licenses across a suite of bloated solutions. 

For enterprises and growing SMBs, the complexities and difficulties of managing multiple teams and projects across your product portfolio are monumental. Not only do organizations need access to detailed information on individual team progress against, they need to aggregate it up to consumable views for leaders to review and make informed decisions against. 

**Across the PPM space we have seen consistent challenges surface as organizations attempt to develop continuous delivery methods while maintaining longer-term planning goals:**

1. The strategy is set and communicated by the business leaders, but becomes diluted as information is passed to individual teams. Moreover, individual teams lack a line of sight to understand how their contributions help execute against the bigger picture. 
1. Continuos delivery frameworks primarily center on short-term cycles rather than strategic goals and thus are often at odds with the strategic horizons that portfolio management attempts to provide.
1. Roadmaps are either too busy or too simple to deliver the right story to different audiences, resulting in miscommunication and confusion on product direction and progress
1. Managing program capacity and investment is difficult due to fragmented or non-existent data on estimated vs actual capacity and velocity
1. Project Risks and critical dependency paths are hard to identify and mitigate quickly 

This creates an environment where the development teams aren’t clear on why the work they are doing matters, and the business has difficulty tracking or predicting progress towards their overall strategy.

## Analyst landscape

We are continually working with analysts to better understand the space of Portfolio Management as it relates to managing and roadmapping multiple programs and projects. Agile methodologies have been around for a good number of years, but they have been focused on small teams, or maybe a small number of teams working closely in concert to execute and deliver features. The industry is now focused on how to take these processes that have been proven in small teams, and scale them to large enterprises with business initiatives that span potentially even multiple departments. What is crucial therefore, is how can organizations deliver business value very quickly, but still allow stakeholders (especially executive stakeholders) the visibility to track progress and be assured that the enterprise is working on the right initiatives in the first place. This has even led to the popular [SAFe (Scaled Agile Framework) methodology](https://www.scaledagileframework.com) as a way to help guide enterprise organizations in their Agile transformations.

At GitLab, we embrace this enterprise [Agile transformation](https://about.gitlab.com/solutions/agile-delivery/) that companies are going through and intened to lead the way by building [empowering solutions](https://about.gitlab.com/blog/2018/03/05/gitlab-for-agile-software-development/). In particular, having a single application to view your entire work breakdown structure, from executive level, all the way down to issues and merge requests and even commits, means that our design truly allows for streamlining that visibility. We are focused on building this work breakdown structure out as the initial goal in Agile Portfolio Management.

## Challenges
<!-- Optional section. What are our constraints? (team size, product maturity, lack of brand, GTM challenges, etc). What are our market/competitive challenges? -->

There are a set of challenges and constraints our current Portfolio Management group is experiencing and are working to mitigate:

1. We currently are sharing a development team with Plan : Certify. While we advocate for additional team members we are ruthlessly prioritizing what features and improvements we work on and attempting to improve internal processes and workflows to be as efficient as we can.
1. Many customers and prospects are currently using a larger or more established PPM suite of tools. As we expand our own PPM functionality we are hyper-focused on discovering and building the essential features that customers need to manage their product portfolios, leveraging GitLab's unique positioning of a unified DevOps platform, while avoiding cruft or bloated features that over-complicate the process
1. Our current Portfolio Management categories, Epics and Roadmaps, are early in their Maturity. A major focus of our 1-year plan is to properly invest and mature these categories to provide the right value for our customers. 

## 1 Year Plan
<!-- Describe key themes, projects, and/or features planned over the next year. Also highlight what we will not be doing in the next year -->

To help customers overcome the above challenges, we are focusing on 3 critical themes over the next year, building a strong foundation within the Plan Stage that we can iterate on in the years to come.  

### Advanced Planning Artifacts

To support higher-level planning of work at scale we will be heavily investing in enhancing our planning artifacts (Epics, Issues, and Milestones) to enable granular planning and sequencing of work. This includes improving the creation process of each of these artifacts, providing linking options for related items together through dependency mappings, and rolling up aggregated data like weights, time tracking, and health/risk status to epics and global reports.  

### Robust Roadmapping 

We will advance our current offering to provide easy to manage roadmaps that help facilitate the communication of planned work and its progress over time. Advancements like: multiple roadmaps, epic progress indication, the visualization of various milestones, and critical dependency paths will empower users to craft the right visual mapping for the right audience, displaying high level or program epics to business leaders who need to see the big picture, to more granular team or product roadmaps showing details down to the issue level. 

### Mapping Strategy to Effort

For our scaling and enterprise customers, we will enable the mapping of strategic initiatives and goals within GitLab that programs and teams can organize their work under, connecting vision to the team efforts across the organization. As business leaders, quickly see where your investments are being committed and how work is progressing. As an individual contributor, see exactly why the work you are tackling is important and what business metric it is intended to affect. 

### What we're not doing

For the next year, we are focusing on the above three themes to ensure we have a solid planning suite to build more advanced and valuable solutions on in the future. As such there are areas we will not be focusing development on, but may continue to research and validate:

**Financial Planning**
We believe to properly provide valuable financial modeling and management in our Portfolio Management suite, we need to first work with the other Plan Groups of Project Management and Certify to solve capacity and velocity management. Once we have a better solution in place for predicting and tracking effort across individual teams we will work to build financial mapping and modeling that leverages that data. 

**Predictive Planning**
There is a major opportunity to empower our customers with predictive planning functions like suggested delivery dates, automated risk scoring of work in flight, or recommended resourcing changes that would enable faster and more efficient portfolio planning. We need to focus on our core offerings in Plan before we can invest in a predictive layer of functions.

**Focusing on Mobile Experience**
With the current state of the Portfolio Management group, we will not be able to focus on any mobile optimization in the near term as we need all bandwidth dedicated to driving increased product maturity and net new functionality.

## 3 Year Strategy
<!-- Where will the product be in 3 years? How will the customer's life/workflow be different in 3 years as a result of our product? -->

In three Years Portfolio Management aims to provide an advanced set of functionality that enables businesses to manage their Product Development Portfolio with deep cross-cutting Roadmapping, advanced Value Stream Mapping and Analytics, and intelligence-driven Capacity and Release Management.

### Value Stream Mapping and Analytics:
Gain insight and make data-driven decisions with deep Value Stream analytics rolled up from all areas of your Portfolio. See aggregate and granular data on team or program performance and compare estimated vs actual planning metrics to better inform your upcoming strategy and investments.

### Intelligent Driven Capacity and Release Management:
Leverage in house intelligence to predict capacity, team output, and provide release date recommendations based on predicted effort, available capacity, and historical trends

### What-If Scenarios for Resource Management:
Run advanced theoretical scenarios on resource allocation and capacity changes to help you decide where to make investments.

## Portfolio Management Categories
<!-- Provide brief descriptions of stage + category direction, along with links to supporting direction pages -->

Large enterprises are continually working on increasingly more complex and larger scope initiatives that cut across multiple teams and even departments, spanning months, quarters, and even years. GitLab's vision is to support organizing these initiatives into powerful **multi-level work breakdown** plans and to enable **tracking the execution** of them over time, to be extremely simple and insightful.

### Epics
[Epics Direction](/direction/plan/epics/)
 
Epics allow you to manage your portfolio of projects more efficiently and with less effort by tracking groups of issues that share a theme, across projects and milestones. Nest multiple child epics under a parent epic to create deeper work structures that enable more flexible and granular planning.

### What's Next for Epics

1. [Change Issue Paretn via Drag and Drop in Epic Tree](https://gitlab.com/gitlab-org/gitlab/-/issues/33039)
1. [Confidential Epics](https://gitlab.com/gitlab-org/gitlab/-/issues/197339)
1. [Create Epic Page](https://gitlab.com/gitlab-org/gitlab/-/issues/10966)
1. [Epic Swimlanes on Boards](https://gitlab.com/gitlab-org/gitlab/-/issues/7371)
1. [Program/Epic Level Boards](https://gitlab.com/groups/gitlab-org/-/epics/2864)

### Roadmaps
[Roadmaps Direction](/direction/plan/roadmaps/)

Visually plan and map projects in a roadmap that can be used for tracking and communication. GitLab provides timeline-based roadmap visualizations to enable users plan from small time scales (e.g. 2-week sprints for development teams) to larger time scales (e.g. quarterly or annual strategic initiatives for entire departments).

### What's Next for Roadmaps

1. [Expand Epics on Roadmap](https://gitlab.com/gitlab-org/gitlab/-/issues/70770)
1. [Better Filtering on Roadmaps](https://gitlab.com/gitlab-org/gitlab/-/issues/212248)
1. [View Start and End Dates on Roadmap](https://gitlab.com/gitlab-org/gitlab/-/issues/7076)
1. [Surface Dependencies on Roadmap](https://gitlab.com/gitlab-org/gitlab/-/issues/33587)

*The above plan can change at any moment and should not be taken as a hard commitment, though we do try to keep things generally stable. In general, we follow the same [prioritization guidelines](/handbook/product/product-management/process/#prioritization) as the product team at large. Issues will tend to flow from having no milestone or epic, to being added to the backlog, to being added to this page and/or a specific milestone for delivery.*
