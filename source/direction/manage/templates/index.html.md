---
layout: markdown_page
title: "Category Direction - Templates"
---

- TOC
{:toc}

Last Reviewed: 2020-05-25

## Introduction and how you can help

Thanks for visiting the direction page for Templates in GitLab. This page belongs to the [Import](https://about.gitlab.com/handbook/product/categories/#import-group) group of the [Manage](https://about.gitlab.com/direction/manage/) stage and is maintained by [Haris Delalić](https://gitlab.com/hdelalic) who can be contacted directly via [email](mailto:hdelalic@gitlab.com). This vision is a work in progress and everyone can contribute. If you'd like to provide feedback or contribute to this vision, please add them as comments in the corresponding [epic](https://gitlab.com/groups/gitlab-org/-/epics/2767) for this category.

## Mission

Project templates make it easy to setup a new project by starting from an existing one that already has all the required configuration, files, and boilerplate. GitLab provides a variety of templates as a starting point for creating new projects and it's also possible to contribute to these; administrators can even configure templates specific to your GitLab instance.

## Overview

We already have dozens of templates for common implementations: [Project templates](https://gitlab.com/gitlab-org/project-templates), [CI YAML templates](https://gitlab.com/gitlab-org/gitlab/tree/master/lib/gitlab/ci/templates), [Pages templates](https://gitlab.com/pages), and even [example projects](https://gitlab.com/gitlab-examples). Don't see a template that you wish we had? Please consider contributing that template to GitLab.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=Category%3ATemplates)
- Documentation: [Project templates](https://docs.gitlab.com/ee/gitlab-basics/create-project.html#project-templates), [Custom group-level project templates](https://docs.gitlab.com/ee/user/group/custom_project_templates), [CI YAML templates](https://docs.gitlab.com/ee/ci/examples/README.html#cicd-examples), [Enterprise templates](https://docs.gitlab.com/ee/gitlab-basics/create-project.html#enterprise-templates-ultimate)

## Competitive landscape

#### GitHub

GitHub offers ["Starter Workflows"](https://github.com/actions/starter-workflows) to get started with GitHub Actions. These are [templates for popular continuous integration workflows written in YAML](https://github.com/actions/starter-workflows/tree/master/ci). The YAML file is dependent on a corresponding .properties.json file that defines the metadata about the workflow. 

#### Jenkins

Jenkins' concept of ["pipeline template"](https://jenkinsci.github.io/templating-engine-plugin/pages/Pipeline_Templating/what_is_a_pipeline_template.html) involves two other components, libraries and configuration files. The pipeline template defines "stages" which depend on "libraries" to specify the jobs to be executed for each stage, while the configuration file specifies which libraries are used. Reusable templates are consolidated and managed under a governance structure using [Jenkins Templating Engine (JTE)](https://jenkinsci.github.io/templating-engine-plugin/), a templating plugin.

#### CircleCI

CircleCI provides sample configuration files ([Sample 2.0 config.yml Files](https://circleci.com/docs/2.0/sample-config/)) to guide the user, along with CircleCI Orbs' importable jobs as building blocks for creating a configuration file.

## What's next & why

The Import group is currently focused on the maintenance and stability fixes for the existing Templates functionality. There are no new features being planned in the short-term. 

The long-term strategy includes the following features, which have been identified as priority for this category:

- [Parameterize Project Templates](https://gitlab.com/gitlab-org/gitlab/issues/26580). A top customer issue is a request to parameterize project templates to enable templates configured with variables to prompt the user for inputs that will eliminate manual customizations after the project is created.
- [Refine the Template Contribution Process](https://gitlab.com/groups/gitlab-org/-/epics/863). The current process for creating additional templates involves manual steps. We are looking to automate those steps to make this process easier for everyone to contribute new templates.

## What is not planned right now

While we continuosly evaluate new template ideas, no new templates are being planned at this time. However, we encourage anyone to contribute new templates to GitLab and we will actively support those efforts.
